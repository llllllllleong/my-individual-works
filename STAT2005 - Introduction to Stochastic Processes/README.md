# STAT2005

| Course                                           | Coursework |  Mark  |
|--------------------------------------------------|:----------:|:------:|
| STAT 2005 - Introduction to Stochastic Processes |    1/3     |   71   |
| STAT 2005 - Introduction to Stochastic Processes |    2/3     |   95   |
| STAT 2005 - Introduction to Stochastic Processes |    3/3     |   86   |

I had little motivation to study for this course, mostly because it was one 
of my first "real" statistics courses and I found it very dry. Nowadays, I 
take it for granted that the lecture content is 300 pages of text and maths.

At the time, I as also enrolled in COMP1110 and COMP1600, both of which I found 
much more interesting than this course.




