# COMP2100

| Course                                          |                   Coursework                   | Mark | ANU Grade |
|-------------------------------------------------|:----------------------------------------------:|:----:|:---------:|
| COMP2100 - Software Design Methodologies        | Group Assignment <br/> (Individually Assessed) |  97  |    HD     |


![](../Images/StudyMePreview.jpg)


The only assignment for this course was a group project, where marks were awarded on an individually assessed basis.
<br>
<br>
To complete this project, I had to teach myself about data structures. As part of my learning process, I created several repos on my personal git account, to create and test data structure code. These repos/processes helped me develop understanding.
<br>
<br>
Feedback was returned in .md form, see: "G38_Leong Chan_GP feedback.md"
- We had initially started with 5 members as intended by the course, however one of our members choose not to contribute.
- We completed the project as a team of 4.
- The final contribution allocation (unanimously decided) was
  - 40% Student A
  - 30% Myself
  - 15% Student B
  - 15% Student C

In the feedback given, they note that my submission was "assessed as a special case given your contribution level and/or number of active members in the group. Your contribution is much appreciated and your marks have been increased".

I have not shared:
- The entire project
- Group member details/identities

I have shared:
- Individual class files which I authored/co-authored
- The final report submission, with masking of sensitive information where appropriate
- Individual feedback received

For all content shared, I do not claim credit or ownership as it was a group effort. I do not intend to form any link between the shared content, and the marks I was awarded. My reason for sharing is strictly limited to documentation of my learning process. Should any group members of this project wish to omit our work, do get in touch.

### Hindsight Reflection
- The report is poorly written. It was the last thing to be completed, thus inevitably it was rushed.
- Each member had written sections by themselves, and the final report was the result of attempting to paste the sections together.
- I feel that a lot of content I wrote was repetitive, and not explained well/concisely. Note that there was no word limit, thus we were not too concerned about this.
- The code I authored performs to my expectation and I'm proud of it (given the situation). However, I didn't get around to refactoring it to adhere to good software design principals.
- I didn't make proper use of abstraction and interfacing.
- I intended to apply .... design pattern to the database.
- I made a Trie interface, but im not sure why I only used it for testing.
- Tokenisation and Parsing is suboptimal. I struggled to understand the importance of creating a solid grammar, tokeniser and parser, until it was late into the project.
- I ended up wasting a lot of time re-doing the grammar, tokeniser and parser. 
- I identify consistency as an area to improve on.


The report starts from below this line.

# Group 38 - Report

## Table of Contents

1. [Team Members and Roles](#team-members-and-roles)
2. [Summary of Individual Contributions](#summary-of-individual-contributions)
3. [Application Description](#application-description)
4. [Application UML](#application-uml)
5. [Application Design and Decisions](#application-design-and-decisions)
6. [Summary of Known Errors and Bugs](#summary-of-known-errors-and-bugs)
7. [Testing Summary](#testing-summary)
8. [Implemented Features](#implemented-features)
9. [Team Meetings](#team-meetings)
10. [Conflict Resolution Protocol](#conflict-resolution-protocol)

## Administrative
- Two user accounts for markers' access are usable on the app's APK:
    - Username: comp2100@anu.edu.au  Password: comp2100
    - Username: comp6442@anu.edu.au  Password: comp6442

- Please note the following API levels for our application:
    - compileSdk = [34]
    - minSdk = [33]
    - targetSdk = [34]

- APK location: items/app-debug.apk

## Team Members and Roles
The key area(s) of responsibilities for each member

| UID     |      Name      |       Role |
|:--------|:--------------:|-----------:|
| ------- |   Student A    |  Developer |
| 6858120 | Leong Yin Chan |  Developer |
| ------- |   Student B    |  Developer |
| ------- |   Student C    |  UI Design |

## Summary of Individual Contributions
### **u-------, Student B**
I was involved in implementing the features

- `[Data-Formats]` - [Users.csv](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/assets/Users.csv?ref_type=heads)
- `[Data-Profile]` - [ProfilePageActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ProfilePageActivity.java?ref_type=heads)
- `[Data-Graphical]` - [LeaderBoardActivity](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/LeaderboardActivity.java?ref_type=heads)

**Code and App Design**
- I created and implemented a csv file for data storage
- I designed and implemented the user profile page and leaderboard
- I created an interactive graphical data representation for the leaderboard
- I created the [CategoryProgressAdapter.java] class to handle a scrollable display of a users progress in various question categories

**Other**
- I was also involved heavily in slides preparation


### **u------- Student C**
I was involved in the implementation of the following custom feature:
- `[Search-Filter]`

**Code and App Design**
- I added the Bottom Navigation Bar to all of the screens including the functionality (activities java files) and layout (xml files)
- I adapted most of the layout details of all the activities (except the ProfilePageActivity and LeaderBoardActivity)
- I implemented and designed the SearchFilter feature including the SearchCategoriesAdapters and the functionality in SearchScreenActivity

**Other**
- Primarily I helped with the planning of the app (functions and organization) and designed and implemented the layout and the UI.
### **u6858120 Leong Yin Chan**
I was involved in the implementation of the basic feature:
- `[Search]`

I was involved in the implementation of the following custom feature:
- `[Search-Invalid]`
- `[Search-Filter]` (Data structure side)
- `[Data-Deletion]`

Details of this code contribution are outlined below:
- **Code Contribution in the final App:**

    - **Search**:
        - `Token`, `Tokenizer` and `Parser` classes, which handles the tokenization of a String search query, and parses the tokens into a `SearchQuery`.
        - `SearchQuery` abstract class, with extension classes `UserQuery`, `CategoryQuery`, `TypeQuery`, `IDQuery`, `TitleQuery`.
        - searchQuestions method, which utilises the reverse index structure, to allow for object retrieval based on the given `SearchQuery`.
        - Relevant code files:
            - [Token.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Token.java)
            - [Tokenizer.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Tokenizer.java)
            - [Parser.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Parser.java)
            - [SearchQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/SearchQuery.java)
            - [UserQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/UserQuery.java)
            - [CategoryQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/CategoryQuery.java)
            - [TypeQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/TypeQuery.java)
            - [IDQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/IDQuery.java)
            - [TitleQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/TitleQuery.java)
            - [Database.searchQuestions](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L270)

        - **Search-Invalid**
        - `Trie` class, which is a data structure which allows for prefix search
        - levenshteinMatcher and similarityScore methods, which are used when the query does not correspond to known attributes. These methods calculate a score for the query, and attempts to best match the invalid query to a valid one, up to a set tolerance threshold.
        - Relevant code files:
            - [Trie.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Trie.java)
            - [Database.levenshteinMatcher](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L474)
            - [Database.similarityScore](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L499)

        - **Search-Filter (Data structure side)**
        - Various reverse-index retrieval methods, implemented in [Database.java].
        - Relevant code files:
            - [Database.searchQuestionsFromCategory](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L303)
            - [Database.searchQuestionFromKey](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L324)
            - [Database.searchQuestionFromType](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L324)
            - [Database.searchQuestionsFromTitle](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L372)

        - **Data-Deletion**
        - delete method in `Database.java`, which calls further delete(or safeDelete if applicable) methods on the inner data structures.
        - sweepMaps and sweep methods, which remove attribute keys which no longer respond to any values, in the reverse index maps.
        - Relevant code files:
            - [Database.delete](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L394)
            - [Database.sweepMaps](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L394)
            - [Database.sweep](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L437)

        - **Data Structures**
            - [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java)
            - [AVLMutable.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/AVLMutable.java)
            - [AVLNonMutable.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/AVLNonMutable.java)
            - [EmptyTreeNonMutable.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/EmptyTreeNonMutable.java)
            - [Keyable.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Keyable.java)
            - [StringTrie.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/StringTrie.java)

        - **Testing**
            - [AVLMutableTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/AVLMutableTest.java)
            - [AVLPerformanceTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/AVLPerformanceTest.java)
            - [DataStructureTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/DataStructureTest.java)
            - [ParserTokenizerTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/ParserTokenizerTest.java)
            - [PerformanceTestUtils.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/PerformanceTestUtils.java)
            - [TrieStringTrieTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/TrieStringTrieTest.java)




### **u-------, Student A**
I have significant code contributions that span multiple areas of the application as I was heavily involved in implementing the following application features;
- `[Login]`
- `[DataFiles]`
- `[LoadShowData]`
- `[DataStream]`

Details of this code contribution are outlined below:

- **Code Contribution in the final App:**

    - **Login**:
        - Implemented the `User` class to manage user data within the application.
        - Developed the foundation of the `LoginActivity`, which handles the user login process and interfaces with the backend for authentication. This included implementing secure password storage and authentication using hashed passwords, ensuring that passwords are not stored as plain text. The login process reads hashed passwords and authenticates users accordingly.
        - Relevant code files:
            - [User.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/auth/User.java)
            - [LoginActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/auth/LoginActivity.java)
            - [SecurityUtils.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/SecurityUtils.java)

    - **DataFiles**
        - Our primary data source for the application is a JSON file, `Questions.JSON`, which contains question objects that are read into our application at runtime. I utilised the help of ChatGPT and python scripting to generate 2500 questions.
        - Created the `Question` class to represent our question objects.
        - The data file can be found here:
            - [Questions.JSON](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/assets/Questions.JSON)
        - Other relevant code files:
            - [Question.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/Question.java)

    - **LoadShowData**
        - Developed the `QuestionDeserializer` and `QuestionsWrapper` class to integrate with Gson for parsing JSON data into the appropriate `Question` subclasses based on the type specified in the JSON.
        - Created the `QuestionManager` helper class which manages operations related to question handling, such as adding questions, deleting questions and reading + writing questions to a JSON file.
        - Developed the logic in `MainActivity` to load questions into the tree data structure in the `Database`. This includes implementing the file reading/insertion logic in the methods `loadQuestionsIntoDatabase`, `initializeQuestions`, and `copyQuestionsFromAssets`.
        - Implemented the `bulkInsert` method in the Database class to insert questions in bulk into our tree data structure.
        - Developed the `ViewCategoriesActivity`, `ViewQuestionsActivity`, and `ViewQuestionDetailActivity` for primary UI navigation, displaying questions and interacting with questions.
        - Implemented the `CategoryAdapter` and `QuestionsAdapter` to interface with the UI and display objects.
            - The `CategoryAdapter` manages a list of categories and handles user interactions with these categories.
            - The `QuestionsAdapter` manages the display of individual questions and handles user interactions with these questions.
        - Relevant code files:
            - [QuestionDeserializer.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/QuestionDeserializer.java)
            - [QuestionsWrapper.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/QuestionsWrapper.java)
            - [QuestionManager.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/QuestionManager.java)
            - [MainActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/MainActivity.java) (methods: loadQuestionsIntoDatabase, initializeQuestions, copyQuestionsFromAssets)
            - [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java) (method: bulkInsert)
            - [ViewCategoriesActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ViewCategoriesActivity.java)
            - [ViewQuestionsActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ViewQuestionsActivity.java)
            - [ViewQuestionDetailActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ViewQuestionDetailActivity.java)
            - [QuestionsAdapter.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/adapters/QuestionsAdapter.java)
            - [CategoryAdapter.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/adapters/CategoryAdapter.java)

    - **DataStream**
        - Implemented functionality in `MainActivity` to simulate the addition of new questions to the database at regular intervals using a `Runnable` and `Handler`.
            - The `MainActivity` periodically adds random questions to the 'database', simulating real-time updates.
        - Developed a `QuestionObserver` interface to listen for question addition events and respond appropriately.
        - Integrated the observer pattern to notify the UI of new questions being added, which triggers a Snackbar notification with an option to view the newly added question.
            - To implement the observer I modified the `Database` class with methods to manage the addition and removal of observers, and to notify them when new questions are added.
        - Relevant code files:
            - [MainActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/MainActivity.java)  (methods: addRandomQuestion, generateRandomQuestion, onQuestionAdded, showSnackbarWithAction, navigateToQuestion)
            - [QuestionObserver.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/QuestionObserver.java)
            - [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java) (methods: addObserver, removeObserver, setNotifyObservers)

    - **Code and App Design**
        - **Design Patterns**
            - **Factory Method - QuestionFactory**
                - Proposed and implemented the `QuestionFactory` class to centralize the creation of `Question` objects based on their type.
                - `QuestionFactory` uses various subclasses `MultipleChoiceQuestion`, `TrueFalseQuestion`, `ShortAnswerQuestion`, and `FillInTheBlankQuestion`
            - Relevant code files:
                - [QuestionFactory.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/QuestionFactory.java)
                - [MultipleChoiceQuestion.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/MultipleChoiceQuestion.java)
                - [TrueFalseQuestion.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/TrueFalseQuestion.java)
                - [ShortAnswerQuestion.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/ShortAnswerQuestion.java)
                - [FillInTheBlankQuestion.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/FillInTheBlankQuestion.java)

            - **Singleton - SessionManager**
                - Proposed and implemented the `SessionManager` class using the Singleton pattern to ensure that only one instance of SessionManager exists throughout the application's lifecycle, providing a single point of access to session management functionalities.
            - Relevant code files:
                - [SessionManager.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/auth/SessionManager.java?ref_type=heads)

            - **Observer - QuestionObserver**
                - As stated above in the [DataStream] feature, I utilised the observer pattern here to manage the notifications of new questions being added.

            - **Adapter - CategoryAdapter and QuestionAdapter**
                - As stated above in the [LoadShowData] feature, I utilised the Adapter pattern which helps to take data from our data model (i.e., lists of categories or lists of questions) and they are used by a `RecyclerView` in our Android app activities to display information.

        - **Other design features**
            - I designed the `AddQuestionActivity` which enables users to add a new qusetion within the 'database'. This includes developing the UI and integrating it with the backend to ensure new questions are added, and subsequently written back to the local Json file.
            - Relevant code file:
                - [AddQuestionActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/auth/SessionManager.java)
                - [MainActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/MainActivity.java) (method: copyQuestionsFromAssets and initializeQuestions)

            - I enhanced data persistence features within the app. Our original design did not include storing/tracking the users attempted questions, or storing new questions added, or removing deleted questions. I was able to incorporate this by essentially copying the JSON file from assets, into local storage so that the file can be written to/modified. This more closely simulates a live app experience as changes and modifications persist the next time that user logs back in/reloads the applicaiton.

            - Relevant code file:
                - [JsonFileWriter.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/JsonFileWriter.java)

        - **Testing**
            - Developed the JsonDeserialization JUnit test: [JsonDeserializationTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/JsonDeserializationTest.java)
            - Developed the SessionManager JUnit test: [SessionManagerTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/SessionManagerTest.java)
            - Developed the SecurityUtils JUnit test: [SecurityUtilsTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/SecurityUtilsTest.java)

    - **Others**
        - Included JavaDoc comments for the code I implemented (with help from ChatGPT to generate basic JavaDoc which I then modified with more nuance to explain the code)
        - Actively participated in group discussions and meetings.
        - Set up basic project structure and foundational classes/activitie defining package structures and creating empty classes as placeholders for future development.
        - Assisted other members of group in troubleshooting/fixing errors and bugs where appropriate.
        - Contributed to writing report.

    - **Notes**
        - There are 3 commits where I realised I was signed in with a different git account '----------' which is linked to my GitHub account. Though I confirm these commits were made by me and only me, I was working on a different device where this account was still signed on in with Git.

## Application Description
For the theme of our app, we chose to address the "Closing the gap" (https://www.closingthegap.gov.au/national-agreement/targets) target #5 "Students achieve their full learning potential" which talks about "Aboriginal and Torres Strait Islander students achieve their full learning potential" and "By 2031, increase the proportion of Aboriginal and Torres Strait Islander people (age 20-24) attaining year 12 or equivalent qualification to 96 per cent".

We designed 'StudyMe' which is an educational application designed specifically for Higher School Certificate (HSC) students focusing on Mathematics. For this implementation we decided to limit the scope to Mathematics, but could easily be extended to incorporate other disciplines in the HSC.

The tool is designed for students preparing for exams and assignments, offering a wide range of quizzes that cover all topics within the HSC Mathematics curriculum. The app aims to enhance academic performance by gamifying the learning process, motivating students to excel by integrating elements such as progress tracking and competitive leaderboards.

### Application Use Cases and or Examples
Use Case 1: Weekly Practice for Students
- Objective: A Year 12 student wants to improve their skills in Calculus to prepare for HSC exams.
    - The student logs into the "StudyMe" app at the end of the school week.
    - They select 'Calculus' (or another topic they are currently studying) from the list of topics in the app and complete the questions within that category.
    - After which they can review the questions they may have answered incorrectly and note down topics for further review/study.

Use Case 2: Teacher-Assisted Learning
- Objective: An HSC Mathematics teacher wishes to provide additional practice material for students and track their progress.
    - The teacher suggests questions that students can search for in the app and attempt as homework before the next lesson.
        - Alternatively, the teacher can use the 'Add a new question' function to add specific questions for students to practice.
    - The teacher, who can also be a user of the app, views the leaderboard and tracks students' progress.
    - The teacher could then review these questions in the next class and modify learning materials/teaching schedule according to how students are progressing.
    - The teacher could then also assign more individualised learning plans for students with lagging areas of knowledge.

Use Case 3: Competition Among Friends
- Objective: A student wants to compete and test their knowledge against friends.
    - Students log into the app and select the same category of questions.
    - They attempt all questions and compare their results.
    - Students can view their progress and compare their performance on the leaderboard.
<hr> 

### Application UML

![ClassDiagram](media/entityUML.png) <br>

![DataStructureDiagram](media/dataUML.png)
<hr>

## Code Design and Decisions


### File Parsing
- JSON file structure and parsing: The JSON parsing is handled by the Gson library, chosen to convert JSON data into Java objects. We employed a JsonDeserializer to handle the specific needs of our abstract Question class, enabling custom instantiation logic based on the JSON "type" attribute.
    - Justification:
        1. Gson is a widely used library, ensuring code readability and maintainability.
        2. Custom deserializers allow handling of polymorphic types, crucial for our Question class hierarchy.
        3. Gson efficiently parses large JSON files, an important factor given our app's large dataset of 2500 Questions.
        4. Gson integrates seamlessly with Android.
        5. The Gson setup includes robust error management, ensuring that parsing errors are clearly logged and handled correctly.

### Class Hierarchy
- Question Class Hierarchy: We utilized an abstract Question class with specific subclasses (MultipleChoiceQuestion, TrueFalseQuestion, etc.) to represent different types of questions.
    - Justification:
        1. Using subclasses for each question type helps organize code logically, making it easier to manage.
        2. New question types can be added with minimal changes to existing code.
        3. This approach leverages Java's type system to ensure that each question type is handled correctly based on its specific attributes.
        4. Common attributes and methods are centralized in the abstract Question class, promoting code reuse.


### Single-User Data Persistence
- For storing and managing user progress and responses, our application utilizes a local JSON file. This file captures all attempted questions along with their answers, marking them as correct or incorrect based on the user's responses. These persisted respones are designed to be solely for the user logged in on the device. If another user logs in, then this will override the responses in the stored local file.
    - Justification:
        1. Managing data for a single user on a device simplifies the design by reducing the need for complex user management systems, particularly as we are using local JSON files for data persistence.
        2. In real-world scenarios, educational and quiz apps are often used by individual learners on their personal devices. By simulating this environment, we ensure that our application is both relevant and realistically modeled for its intended use case.
        3. Handling data for a single user minimizes the resource usage on the device. It avoids the complications of data conflicts and synchronization issues that can arise in multi-user environments, ensuring smooth and efficient app performance.


<hr>

### Data Structures: Database Class
The 'Database' class act as an interface to the underlying data structures and indexing methods.
- Unified Access to Data Structures
    - The Database class provides the fundamental methods for inserting, deleting, retrieving, and searching. This decision allows for the complexities of data insertion, retrieval and deletion to be abstracted,
      providing a more user-friendly way to interact and manipulate the objects inside.
- Abstracting underlying implementations
    - This allows for future-proofing through easy modifications
    - Primary storage can be switched between Mutable AVL and Non-Mutable AVL, without changing the interface
    - Indexing: The char/prefix Trie can be switched to the String Trie, if memory usage scaling becomes an issue in the future, as the database grows
- Encapsulation and Modularity
    - Changes to the underlying data structure are isolated from other parts of the app
    - This reduces the chances of side effects causing issues and bugs.
    - It also allows for easier traceability of issues and bugs, a feature which proved itself during development.
- Observer Pattern Integration
    - The Database class implements the observer pattern to notify observers about changes.
    - This allows for real-time updates and dynamic responses to data changes, for example, when a question is added or removed.

### Data Structures: Reverse Indexing via Unique Integer Key
The database was implemented as a reverse-indexed data structure. This decision was based on the fact that fast and efficient search queries was expected to be an integral part of the app. Furthermore, this
allows for flexibility in the types of queries, such as phrases, ranking queries, and compound queries, such as intersection union, or merge.
- Indexing via unique key
    - Every object successfully inserted into the database is assigned a unique Integer key.
    - Prior to insertion, the database will check if the object is already in the database. If the object already has a key, it will check if the key is mapped to an active object. If the object does not have a key, it will use the prefix trie to search for the question title, and matches will compared using an overridden .equals method.
    - This ensures that duplicate questions will not be inserted into the database.
    - By assigning a unique key to eligible objects at time of insertion, ensures that no object has the same key, and allows for the indexing search/filter methods to efficiently query the object.
    - The unique key is assigned via a private local keyCounter variable in the database class. Since there is no other way in or out of the data structure except for the database class, this ensures that all keys are unique and accounted for.
    - Keys provide a direct way to access question. Instead of searching through titles or other attributes first, which is time and resource intensive, the key allows for O(1) average-time complexity lookups.

### Main Storage: AVL Tree Data Structure
Code locations: defined in [AVLMutable.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/AVLMutable.java) used in [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java)
- AVL Tree for Question Management: We implemented an AVLTree to manage and organize questions efficiently within the app.
    - Justification:
        1. AVL trees maintain balance through rotations, ensuring that the tree's height is kept to a minimum. This results in consistently fast search times, important for efficiently finding questions based on the questionID and title.
        2. Unlike other balanced trees, AVL trees maintain a strict balance, which is crucial for operations that require ordered data, such as displaying questions in a specific order.
        3. The AVL tree allows for dynamic insertion and deletion of questions while maintaining balance.
        4. AVL trees support efficient in-order traversal to list questions in sorted order.
        5. While maintaining balance, AVL trees require fewer rotations compared to other self-balancing binary search trees like Red-Black trees, which can lead to more straightforward implementation and slightly better performance for read-heavy workloads.

- Mutable AVL as opposed to the Non-Mutable:
    - We created both Non-Mutable and Mutable AVL data structures, and used testing to determine which would be the best fit.
    - Insertion performance was superior on the mutable tree
    - Safe-deletion performance was superior on the mutable tree
    - Retrieval/Search Performance was identical
    - Memory usage was significantly lower for the Mutable tree



### Prefix Index Mapping: Trie Data Structure
Code locations: defined in [Trie.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Trie.java?ref_type=heads) used in [Database.java]
- A char prefix trie was implemented to map the title of question objects to their index.
    - Efficient Prefix Searching: This allows for users to search for question titles
    - Autocomplete: Allows for a successful search, even if their search query does not contain the full title of the question
    - Speed: O(n) retrieval time, where n is the length of the search query
- However, in our testing we found that memory usage started to become an issue when size of the database got to the 100,000 mark, and the trie was the main culprit.
    - In response to this, we implemented an alternative: StringTrie, which provides the same features and a lower memory usage, at the cost of speed.
    - The StringTrie is not currently in use, because memory usage is not a concern yet. However, the char trie can easily be swapped for the String Trie in the future
    - This gives us confidence in future scalability
    - The StringTrie is able to provide the same features as the char Trie, through Levenshtein distance calculations, with a Levenshtein distance threshold to determine how flexible it is in string matching

### Attributes Index Mapping: HashMap
Two HashMaps have been used to complement the Trie in reverse index mapping. HashMaps were selected, due to their efficient hashing algorithm, and their unique key value pair feature.
1.  categoryIndexMap
- The key for this HashMap is the String representation of question categories, such as "Calculus" or "Trigonometry".
- The value is a list of Integer keys, which correspond to questions which fall under the key category.
- Search queries looking for questions under a specific category, will utilise this index map
- Levenshtein distance calculations are used for when queries do not directly match with an existing String key in the map. In this case, it will attempt to best match the input Category with an existing Category key in the map
2. typeIndexMap
    - The key for is the enum "QuestionType", which is defined under the QuestionType class.
    - The value is also a list of Integer keys, corresponding to Question objects which fall under the respective QuestionType enum.
    - Levenshtein distance is also implemented.


<hr>

### Design Patterns
1. *Factory Method Design Pattern*
    * *Objective:* To handle the creation of different types of `Question` objects. Question objects can be created via reading in from a JSON file or by adding new questions within the app itself.
    * *Code Locations:* defined in [QuestionFactory.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/QuestionFactory.java)
    * *Reasons*:
        * The Factory Method allows the application to introduce new types of questions without altering the existing codebase. This is particularly useful for future expansions where more complex question types might be required.
        * This pattern decouples the code that generates questions from the classes that use these questions. By doing so, changes to the question creation logic or the underlying question model will have minimal impact on the rest of the application.
        * Having a centralized question factory ensures that all questions are created consistently, adhering to predefined rules and structures which helps maintain data integrity especially when parsing from external sources like JSON files.

2. *Singleton Design Pattern*
    * *Objective:* Enable consistent session management across the application.
    * *Code Locations:* defined in [SessionManager.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/auth/SessionManager.java)
    * *Reasons*:
        * The singleton pattern ensures that there is only one instance of the `SessionManager` class throughout the application. This helps to maintain a consistent state across different parts of the app.
        * Usage of a singleton pattern is resource efficient because it restricts instantiation of the session management class to one object. This is particularly beneficial in mobile enviroments like Android.
        * The Singleton pattern simplifies the implementation of session management as the session manager can be accessed globally without needing to re-instantiate the object or pass references throughout the app.

3. *Observer Design Pattern*
    * *Objective:* To notify users dynamically about the addition of new questions during the session.
    * *Code Locations:* defined in [QuestionObserver.java] which is an interface used in [Database.java] and [MainActivity.java]
        - Files:
            - [MainActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/MainActivity.java)
            - [QuestionObserver.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/questions/QuestionObserver.java)
            - [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java)
    * *Reasons*:
        * Utilizing the Observer pattern allows the application to inform users immediately when new questions are available. This interaction is simulated from our [DataStream] feature implementation.
        * By separating the question addition logic from the notification logic, the application enhances its modularity. The observing components (UI elements displaying the questions) are only concerned with how to respond to the update, not how or when the updates occur.
        * Integration with Android's Runnable and Handler provides a reliable way to schedule future updates. This scheduling capability is essential for simulating a dynamic learning environment where new questions appear at predefined intervals

<hr>

### Parser

### <u>Grammar(s)</u>

Production Rules:
The app parses search tokens, according to the following token grammar:

    <SearchQuery>      ::= <UserQuery> | <CategoryQuery> | <TypeQuery> | <IDQuery> | <TitleQuery>

    <UserQuery>        ::= (UserQuantifier Token) (Word Token)
    <CategoryQuery>    ::= (CategoryQuantifier Token) (Word Token)
    <TypeQuery>        ::= (TypeQuantifier Token) (Word Token)
    <IDQuery>          ::= (IDQuantifier Token) (Number Token)
    <TitleQuery>       ::= (Word Token) | (Word Token) <TitleQuery>

The tokenizer tokenizes String search queries, according to the following string regex:

    <UserQuantifier Token>          ::= "@"
    <CategoryQuantifier Token>      ::= "QC:"
    <TypeQuantifier Token>          ::= "QT:"
    <IDQuantifier Token>            ::= "QID:"
    <Number Token>                  ::= A sequence of digits
    <Word Token>                    ::= A sequence of alphanumeric characters


### <u>Tokenizers and Parsers</u>
[Token.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Token.java)

[Tokenizer.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Tokenizer.java)

[Parser.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Parser.java)

- The code for the Token, Tokenizer and Parser have their own individual respective classes.
    - Modularity and Reliability: Allows for each element to be tested individually, and prevents changes in one class from affecting all other classes.
    - Future-proofing: Additional tokens or changes in the tokenization or parsing logic can be easily made in the future.
    - Tokenizer Regex:  Flexible and powerful pattern matching, with the potential to extend the tokenization logic to support new patterns or token types in the future.

- The output of the parser is a `SearchQuery` abstract class, with extension classes `UserQuery`, `CategoryQuery`, `TypeQuery`, `IDQuery`, `TitleQuery`.
    - For the same reasons stated above.
    - Future-proofing: Additional types of SearchQueries can easily be added in the future, simply by extending the `SearchQuery` abstract class.

- The grammar has been designed as non-ambiguous.
    - Clear and Explicit Structure: The grammar is explicitly defined, making it clear what types of tokens and queries are supported.
    - Readability: The supported types of queries is easily identifiable at a glance.
    - Modularity: Retains the modularity design.
    - Easily extendable.
    - Support for search compounding.

<br>
<hr>

## Implemented Features
### Basic Features
1. [LogIn].
    * Code: The primary functionality of the login feature is implemented in [LoginActivity.java], with security utilities managed in [SecurityUtils.java]
        - Code files:
            - [LoginActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/auth/LoginActivity.java)
            - [SecurityUtils.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/SecurityUtils.java)
            - [Users.csv](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/assets/Users.csv)
    * *Description of Feature:*
        * The login feature provides secure authentication for users attempting to access the app. It verifies user credentials (username and password) against a list of registered users. The feature also includes error handling to manage failed login attempts and uses secure hashing for password verification.  <br>
    * *Description of Implementation:*
        * `LoginActivity` includes some basic UI components, including username and password fields and a login button
        * When the login button is clicked, the `performLogin` method is called which retrieves a list of users from [Users.csv] and uses the `hashPassword` method from `SecurityUtils` to hash the entered password. It then checks if the hashed pasword matches the stored hashed password for the username.
        * If the login is successful (i.e., credentials verified) the `loginUser` method is called which uses the `SessionManager` singleton to set the current user and the user is navgiated to the `MainActivity`.

2. [DataFiles].
    * Our primary data source for the app is stored in [Questions.JSON](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/assets/Questions.JSON) which represents all of the questions i.e, `Question` objects.
    * Additionally, user data for login authentications is stored in [Users.csv] which represents all of the users i.e.,  `User` objects for the app.
    * Lastly [RandomQuestions.JSON](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/assets/RandomQuestions.JSON) is used in our [DataStream] feature, which loads questions from this file randomly at set intervals.

3. [LoadShowData]
    * *Code:* The main implementation is in [ViewCategoriesActiviy.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ViewCategoriesActivity.java), which acts as the starting point for the quiz question navigation/interaction. However the following activities also show our loaded data:
        - [ViewQuestionsActivity](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ViewQuestionsActivity.java)
        - [ViewQuestionDetailActivity](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ViewQuestionDetailActivity.java)

    * *Description of Feature*:
        * This feature is responsible for loading and displaying questions from the Questions.JSON file mentioned above into a database that can be utilized within the app.
    * Description of Implementation:
        * Upon creation of `ViewCategoriesActivity`, the `RecyclerView` for displaying a list of question categories (Mathematics sub-topics) is set up. This setup includes loading question data into the application's database, which the `loadQuestionsIntoDatabase` method performs by parsing questions from a JSON file and inserting them into an AVL tree structure managed by the app's Database class.
        * In `ViewCategoriesActivity`, the categories are extracted and displayed in a `RecyclerView` using a `CategoryAdapter`. This adapter handles the layout and interaction of category items within the list. Here, users can select a category or search for a specific category that can then be clicked on.
        * When a category is clicked, the `onCategoryClick` method triggers, which navigates the user to `ViewQuestionsActivity` where questions from the selected category are retrieved from the database based on the selected category. This is achieved by calling the `getQuestionsForCategory` method, which filters the questions and returns a list of questions belonging to the selected category.
        * The filtered questions are displayed in a `RecyclerView` using `QuestionAdapter`, which handles the layout and interaction of individual question items.
        * When a user selects a specific question from the `RecyclerView` in `ViewQuestionsActivity`, they are navigated to `ViewQuestionDetailsActivity`.
        *  This activity presents the detailed view of the selected question, including the question text, available options (for multiple-choice questions), and input fields (for short answer or fill-in-the-blank questions).
        * Users can submit their answers and receive immediate feedback in this activity.
        * The state of the question (answered or unanswered) is updated in the database to reflect the user's progress and this state is written back to a JSON file so that their responses persist the next time the application loads.
        * Users can also navigate through the questions using 'next' and 'back' buttons provided in the UI. These buttons facilitate seamless movement between questions within the selected category.

4. [DataStream]
    * *Code*:
        * The implementation of our [DataStream] feature is in [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java) and [MainActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/MainActivity.java)
    * *Description of Feature:*
        * This feature implements the Observer pattern to notify registered observers about the addition of new questions to the database.
    * *Description of Implementation:*
        * The `QuestionObserver` interface defines a method `onQuestionAdded` which is called to notify observers about new questions being added to the database.
        * The `Database` class maintains a list of observers and provides methods to add and remove observers. The `setNotifyObservers` method controls whether observers should be notified of new questions.
        * A new question being inserted into the database is simulated in the `MainActivity` by calling the `insert` method from the `Database` which inserts a random question, from a [RandomQuestions.JSON]. This simulates a scenario in the application where another user has added a question into the database, and then the user logged in receives a notification.
        * If `notifyObservers` is true, all registered observers are notified via a `Snackbar` once the question gets added. The user can interact with the `SnackBar` which will navigate the user to view the details of the newly added question, and they can interact with the question by attempting to answer it.

5. [Search]
    * *Code*:
        * The implementation of Search spans multiple classes
        * [Database.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java)
        * [Token.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Token.java)
        * [Tokenizer.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Tokenizer.java)
        * [Parser.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/Parser.java)
        * [SearchQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/SearchQuery.java)
        * [UserQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/UserQuery.java)
        * [CategoryQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/CategoryQuery.java)
        * [TypeQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/TypeQuery.java)
        * [IDQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/IDQuery.java)
        * [TitleQuery.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/utils/TitleQuery.java)
        * [Database.searchQuestions](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L270)
    * *Description of Feature:*
        * A search query is first called via the `search()` method in Database. The database will tokenize the String into tokens, for the parser to parse into a `SearchQuery`.
        * The Database will identify the instance of `SearchQuery` and will call the respective reverse index search method.
        * The implementation of a reverse index database allows for lightning fast searches to be executed.
        * A disadvantage of a reverse index database is that you are only able to access items via a Key. If you do not know what is the key of the object you are looking for, you will not be able to retrieve anything usefull.
        * Therefore, we have reverse index maps, which keep a record of what attributes correspond to what key.
    * *Description of Implementation:*
        * The `SearchQuery` abstract class defines what type of searches can be performed on the database.
        * The tokenization grammar defines how to identify the type of SearchQuery to use.
        * The details of implementation have been mostly described above, where we justified our design of the data structure.



### Custom Features
Feature Category: Greater Data Usage, Handling and Sophistication <br>
1. [Data-Formats] (add links to relevant code)
    * *Description of Feature:*
    * *Description of Implementation:*

2. [Data-Profile]
    * *Description of Feature:*
        * The profile page is an easy way for a user to check their progress on various categories as well as their overall progress and placing on the leaderboard
    * *Description of Implementation:*
        * This feature was implemented using the classes [ProfilePageActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/ProfilePageActivity.java?ref_type=heads) and [CategoryProgressAdapter.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/adapters/CategoryProgressAdapter.java?ref_type=heads)
        * In the `OnCreate()` features of the profile page are initialized with user data and a CategoryProgressAdapter is created and set to the RecyclerView
        * A custom view holder is created by `OnCreateViewHolder()`, using the `category_card_layout.xml` file
        * `getProgressScores()` returns the users progress on various categories as a portion of the total questions in each category and initializes progress bars


3. [Data-Graphical] (add links to relevant code)
    * *Description of Feature:*
        * The leaderboard page can be accessed from the users profile and contains an intractable bar chart which ranks the progress of various users based on their scores
    * *Description of Implementation:*
        * This feature was implemented by the class [LeaderboardActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/LeaderboardActivity.java?ref_type=heads)
        * The leaderboard uses an external library- MPAndroid - in order to generate the bar chart
        * By instantiating a custom `OnChartValueSelectedListener` the user is able to click on chart entries and obtain information such as username and score
        * `returnPlace()` finds the current users ranking in the leaderboard by parsing data from the `Users.csv` file

4. [Data-Deletion]
    * *Description of Feature:*
        * Objects in the database can be deleted
    * *Description of Implementation:*
        * First, the unique key of the object to be deleted, has to be identified. This can be done through searching for the object.
        * Once the valid key of the object is obtained, the object can be deleted from the main storage AVL tree.
        * As the database is a reverse index data structure, the key also has to be deleted from the attribute index maps: Trie, categoryIndexMap and questionTypeIndexMap. This is accomplished through the sweep and sweepMaps methods, which are automatically called by the database.
        * Deletion from the mutable AVL tree, is performed though a "Safe-delete" implementation.
        * Although we have a method for deletion on the mutable tree, we decided to substitute it for our own "Safe-delete" method, over concurrency and thread safety concerns.
        * Our "Safe-delete" method, copies all object in the tree except for the item to be deleted, creates a new mutable tree, and re-inserts all objects into the new tree through the "Insert" method
        * Although "Safe-delete" is more inefficient, it minimises the risk of errors.
        * We do not anticipate the delete feature to be used often by the user, therefore we justify the use of a more inefficient method, in return for stability and safety.
        * In our testing, the "Safe-delete" method does not take up significant runtime(less than 200 ms for tree sizes up to 50,000)
        * This method ensures that the AVL tree remains balanced, assuming the methods used to construct the tree, are correct.

Feature Category: Search-related features

5.[Search-Invalid]

[Trie.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Trie.java)

[Database.levenshteinMatcher](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L474)

[Database.similarityScore](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/datastructures/Database.java#L499)

* *Description of Feature:*
    * AutoComplete: Prefix searching.
    * When searching for question title, the search query does not need to have the full title of the question.
    * For example, a search query looking for question title "What is t" will still return questions such as
        * What is the derivative of 3x^2?
        * What is the mode of the dataset {2,3,4,4,5,5,5,6}?
    * Levenshtein distance string matching for illegal attribute search queries
    * Allows for invalid searches to be successfully performed, up to a set tolerance parameter.
    * For example, a search query looking for questions under "Trickonometry", will still successfully return Trigonometry questions.
    * "mcq" and "multi" will still successfully return MULTIPLE_CHOICE Questions.
    * The tolerance/flexibility in String matching is configurable with the local variable: "levenshteinThreshold" in database.
* *Description of Implementation:*
    *  Prefix searching is accomplished through a Trie data structure.
    *  Every object successfully inserted into the database, is also indexed into the Trie. The question title is parsed into characters, and the question's unique key is stored in the Trie.
    *  Incomplete strings are detected by the Trie, and the Trie is able to search further child nodes for index keys.
    *  Levenshtein distance String matching is only used when the search query string does not match known attributes.
    *  The levenshteinMatcher method sorts matches based on their similarity scores, calculated by the Levenshtein distance algorithm.
    *  The similarityScore method computes the Levenshtein distance between two strings by via dynamic programming approach.
    *  It will fill a 2D array with the minimum number of edits (insertions, deletions, substitutions) required to transform one string into the other.
    *  By comparing the Levenshtein distance to the "levenshteinThreshold", the algorithm determines whether the best match is close enough to the input to be considered similar.
    *  If the distance is within the threshold, the closest matching string is returned. Otherwise, the original input is returned.
    *  This approach allows users to still find relevant results, even if their search terms are not exact matches.



6. [Search-Filter]
    * *Description of Feature:*
        * The Search Filter allows you to more efficiently search for the all the questions of a specific category with the click of a button, with a horizontal scrollable menu of category buttons.
    * *Description of Implementation:*
        * This feature was implemented by the class [SearchScreenActivity.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/activities/SearchScreenActivity.java?ref_type=heads)
        * This feature was implemented by the class [SearchCategoriesAdapters.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/main/java/com/example/groupproject24s1/adapters/SearchCategoriesAdapter.java?ref_type=heads)
        * This feature simply reads the least of categories from the JSON file, and process it with the Search Categories Adapter, in order to give the functionality to each item of the displyed list of updating the paramaters with the function and the pass in paramaters: `updateQuestionList(category, "Question Category"`, which would effectively update the displayed result list to the questions belonging to the selected category.
<hr>

## Summary of Known Errors and Bugs

1. *Bug 1:*
    - Searching for 'Question Type' = multiple choice will return no results which is not intended. Other variations of 'multiple choice' will return correctly such as:
        - multiple_choice
        - mult

    - ![SearchBugExample](media/searchBug.png)

2. *Bug 2:*
    - The user score does not persist after log out. The app will be able to restore the user answers, bit it is not reflected in the user score. The app will load with user comp2100@anu.edu.au score at 30. The app will reload any correctly answered questions but the score is not persisted. Example:

    - ![userScoreBug](media/scoreBug.png)

3. *Bug 3*
    - There is a bug with questions being re-initialised every time the main activity creates. This unfortunately leads to questions being duplicated. The issue is specifically the `initializeQuestions` method in the `onCreate` of `MainActivity`. This could be resolved by adding the following:

        - Initialise a boolean variable which will check if the questions have been initialised:
          `private Boolean questionsInit = false`

        - In the `initializeQuestions` method, add the following code:

              ```
              if (!questions.isEmpty()) {
                 SessionManager.getInstance().getDatabase().bulkInsert(questions);
                 questionsInit = true;
              }

        - and then add the following in the `onCreate`:

              ```
                 if (!questionsInit) {
                    initializeQuestions();
                 }      

   <hr>

## Testing Summary

1. AVL Tree Performance Testing

For every tree size,
- Insertion performance was assessed at 100 objects inserted.
- Search/Retrieval performance was assessed at 100 objects retrieved.
- Deletion performance was assessed at 100 objects deleted.

![avlInsertPerformance](media/avlInsertPerformance.png)
![avlSearchPerformance](media/avlSearchPerformance.png)
![avlDeletePerformance](media/avlDeletePerformance.png)
![avlMemoryUsage](media/avlMemoryUsage.png)


2. Trie Performance Testing

For every trie size,
- Insertion performance was assessed at 10 objects inserted.
- Search/Retrieval performance was assessed at 1 object retrieved.
- Deletion performance was assessed at 10 objects deleted.
  ![trieInsertPerformance](media/trieInsertPerformance.png)
  ![trieSearchPerformance](media/trieSearchPerformance.png)
  ![trieDeletePerformance](media/trieDeletePerformance.png)
  ![trieMemoryUsage](media/trieMemoryUsage.png)


3. Parser, Tokenizer and Token
    - [ParserTokenizerTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/ParserTokenizerTest.java)
    - *Number of test cases:*
        - 13
    - *Code coverage:*
        - Main functionalities of the Tokenizer and Parser classes
        - Tokenization of different quantifiers (UserQuantifier, CategoryQuantifier, TypeQuantifier, IDQuantifier)
        - Tokenization of numbers and words
        - Parsing of various query types (UserQuery, CategoryQuery, TypeQuery, IDQuery, TitleQuery)
        - Handling of mixed case quantifiers
    - *Types of tests created and descriptions:*
        - **testTokenizerWithUserQuantifier:** Verifies that the tokenizer correctly identifies and tokenizes a user quantifier and word token.
        - **testTokenizerWithCategoryQuantifier:** Checks that the tokenizer properly handles category quantifiers, both with and without additional whitespace.
        - **testTokenizerWithTypeQuantifier:** Ensures the tokenizer can correctly process type quantifiers.
        - **testTokenizerWithIDQuantifier:** Confirms that the tokenizer accurately tokenizes ID quantifiers and number tokens.
        - **testTokenizerWithNumber:** Tests that a sequence of digits is correctly tokenized as a number.
        - **testTokenizerWithWords:** Validates that multiple words are correctly tokenized as individual word tokens.
        - **testTokenizerWithMixedCaseQuantifiers:** Assesses the tokenizers ability to handle mixed case quantifiers.
        - **testParserWithUserQuery:** Verifies that the parser can correctly parse a user query and produce a UserQuery object with the expected value.
        - **testParserWithCategoryQuery:** Ensures the parser correctly parses a category query and produces a CategoryQuery object with the expected value.
        - **testParserWithTypeQuery:** Confirms that the parser correctly parses a type query and produces a TypeQuery object with the expected value.
        - **testParserWithIDQuery:** Checks that the parser accurately parses an ID query and produces an IDQuery object with the expected value.
        - **testParserWithTitleQuery:** Tests that the parser can parse a title query with multiple words and produce a TitleQuery object with the expected value.
        - **testTokenizerWithMixedCaseQuantifiers:** Validates that the parser and tokenizer together can handle mixed case quantifiers, ensuring they are correctly tokenized and parsed into the appropriate query objects.


4. Data Structure
    - [DataStructureTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/DataStructureTest.java)
    - *Number of test cases:*
        - 7
    - *Code coverage:*
        - Integration with JSON data for bulk insertion and search
        - Initialization of the database
        - Insertion of questions
        - Handling of duplicate insertions
        - Deletion of questions
        - Searching for questions by various criteria (ID, category, title)
        - Handling of unknown or non-existent questions
    - *Types of tests created and descriptions:*
        - **databaseInitialisationTest:** Verifies that the database is correctly initialized to be empty and has a size of 0.
        - **databaseInsertTest:** Tests the insertion of multiple questions into the database and checks if the size of the database is updated correctly.
        - **databaseInsertDuplicatesTest:** Ensures that duplicate questions are not inserted into the database, verifying that the size remains consistent.
        - **databaseDeleteTest:** Tests the deletion of a question from the database, ensuring that the size is updated correctly and that the deleted question is no longer present. Also checks for the removal of categories when no questions remain in that category.
        - **databaseUnknownQuestion:** Ensures that operations (such as search and delete) on unknown or non-existent questions are handled gracefully, without affecting the database.
        - **databaseSearchTest:** Verifies the search functionality by category, ensuring that questions belonging to a specified category are correctly retrieved.
        - **levenshteinMatcherTest:** A special test (currently ignored) used to determine the suitable value for the Levenshtein distance threshold, testing the string matching logic under various scenarios.

5. Mutable AVL Tree
    - [AVLMutableTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/AVLMutableTest.java)
    - *Number of test cases:*
        - 15
    - *Code coverage:*
        - Insertion of elements into the AVL tree
        - Deletion of elements from the AVL tree
        - Safe deletion and checking if elements are present
        - Retrieval of elements by key
        - Checking if the tree is empty
        - Bulk insertion and deletion of elements
        - Ensuring the AVL tree remains balanced
        - Handling of null values and edge cases
    - *Types of tests created and descriptions:*
        - **mutableAVLInsertAndContainsKeyTest:** Verifies that the AVL tree correctly inserts elements and checks for their presence using the key.
        - **mutableAVLGetTest:** Ensures that the AVL tree can retrieve elements by their key, returning the correct element or null if the key is not present.
        - **mutableAVLSafeDeleteTest:** Tests the safe deletion of elements from the AVL tree, ensuring that elements are removed correctly and return appropriate results when trying to delete non-existent elements.
        - **mutableAVLIsEmptyAndDeleteAllTest:** Checks if the AVL tree is empty initially, and after inserting and deleting elements, ensuring the tree's empty state is correctly reported.
        - **mutableAVLGetAllTest:** Verifies that all elements can be retrieved from the AVL tree and that the correct elements are present.
        - **mutableAVLSizeTest:** Ensures the AVL tree's size is correctly updated after inserting and deleting elements.
        - **mutableAVLInsertNullTest:** Ensures that inserting a null element throws an `IllegalArgumentException`.
        - **mutableAVLDeleteFromEmptyTreeTest:** Ensures that attempting to delete an element from an empty AVL tree throws an `IllegalArgumentException`.
        - **mutableAVLDeleteNullKeyTest:** Ensures that attempting to delete an element with a null key throws an `IllegalArgumentException`.
        - **mutableAVLGetNullKeyTest:** Ensures that attempting to retrieve an element with a null key throws an `IllegalArgumentException`.
        - **mutableAVLLargeScaleInsertionTest:** Verifies that the AVL tree can handle large-scale insertions and correctly report the size and presence of elements.
        - **mutableAVLLargeScaleSearchTest:** Ensures that the AVL tree can handle large-scale searches, retrieving elements correctly after a large number of insertions.
        - **mutableAVLLargeScaleDeletionTest:** Verifies that the AVL tree can handle large-scale deletions, ensuring all elements can be deleted and the tree is empty afterward.
        - **mutableAVLLargeScaleCombinedTest:** Tests the AVL tree's ability to handle a combination of large-scale insertions, deletions, and searches, ensuring the tree's size and elements are managed correctly.
        - **mutableAVLLargeScaleCombinedBalanceTest:** Ensures that the AVL tree remains balanced after a series of large-scale insertions and deletions, verifying the balance factor and height properties.


6. AVL Tree Performance
    - [AVLPerformanceTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/AVLPerformanceTest.java)
    - *Number of test cases:*
        - 4
    - *Code coverage:*
        - Insertion, deletion, and search operations for both non-mutable and mutable AVL trees
        - Performance comparison between non-mutable and mutable AVL trees
        - Memory usage measurement for both non-mutable and mutable AVL trees
    - *Types of tests created and descriptions:*
        - **demonstrateHowToUseAVL:** Demonstrates how to use the basic operations (insert, search, safe-delete) on both non-mutable and mutable AVL trees.
        - **avlPerformance:** (Ignored) Compares the performance between non-mutable and mutable AVL trees for various tree sizes. It measures the time taken for insertion, deletion, and search operations and writes the results to a CSV file.
        - **nmAVLMemoryUsagePerformance:** (Ignored) Measures the memory usage of the non-mutable AVL tree for various tree sizes and writes the results to a CSV file.
        - **mAVLMemoryUsagePerformance:** (Ignored) Measures the memory usage of the mutable AVL tree for various tree sizes and writes the results to a CSV file.

7. Trie and StringTrie Performance
    - [TrieStringTrieTest.java](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/AndroidApp/app/src/test/java/com/example/groupproject24s1/TrieStringTrieTest.java)
    - *Number of test cases:*
        - 7
    - *Code coverage:*
        - Insertion of elements into both `Trie` and `StringTrie`
        - Deletion of elements from both `Trie` and `StringTrie`
        - Searching for elements by prefix in both `Trie` and `StringTrie`
        - Performance comparison between `Trie` and `StringTrie` for insertion, deletion, and search operations
        - Memory usage measurement for both `Trie` and `StringTrie`
    - *Types of tests created and descriptions:*
        - **trieSingleInsertTest:** Verifies that a single element can be inserted into both `Trie` and `StringTrie`, and that the element is correctly contained within the structures.
        - **trieInsertionTest:** Tests the insertion of multiple elements into both `Trie` and `StringTrie`, ensuring that all elements are correctly contained.
        - **trieDeleteExistingTest:** Ensures that existing elements can be successfully deleted from both `Trie` and `StringTrie`, and that the elements are no longer present after deletion.
        - **trieDeleteUnknownTest:** Verifies that attempting to delete unknown elements from both `Trie` and `StringTrie` correctly fails without affecting the structures.
        - **trieSearchTest:** Tests searching for elements by prefix in both `Trie` and `StringTrie`, ensuring that all relevant keys are returned.
        - **triePerformance:** (Ignored) Compares the performance between `Trie` and `StringTrie` for insertion, deletion, and search operations, and writes the results to a CSV file.
        - **trieMemoryUsage:** (Ignored) Compares the memory usage of `Trie` and `StringTrie` for various sizes, and writes the results to a CSV file.



<br> <hr>

## Team Management

### Meetings Records
- Team meeting minutes can be found here:
    - [Minutes](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/tree/main/items/Meeting%20Minutes?ref_type=heads)

<hr>

### Conflict Resolution Protocol
- Our conflict resolution protocol can be found here:
    - [Conflict Resolution Protocol](https://gitlab.cecs.anu.edu.au/u-------/gp-24s1/-/blob/main/items/conflict-resolution.md?ref_type=heads)









