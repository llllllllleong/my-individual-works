package test.java.com.example.groupproject24s1;

import com.example.groupproject24s1.utils.CategoryQuery;
import com.example.groupproject24s1.utils.Parser;
import com.example.groupproject24s1.utils.SearchQuery;
import com.example.groupproject24s1.utils.Token;
import com.example.groupproject24s1.utils.Tokenizer;
import com.example.groupproject24s1.utils.TypeQuery;
import com.example.groupproject24s1.utils.IDQuery;
import com.example.groupproject24s1.utils.UserQuery;
import com.example.groupproject24s1.utils.TitleQuery;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import java.util.Deque;

/**
 * Test Class used to assess our implementation of the Parse and Tokenizer.
 *
 * @author Leong Yin Chan
 */
public class ParserTokenizerTest {
    private Tokenizer tokenizer;
    private Parser parser;


    @Test
    public void testTokenizerWithUserQuantifier() {
        tokenizer = new Tokenizer("@user123");
        Deque<Token> tokens = tokenizer.tokenize();
        //Should be split into two tokens, the first one being a UserQuantifier, the second one
        //containing the "used123" word token
        assertEquals(2, tokens.size());
        assertEquals(Token.Type.UserQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.peek().getType());
        assertEquals("user123", tokens.poll().getTokenString());
    }

    @Test
    public void testTokenizerWithCategoryQuantifier() {
        tokenizer = new Tokenizer("QC:category");
        Deque<Token> tokens = tokenizer.tokenize();
        //Should be split into a categoryquantifier token and a word token
        assertEquals(2, tokens.size());
        assertEquals(Token.Type.CategoryQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.peek().getType());
        assertEquals("category", tokens.poll().getTokenString());
        //With whitespace
        tokenizer = new Tokenizer("QC: category");
        tokens = tokenizer.tokenize();
        //Should be split into a categoryquantifier token and a word token
        assertEquals(2, tokens.size());
        assertEquals(Token.Type.CategoryQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.peek().getType());
        assertEquals("category", tokens.poll().getTokenString());
    }

    @Test
    public void testTokenizerWithTypeQuantifier() {
        tokenizer = new Tokenizer("QT:type");
        Deque<Token> tokens = tokenizer.tokenize();
        assertEquals(2, tokens.size());
        assertEquals(Token.Type.TypeQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.poll().getType());
    }

    @Test
    public void testTokenizerWithIDQuantifier() {
        tokenizer = new Tokenizer("QID:12345");
        Deque<Token> tokens = tokenizer.tokenize();
        assertEquals(2, tokens.size());
        assertEquals(Token.Type.IDQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Number, tokens.poll().getType());
    }

    @Test
    public void testTokenizerWithNumber() {
        tokenizer = new Tokenizer("12345");
        Deque<Token> tokens = tokenizer.tokenize();
        assertEquals(1, tokens.size());
        assertEquals(Token.Type.Number, tokens.poll().getType());
    }

    @Test
    public void testTokenizerWithWords() {
        tokenizer = new Tokenizer("hello world");
        Deque<Token> tokens = tokenizer.tokenize();
        assertEquals(2, tokens.size());
        assertEquals(Token.Type.Word, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.poll().getType());
    }

    @Test
    public void testParserWithUserQuery() {
        parser = new Parser("@user123");
        SearchQuery query = parser.parse();
        assertTrue(query instanceof UserQuery);
        assertEquals("user123", ((UserQuery) query).getValue());
    }

    @Test
    public void testParserWithCategoryQuery() {
        parser = new Parser("QC:category");
        SearchQuery query = parser.parse();
        assertTrue(query instanceof CategoryQuery);
        assertEquals("category", ((CategoryQuery) query).getValue());
    }

    @Test
    public void testParserWithTypeQuery() {
        parser = new Parser("QT:type");
        SearchQuery query = parser.parse();
        assertTrue(query instanceof TypeQuery);
        assertEquals("type", ((TypeQuery) query).getValue());
    }

    @Test
    public void testParserWithIDQuery() {
        parser = new Parser("QID:12345");
        SearchQuery query = parser.parse();
        assertTrue(query instanceof IDQuery);
        assertEquals("12345", ((IDQuery) query).getValue());
    }

    @Test
    public void testParserWithTitleQuery() {
        parser = new Parser("hello there world");
        SearchQuery query = parser.parse();
        assertTrue(query instanceof TitleQuery);
        assertEquals("hello there world", ((TitleQuery) query).getValue());
    }


    @Test
    public void testTokenizerWithMixedCaseQuantifiers() {
        tokenizer = new Tokenizer("qC:Category qT:Type qID:12345");
        Deque<Token> tokens = tokenizer.tokenize();
        assertEquals(6, tokens.size());
        assertEquals(Token.Type.CategoryQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.poll().getType());
        assertEquals(Token.Type.TypeQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Word, tokens.poll().getType());
        assertEquals(Token.Type.IDQuantifier, tokens.poll().getType());
        assertEquals(Token.Type.Number, tokens.poll().getType());
    }


}
