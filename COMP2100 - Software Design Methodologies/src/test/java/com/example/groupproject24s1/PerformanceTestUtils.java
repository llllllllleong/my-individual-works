package test.java.com.example.groupproject24s1;

import static com.example.groupproject24s1.questions.QuestionFactory.createQuestion;

import com.example.groupproject24s1.questions.Question;
import com.example.groupproject24s1.questions.QuestionType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Helper class, used for testing purposes.
 *
 * Includes methods to generate random questions with and without keys
 * @author Leong Yin Chan
 */
public class PerformanceTestUtils {

    static int questionTitleWordLength = 10;

    static Random random = new Random();
    static final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    static int keyCounter = 1;


    /**
     * Generates a list of random questions, each assigned a unique key.
     *
     * @param count The number of random questions to generate.
     * @return A list of randomly generated questions with unique keys.
     */
    public static List<Question> generateRandomQuestionsWithKey(int count) {
        List<Question> questions = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            questions.add(createRandomQuestionWithKey(i+1));
        }
        return questions;
    }
    /**
     * Generates a list of random questions without assigning keys.
     *
     * @param count The number of random questions to generate.
     * @return A list of randomly generated questions.
     */
    public static List<Question> generateRandomQuestions(int count) {
        List<Question> questions = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            questions.add(createRandomQuestions());
        }
        return questions;
    }
    /**
     * Creates a random question and assigns it a unique key.
     *
     * @param id The unique key to assign to the question.
     * @return A randomly generated question with the assigned key.
     */
    public static Question createRandomQuestionWithKey(int id) {
        String questionID = generateRandomString(10);
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            text.append(generateRandomWord(random.nextInt(4) + 3)); // words of length 3-6 characters
            if (i < 99) text.append(" "); // add space after each word except the last
        }
        List<String> options = generateRandomOptions(4, 10);
        String correctAnswer = generateRandomString(10);
        String category = generateRandomWord(random.nextInt(4) + 3); // single word, 3-6 characters long
        QuestionType[] types = QuestionType.values();
        QuestionType type = types[random.nextInt(types.length)];
        Question output = createQuestion(questionID, text.toString(), options, correctAnswer, category, type,
                false, false, "", "");

        output.setKey(id);
        return output;
    }
    /**
     * Creates a random question without assigning a key.
     *
     * @return A randomly generated question.
     */
    public static Question createRandomQuestions() {
        String questionID = generateRandomString(10);
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            text.append(generateRandomWord(random.nextInt(4) + 3)); // words of length 3-6 characters
            if (i < 99) text.append(" "); // add space after each word except the last
        }
        List<String> options = generateRandomOptions(4, 10);
        String correctAnswer = generateRandomString(10);
        String category = generateRandomWord(random.nextInt(4) + 3); // single word, 3-6 characters long
        QuestionType[] types = QuestionType.values();
        QuestionType type = types[random.nextInt(types.length)];
        Question output = createQuestion(questionID, text.toString(), options, correctAnswer, category, type,
                false, false, "", "");
        return output;
    }
    /**
     * Generates a random word of the specified length.
     *
     * @param length The length of the random word to generate.
     * @return A randomly generated word.
     */
    public static String generateRandomWord(int length) {
        return generateRandomString(length);
    }
    public static List<String> generateRandomOptions(int numOptions, int lengthOfOption) {
        List<String> options = new ArrayList<>();
        for (int i = 0; i < numOptions; i++) {
            options.add(generateRandomString(lengthOfOption));
        }
        return options;
    }
    /**
     * Generates a random string of the specified length.
     *
     * @param length The length of the random string to generate.
     * @return A randomly generated string.
     */
    public static String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(alphabet.charAt(random.nextInt(alphabet.length())));
        }
        return sb.toString();
    }
    /**
     * Generates a random question title and appends a unique key.
     *
     * @return A randomly generated question title with a unique key.
     */
    public static String generateRandomTitleWithKey() {
        String title = generateQuestionTitle();
        title += " #" + keyCounter;
        return title;
    }
    /**
     * Generates a random question title.
     *
     * @return A randomly generated question title.
     */
    public static String generateQuestionTitle() {
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < questionTitleWordLength; i++) {
            text.append(generateRandomWord(random.nextInt(4) + 3));
            if (i < questionTitleWordLength) text.append(" ");
        }
        return text.toString();
    }
    /**
     * Extracts the key from a titled question.
     *
     * @param titledQuestion The titled question from which to extract the key.
     * @return The extracted key.
     */
    public static int extractKey(String titledQuestion) {
        return Integer.parseInt(titledQuestion.substring(titledQuestion.lastIndexOf('#') + 1));
    }
    /**
     * Calculates the used memory of the runtime.
     *
     * @param runtime The runtime to measure memory usage.
     * @return The used memory in bytes.
     */
    static long getUsedMemory(Runtime runtime) {
        return runtime.totalMemory() - runtime.freeMemory();
    }
}
