package test.java.com.example.groupproject24s1;

import static com.example.groupproject24s1.PerformanceTestUtils.generateRandomQuestions;
import static com.example.groupproject24s1.questions.QuestionFactory.createQuestion;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.example.groupproject24s1.datastructures.Database;
import com.example.groupproject24s1.questions.Question;
import com.example.groupproject24s1.questions.QuestionDeserializer;
import com.example.groupproject24s1.questions.QuestionType;
import com.example.groupproject24s1.questions.QuestionsWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
/**
 * Test Class used to assess our implementation of the Database class.
 *
 * @author Leong Yin Chan
 */
public class DataStructureTest {
    Database database = new Database();
    Gson gson;
    List<Question> jsonQuestions;
    List<Question> questionList = new ArrayList<>();
    List<Integer> testSizes;
    int maxTestSize = 5000;
    int incrementSize = 500;


    @Before
    public void setUp() {
        this.database = new Database();
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Question.class, new QuestionDeserializer());
        gson = builder.create();
        // Load questions into a list
        try (Reader reader = new InputStreamReader(Objects.requireNonNull(getClass()
                .getClassLoader()).getResourceAsStream("Questions.JSON"), StandardCharsets.UTF_8)) {
            QuestionsWrapper questionWrapper = gson.fromJson(reader, QuestionsWrapper.class);
            jsonQuestions = questionWrapper.getQuestions();
        } catch (IOException e) {
            throw new RuntimeException("Failed to load questions", e);
        }
        assertNotNull("Question list should not be null after loading", jsonQuestions);
        assertFalse("Question list should not be empty after loading", jsonQuestions.isEmpty());

        int i = 0;
        testSizes = new ArrayList<>();
        while (i < maxTestSize) {
            i += incrementSize;
            testSizes.add(i);
        }
    }

    public void insertJsonQuestions() {
        for (Question q : jsonQuestions) this.database.insert(q);
    }

    public void insertTestQuestions() {
        database.insert(createQuestion("", "Calculus basics?", Arrays.asList("Yes", "No"), "Yes", "Calculus, Basics", QuestionType.MULTIPLE_CHOICE, false, false, "", ""));
        database.insert(createQuestion("", "Advanced Derivatives?", Arrays.asList("True", "False"), "True", "Calculus, Derivatives", QuestionType.TRUE_FALSE, false, false, "", ""));
        database.insert(createQuestion("", "Introduction to Linear Algebra?", Arrays.asList("Answer A", "Answer B"), "Answer A", "Algebra, Test", QuestionType.FILL_IN_THE_BLANK, false, false, "", ""));
        database.insert(createQuestion("", "Physics: Newton's Laws?", Arrays.asList("Answer A", "Answer B"), "Answer A", "Physics, Test", QuestionType.FILL_IN_THE_BLANK, false, false, "", ""));
        database.insert(createQuestion("123", "Specific ID question?", Arrays.asList("Yes", "No"), "Yes", "Specific, IDTest", QuestionType.FILL_IN_THE_BLANK, false, false, "", ""));
        database.insert(createQuestion("", "Search by title example?", Arrays.asList("Answer A", "Answer B"), "Answer B", "TitleSearch, Example", QuestionType.SHORT_ANSWER, false, false, "", ""));
    }


    @Test
    public void databaseInitialisationTest() {
        assertTrue(database.isEmpty());
        assertEquals(0, database.getSize());
    }


    @Test
    public void databaseInsertTest() {
        List<Question> questions = generateRandomQuestions(10);
        for (Question q : questions) database.insert(q);
        assertEquals(10, database.getSize());
    }

    @Test
    public void databaseInsertDuplicatesTest() {
        Question question = generateRandomQuestions(1).get(0);
        database.insert(question);
        database.insert(question);
        assertEquals(1, database.getSize());
    }

    @Test
    public void databaseDeleteTest() {
        List<Question> questions = generateRandomQuestions(5);
        for (Question q : questions) database.insert(q);
        Question toDelete = questions.get(0);
        assertTrue(database.delete(toDelete.getKey()));
        assertEquals(4, database.getSize());
        assertFalse(database.containsQuestion(toDelete));
        database.deleteAll();
        insertTestQuestions();
        assertTrue("Database should have a category Example, " +
                "from loading the test questions", database.getCategories().contains("Example"));
        Question q = database.searchQuestionsFromTitle("Search by titl").get(0);
        Integer key = q.getKey();
        database.delete(key);
        assertFalse("Database should not have the category, after deleting the " +
                "one and only question which was categorised as Example", database.getCategories().contains("Example"));
    }

    @Test
    public void databaseUnknownQuestion() {
        List<Question> questions = generateRandomQuestions(5);
        for (Question q : questions) database.insert(q);
        assertFalse(database.containsKey(999));
        assertFalse(database.delete(999));
        List<Question> searchResult1 = database.searchQuestions("QID: 999");
        List<Question> searchResult2 = database.searchQuestions("QID: 999");
        assertTrue(searchResult1.isEmpty());
        assertTrue(searchResult2.isEmpty());
    }

    @Test
    public void databaseSearchTest() {
        this.database = new Database();
        insertJsonQuestions();
        insertTestQuestions();
        List<Question> results = database.searchQuestions("QC: Physics");
        assertFalse("Should find a question under category: Physics", results.isEmpty());
    }


    @Ignore("This test was used to determine a suitable value for the levenshteinMatcher threshold")
    @Test
    public void levenshteinMatcherTest() {
        insertJsonQuestions();
        String[] matchToArray = {"Calculus", "Calculator", "Algebra, Linear Algebra"};
        List<String> matchToList = new ArrayList<>(Arrays.asList(matchToArray));
        System.out.println("Testing search query: Calculus");
        System.out.println(database.levenshteinMatcher("c", matchToList));
        System.out.println(database.levenshteinMatcher("ca", matchToList));
        System.out.println(database.levenshteinMatcher("cal", matchToList));
        System.out.println(database.levenshteinMatcher("calc", matchToList));
        System.out.println(database.levenshteinMatcher("calcu", matchToList));
        System.out.println(database.levenshteinMatcher("calcul", matchToList));
        System.out.println(database.levenshteinMatcher("calculu", matchToList));
        System.out.println(database.levenshteinMatcher("calculus", matchToList));
        System.out.println("Testing search query: Calculator");
        System.out.println(database.levenshteinMatcher("c", matchToList));
        System.out.println(database.levenshteinMatcher("ca", matchToList));
        System.out.println(database.levenshteinMatcher("cal", matchToList));
        System.out.println(database.levenshteinMatcher("calc", matchToList));
        System.out.println(database.levenshteinMatcher("calcu", matchToList));
        System.out.println(database.levenshteinMatcher("calcul", matchToList));
        System.out.println(database.levenshteinMatcher("calcula", matchToList));
        System.out.println(database.levenshteinMatcher("calculat", matchToList));
        System.out.println(database.levenshteinMatcher("calculato", matchToList));
        System.out.println(database.levenshteinMatcher("calculator", matchToList));
        String[] matchToArray2 = {"Multi Choice", "Multiple Choice", "MCQ", "Short Answer", "TorF", "True or False", "Fill in the blank", "FITB"};
        List<String> matchToList2 = new ArrayList<>(Arrays.asList(matchToArray2));
        System.out.println("Testing search query: mcq");
        System.out.println(database.levenshteinMatcher("m", matchToList2));
        System.out.println(database.levenshteinMatcher("mc", matchToList2));
        System.out.println(database.levenshteinMatcher("mcq", matchToList2));
        System.out.println("Testing search query: MCQ");
        System.out.println(database.levenshteinMatcher("M", matchToList2));
        System.out.println(database.levenshteinMatcher("MC", matchToList2));
        System.out.println(database.levenshteinMatcher("MCQ", matchToList2));
        System.out.println("Testing search query: 123");
        System.out.println(database.levenshteinMatcher("1", matchToList2));
        System.out.println(database.levenshteinMatcher("12", matchToList2));
        System.out.println(database.levenshteinMatcher("123", matchToList2));
        System.out.println("Testing search query: tor");
        System.out.println(database.levenshteinMatcher("t", matchToList2));
        System.out.println(database.levenshteinMatcher("to", matchToList2));
        System.out.println(database.levenshteinMatcher("tor", matchToList2));


    }

}