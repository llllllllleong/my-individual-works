package test.java.com.example.groupproject24s1;

import static com.example.groupproject24s1.PerformanceTestUtils.generateRandomQuestionsWithKey;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.example.groupproject24s1.datastructures.AVLMutable;
import com.example.groupproject24s1.questions.Question;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
/**
 * Test Class, used to test the feasibility of using an Mutable AVL tree as the main storage
 * data structure.
 *
 * @author Leong Yin Chan
 */

public class AVLMutableTest {

    AVLMutable<Question> mAVL;

    @Before
    public void setUp() {
        mAVL = new AVLMutable<>();
    }

    @Test
    public void mutableAVLInsertAndContainsKeyTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2);
        Question q1 = questions.get(0);
        Question q2 = questions.get(1);
        mAVL.insert(q1);
        assertTrue(mAVL.containsKey(q1.getKey()));
        assertFalse(mAVL.containsKey(q2.getKey()));
        mAVL.insert(q2);
        assertTrue(mAVL.containsKey(q2.getKey()));
    }

    @Test
    public void mutableAVLGetTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2);
        Question q1 = questions.get(0);
        Question q2 = questions.get(1);
        mAVL.insert(q1);
        assertEquals(q1, mAVL.get(q1.getKey()));
        assertNull(mAVL.get(q2.getKey()));
        mAVL.insert(q2);
        assertEquals(q2, mAVL.get(q2.getKey()));
    }

    @Test
    public void mutableAVLSafeDeleteTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2);
        Question q1 = questions.get(0);
        Question q2 = questions.get(1);
        mAVL.insert(q1);
        assertTrue(mAVL.containsKey(q1.getKey()));
        assertTrue(mAVL.safeDelete(q1.getKey()));
        assertFalse(mAVL.containsKey(q1.getKey()));
        assertFalse(mAVL.safeDelete(q2.getKey()));
    }

    @Test
    public void mutableAVLIsEmptyAndDeleteAllTest() {
        assertTrue(mAVL.isEmpty());
        List<Question> questions = generateRandomQuestionsWithKey(1);
        Question q1 = questions.get(0);
        mAVL.insert(q1);
        assertFalse(mAVL.isEmpty());
        mAVL.deleteAll();
        assertTrue(mAVL.isEmpty());
    }

    @Test
    public void mutableAVLGetAllTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2);
        Question q1 = questions.get(0);
        Question q2 = questions.get(1);
        mAVL.insert(q1);
        mAVL.insert(q2);
        List<Question> allQuestions = mAVL.getAll();
        assertTrue(allQuestions.contains(q1));
        assertTrue(allQuestions.contains(q2));
        assertEquals(2, allQuestions.size());
    }

    @Test
    public void mutableAVLSizeTest() {
        assertEquals(0, mAVL.size());
        List<Question> questions = generateRandomQuestionsWithKey(2);
        Question q1 = questions.get(0);
        Question q2 = questions.get(1);
        mAVL.insert(q1);
        assertEquals(1, mAVL.size());
        mAVL.insert(q2);
        assertEquals(2, mAVL.size());
        mAVL.delete(q1.getKey());
        assertEquals(1, mAVL.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void mutableAVLInsertNullTest() {
        mAVL.insert(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void mutableAVLDeleteFromEmptyTreeTest() {
        mAVL.delete(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void mutableAVLDeleteNullKeyTest() {
        List<Question> questions = generateRandomQuestionsWithKey(1);
        Question q1 = questions.get(0);
        mAVL.insert(q1);
        mAVL.delete(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void mutableAVLGetNullKeyTest() {
        mAVL.get(null);
    }

    @Test
    public void mutableAVLLargeScaleInsertionTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2500);
        for (Question q : questions) {
            mAVL.insert(q);
        }
        assertEquals(2500, mAVL.size());
        for (Question q : questions) {
            assertTrue(mAVL.containsKey(q.getKey()));
        }
    }

    @Test
    public void mutableAVLLargeScaleSearchTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2500);
        for (Question q : questions) {
            mAVL.insert(q);
        }
        for (Question q : questions) {
            Question retrieved = mAVL.get(q.getKey());
            assertEquals(q, retrieved);
        }
    }

    @Test
    public void mutableAVLLargeScaleDeletionTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2500);
        for (Question q : questions) {
            mAVL.insert(q);
        }
        assertEquals(2500, mAVL.size());
        for (Question q : questions) {
            assertTrue(mAVL.delete(q.getKey()));
        }
        assertEquals(0, mAVL.size());
        for (Question q : questions) {
            assertFalse(mAVL.containsKey(q.getKey()));
        }
    }

    @Test
    public void mutableAVLLargeScaleCombinedTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2500);
        for (Question q : questions) {
            mAVL.insert(q);
        }
        assertEquals(2500, mAVL.size());
        for (Question q : questions) {
            Question retrieved = mAVL.get(q.getKey());
            assertEquals(q, retrieved);
        }
        for (Question q : questions) {
            assertTrue(mAVL.delete(q.getKey()));
        }
        assertEquals(0, mAVL.size());
        for (Question q : questions) {
            mAVL.insert(q);
        }
        assertEquals(2500, mAVL.size());
        for (Question q : questions) {
            Question retrieved = mAVL.get(q.getKey());
            assertEquals(q, retrieved);
        }
    }

    @Test
    public void mutableAVLLargeScaleCombinedBalanceTest() {
        List<Question> questions = generateRandomQuestionsWithKey(2500);
        for (Question q : questions) {
            mAVL.insert(q);
        }
        assertTrue(isBalanced(mAVL.getRoot()));
        for (int i = 0; i < 1250; i++) {
            mAVL.delete(questions.get(i).getKey());
        }
        assertTrue(isBalanced(mAVL.getRoot()));
        List<Question> newQuestions = generateRandomQuestionsWithKey(1250);
        for (Question q : newQuestions) {
            mAVL.insert(q);
        }
        assertTrue(isBalanced(mAVL.getRoot()));
    }

    private boolean isBalanced(AVLMutable.AVLNode<Question> node) {
        if (node == null) {
            return true;
        }
        int balanceFactor = getBalanceFactor(node);
        if (balanceFactor < -1 || balanceFactor > 1) {
            return false;
        }
        return isBalanced(node.getLeftNode()) && isBalanced(node.getRightNode());
    }

    private int getBalanceFactor(AVLMutable.AVLNode<Question> node) {
        if (node == null) {
            return 0;
        }
        return getHeight(node.getLeftNode()) - getHeight(node.getRightNode());
    }

    private int getHeight(AVLMutable.AVLNode<Question> node) {
        return (node == null) ? -1 : node.height;
    }
}
