package test.java.com.example.groupproject24s1;

import static com.example.groupproject24s1.PerformanceTestUtils.extractKey;
import static com.example.groupproject24s1.PerformanceTestUtils.generateRandomQuestionsWithKey;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

import com.example.groupproject24s1.datastructures.AVLMutable;
import com.example.groupproject24s1.datastructures.AVLNonMutable;
import com.example.groupproject24s1.datastructures.EmptyTreeNonMutable;
import com.example.groupproject24s1.questions.Question;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Test class used to compare the performance between a Non-Mutable AVL and a Mutable AVL
 *
 * @author Leong Yin Chan
 */

public class AVLPerformanceTest extends DataStructureTest {

    List<Integer> treeSizes;
    int insertionRetrievalCount;
    int deletionCount;
    AVLNonMutable<Question> nmAVL;
    AVLMutable<Question> mAVL;

    @Before
    public void setUp() {
        treeSizes = new ArrayList<>();
        int i = 0;
        while (i < 50000) {
            i += 2500;
            treeSizes.add(i);
        }
        insertionRetrievalCount = 100;
        deletionCount = 100;
        nmAVL = new EmptyTreeNonMutable<>();
        mAVL = new AVLMutable<>();
    }

    @Test
    public void demonstrateHowToUseAVL() throws IOException {
        List<Question> questionList = generateRandomQuestionsWithKey(100);
        Question q = questionList.get(0);
        Integer key = q.getKey();
        // To insert
        nmAVL = nmAVL.insert(q);
        mAVL.insert(q);
        // To search
        nmAVL.get(key);
        mAVL.get(key);
        // To safe-delete
        nmAVL = nmAVL.safeDelete(key);
        mAVL.safeDelete(key);
    }

    @Ignore("This test compares the performance between the non-mutable AVL and the mutable AVL. It " +
            "compares the insertion, deletion, and search time for a range of tree sizes, and prints the " +
            "result into a CSV.")
    @Test
    public void avlPerformance() throws IOException {
        FileWriter csvWriter = new FileWriter("avlPerformance.csv");
        csvWriter.append("TestSize, NonMutable Insert (ms), Mutable Insert (ms), NonMutable Delete (ms), Mutable Delete (ms), NonMutable Search (ms), Mutable Search (ms)\n");
        int currentSize = 0;
        for (int size : treeSizes) {
            List<Question> questions = generateRandomQuestionsWithKey(size - currentSize);
            List<Question> testQuestions = generateRandomQuestionsWithKey(insertionRetrievalCount);
            List<Question> deleteQuestions = questions.subList(0, deletionCount);
            currentSize = size;
            for (Question q : questions) {
                nmAVL = nmAVL.insert(q);
                mAVL.insert(q);
            }

            // Insertions
            double nmInsertTime = avlInsertPerformance(nmAVL, testQuestions);
            double mInsertTime = avlInsertPerformance(mAVL, testQuestions);

            // Searches
            double nmSearchTime = avlSearchPerformance(nmAVL, testQuestions);
            double mSearchTime = avlSearchPerformance(mAVL, testQuestions);

            // Deletions
            double nmDeleteTime = avlDeletePerformance(nmAVL, deleteQuestions);
            double mDeleteTime = avlDeletePerformance(mAVL, deleteQuestions);

            csvWriter.append(String.format("%d, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f\n",
                    size,
                    nmInsertTime, mInsertTime,
                    nmDeleteTime, mDeleteTime,
                    nmSearchTime, mSearchTime));
        }

        csvWriter.flush();
        csvWriter.close();
    }

    @Ignore("This test measures the memory usage of the non-mutable AVL tree.")
    @Test
    public void nmAVLMemoryUsagePerformance() throws IOException {
        FileWriter csvWriter = new FileWriter("nmAVLMemoryUsage.csv");
        csvWriter.append("TestSize, NonMutable Memory (MB)\n");
        Runtime runtime = Runtime.getRuntime();

        for (int size : treeSizes) {
            System.out.println(size);
            List<Question> questions = generateRandomQuestionsWithKey(size);
            // Non-Mutable AVL Memory Usage
            nmAVL = new EmptyTreeNonMutable<>();
            runtime.gc();
            long beforeMemoryNmAVL = getUsedMemory(runtime);
            for (Question question : questions) {
                nmAVL = nmAVL.insert(question);
            }
            long afterMemoryNmAVL = getUsedMemory(runtime);
            double nmAVLMemoryUsage = (afterMemoryNmAVL - beforeMemoryNmAVL) / 1024.0 / 1024.0;

            csvWriter.append(String.format("%d, %.10f\n", size, nmAVLMemoryUsage));
        }

        csvWriter.flush();
        csvWriter.close();
    }

    @Ignore("This test measures the memory usage of the mutable AVL tree.")
    @Test
    public void mAVLMemoryUsagePerformance() throws IOException {
        FileWriter csvWriter = new FileWriter("mAVLMemoryUsage.csv");
        csvWriter.append("TestSize, Mutable Memory (MB)\n");
        Runtime runtime = Runtime.getRuntime();

        for (int size : treeSizes) {
            System.out.println(size);
            // Mutable AVL Memory Usage
            List<Question> questions = generateRandomQuestionsWithKey(size);
            mAVL = new AVLMutable<>();
            runtime.gc();
            long beforeMemoryMAVL = getUsedMemory(runtime);
            for (Question question : questions) {
                mAVL.insert(question);
            }
            long afterMemoryMAVL = getUsedMemory(runtime);
            double mAVLMemoryUsage = (afterMemoryMAVL - beforeMemoryMAVL) / 1024.0 / 1024.0;

            csvWriter.append(String.format("%d, %.10f\n", size, mAVLMemoryUsage));
        }
        csvWriter.flush();
        csvWriter.close();
    }

    private double avlInsertPerformance(AVLNonMutable<Question> avl, List<Question> questions) {
        long start = System.nanoTime();
        for (Question question : questions) {
            avl = avl.insert(question);
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double avlInsertPerformance(AVLMutable<Question> avl, List<Question> questions) {
        long start = System.nanoTime();
        for (Question question : questions) {
            avl.insert(question);
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double avlDeletePerformance(AVLNonMutable<Question> avl, List<Question> questions) {
        long start = System.nanoTime();
        for (Question question : questions) {
            avl = avl.safeDelete(question.getKey());
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double avlDeletePerformance(AVLMutable<Question> avl, List<Question> questions) {
        long start = System.nanoTime();
        for (Question question : questions) {
            avl.safeDelete(question.getKey());
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double avlSearchPerformance(AVLNonMutable<Question> avl, List<Question> questions) {
        long start = System.nanoTime();
        for (Question question : questions) {
            avl.get(question.getKey());
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double avlSearchPerformance(AVLMutable<Question> avl, List<Question> questions) {
        long start = System.nanoTime();
        for (Question question : questions) {
            avl.get(question.getKey());
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private long getUsedMemory(Runtime runtime) {
        return runtime.totalMemory() - runtime.freeMemory();
    }
}
