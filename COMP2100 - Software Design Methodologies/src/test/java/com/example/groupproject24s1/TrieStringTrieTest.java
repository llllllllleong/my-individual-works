package test.java.com.example.groupproject24s1;


import static com.example.groupproject24s1.PerformanceTestUtils.extractKey;
import static com.example.groupproject24s1.PerformanceTestUtils.generateRandomTitleWithKey;
import static com.example.groupproject24s1.PerformanceTestUtils.generateRandomWord;
import static com.example.groupproject24s1.PerformanceTestUtils.getUsedMemory;
import static com.example.groupproject24s1.PerformanceTestUtils.keyCounter;
import static com.example.groupproject24s1.PerformanceTestUtils.random;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.example.groupproject24s1.datastructures.StringTrie;
import com.example.groupproject24s1.datastructures.Trie;
import com.example.groupproject24s1.datastructures.TrieInterface;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Test Class, used to test the feasibility a char Trie and a String Trie
 * data structure.
 *
 * @author Leong Yin Chan
 */

public class TrieStringTrieTest {
    StringTrie st;
    Trie trie;
    List<Integer> testSizes;
     int maxTestSize = 50000;
     int incrementSize = 1000;

     int performanceTestInsertionRetrievalCount = 10;
     int performanceTestSearchCount = 1;


    @Before
    public void setUp() {
        st = new StringTrie(2);
        trie = new Trie();
        int i = 0;
        testSizes = new ArrayList<>();
        while (i < maxTestSize) {
            i += incrementSize;
            testSizes.add(i);
        }
    }


    @Test
    public void trieSingleInsertTest() {
        String testString = generateRandomTitleWithKey();
        int key = extractKey(testString);
        st.insert(testString, key);
        trie.insert(testString, key);
        assertTrue("String should be in StringTrie after insertion", st.contains(testString));
        assertTrue("String should be in Trie after insertion", trie.contains(testString));
    }

    @Test
    public void trieInsertionTest() {
        for (int i = 0; i < 1000; i++) {
            String testString = generateRandomTitleWithKey();
            int key = extractKey(testString);
            st.insert(testString, key);
            trie.insert(testString, key);
            assertTrue("String should be present in StringTrie", st.contains(testString));
            assertTrue("String should be present in Trie", trie.contains(testString));
        }
    }


    @Test
    public void trieDeleteExistingTest() {
        insertRandomQuestions(1000);
        String testString = generateRandomTitleWithKey();
        int key = extractKey(testString);
        st.insert(testString, key);
        trie.insert(testString, key);
        assertTrue("Deletion should be successful in StringTrie", st.delete(testString, key));
        assertTrue("Deletion should be successful in Trie", trie.delete(testString, key));
        assertFalse("String should no longer be present in StringTrie", st.search(testString).contains(key));
        assertFalse("String should no longer be present in Trie", trie.search(testString).contains(key));
    }


    @Test
    public void trieDeleteUnknownTest() {
        insertRandomQuestions(1000);
        String testString = "unknownstring #99999";
        int key = extractKey(testString);
        assertFalse("Deletion should fail in StringTrie", st.delete(testString, key));
        assertFalse("Deletion should fail in Trie", trie.delete(testString, key));
    }

    @Test
    public void trieSearchTest() {
        String prefix = "test";
        List<Integer> keys = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String testString = prefix + " " + generateRandomWord(random.nextInt(4) + 3) + " #" + keyCounter;
            int key = keyCounter++;
            st.insert(testString, key);
            trie.insert(testString, key);
            keys.add(key);
        }

        List<Integer> stResults = st.search(prefix);
        List<Integer> trieResults = trie.search(prefix);
        assertTrue("All keys should be returned for the prefix in StringTrie", stResults.containsAll(keys));
        assertTrue("All keys should be returned for the prefix in Trie", trieResults.containsAll(keys));
    }


    @Ignore("This test compares the performance between the char Trie and the String Trie. It " +
            "compares the insertion and deletion time for a range of trie sizes, and prints the " +
            "result into a csv.")
    @Test
    public void triePerformance() throws IOException {
        FileWriter csvWriter = new FileWriter("triePerformance.csv");
        csvWriter.append("TestSize, Trie Insert (ms), StringTrie Insert (ms), Trie Delete (ms), StringTrie Delete (ms), Trie Search (ms), StringTrie Search (ms)\n");
        for (int size : testSizes) {
            trie = new Trie();
            st = new StringTrie(2);
            insertRandomQuestions(size);
            List<String> testStrings = new ArrayList<>();
            List<Integer> keys = new ArrayList<>();
            for (int i = 0; i < performanceTestInsertionRetrievalCount; i++) {
                String testString = generateRandomTitleWithKey();
                int key = extractKey(testString);
                testStrings.add(testString);
                keys.add(key);
            }
            double trieInsertTime = trieInsertPerformance(trie, testStrings, keys);
            double stringTrieInsertTime = trieInsertPerformance(st, testStrings, keys);
            double trieDeleteTime = trieDeletePerformance(trie, testStrings, keys);
            double stringTrieDeleteTime = trieDeletePerformance(st, testStrings, keys);
            double trieSearchTime = trieSearchPerformance(trie, "test");
            double stringTrieSearchTime = trieSearchPerformance(st, "test");
            System.out.println(size + ", " + trieInsertTime + ", " + stringTrieInsertTime + ", " + trieDeleteTime + ", " + stringTrieDeleteTime + ", " + trieSearchTime + ", " + stringTrieSearchTime);
            csvWriter.append(String.format("%d, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f\n",
                    size,
                    trieInsertTime, stringTrieInsertTime,
                    trieDeleteTime, stringTrieDeleteTime,
                    trieSearchTime, stringTrieSearchTime));
        }

        csvWriter.flush();
        csvWriter.close();
    }

    private double trieInsertPerformance(TrieInterface t, List<String> testStrings, List<Integer> keys) {
        long start = System.nanoTime();
        for (int i = 0; i < performanceTestInsertionRetrievalCount; i++) {
            t.insert(testStrings.get(i), keys.get(i));
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double trieDeletePerformance(TrieInterface t, List<String> testStrings, List<Integer> keys) {
        long start = System.nanoTime();
        for (int i = 0; i < performanceTestInsertionRetrievalCount; i++) {
            t.delete(testStrings.get(i), keys.get(i));
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }

    private double trieSearchPerformance(TrieInterface t, String prefix) {
        long start = System.nanoTime();
        for (int i = 0; i < performanceTestSearchCount; i++) {
            t.search(prefix);
        }
        return (System.nanoTime() - start) / 1_000_000.0;
    }



    @Ignore("This test compares the performance between the char Trie and the String Trie. It " +
            "compares the memory usage for a range of trie sizes, and prints the result into a csv.")
    @Test
    public void trieMemoryUsage() throws IOException {
        FileWriter csvWriter = new FileWriter("trieMemoryUsage.csv");
        csvWriter.append("TestSize, Trie Memory (MB), StringTrie Memory (MB)\n");
        Runtime runtime = Runtime.getRuntime();

        for (int size : testSizes) {
            System.out.println(size);
            trie = new Trie();
            st = new StringTrie(2);
            runtime.gc();
            long beforeMemoryTrie = getUsedMemory(runtime);
            insertRandomQuestions(trie, size);
            long afterMemoryTrie = getUsedMemory(runtime);
            double trieMemoryUsage = (afterMemoryTrie - beforeMemoryTrie) / 1024.0 / 1024.0;

            runtime.gc();
            long beforeMemoryStringTrie = getUsedMemory(runtime);
            insertRandomQuestions(st, size);
            long afterMemoryStringTrie = getUsedMemory(runtime);
            double stringTrieMemoryUsage = (afterMemoryStringTrie - beforeMemoryStringTrie) / 1024.0 / 1024.0;

            csvWriter.append(String.format("%d, %.6f, %.6f\n", size, trieMemoryUsage, stringTrieMemoryUsage));
        }
        csvWriter.flush();
        csvWriter.close();
    }






    public void insertRandomQuestions(int count) {
        for (int i = 0; i < count; i++) {
            String testString = generateRandomTitleWithKey();
            int key = extractKey(testString);
            st.insert(testString, key);
            trie.insert(testString, key);
        }
    }

    private void insertRandomQuestions(TrieInterface trie, int count) {
        for (int i = 0; i < count; i++) {
            String testString = generateRandomTitleWithKey();
            int key = extractKey(testString);
            trie.insert(testString, key);
        }
    }


}
