package main.java.com.example.groupproject24s1.utils;

import java.util.*;

/**
 * Search query parser, according to the following token grammar
 * <SearchQuery>      ::= <UserQuery> | <CategoryQuery> | <TypeQuery> | <IDQuery> | <TitleQuery>
 *
 * <UserQuery>        ::= (UserQuantifier Token) (Word Token)
 * <CategoryQuery>    ::= (CategoryQuantifier Token) (Word Token)
 * <TypeQuery>        ::= (TypeQuantifier Token) (Word Token)
 * <IDQuery>          ::= (IDQuantifier Token) (Number Token)
 * <TitleQuery>       ::= (Word Token) | (Word Token) <TitleQuery>
 *
 * @author Leong Yin Chan
 */
public class Parser {

    public Tokenizer tokenizer;
    public Deque<Token> tokenQueue;
    public SearchQuery searchQuery;


    /**
     * Custom exception for handling illegal grammar
     */
    public static class IllegalParseGrammar extends IllegalArgumentException {
        public IllegalParseGrammar(String errorMessage) {
            super(errorMessage);
        }
    }


    public Parser(String input) {
        this.tokenizer = new Tokenizer(input);
        this.tokenQueue = tokenizer.tokenize();
    }

    public SearchQuery parse() {
        searchQuery = null;
        try {
            searchQueryParse();
        } catch (IllegalParseGrammar e) {
            //Optional catch block to deal with illegal grammar
            //Current implementation allows for the query before the grammar, to still be
            //searched.
        }
        return searchQuery;
    }


    public void searchQueryParse() throws IllegalParseGrammar {
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at searchQueryParse");
        Token quantiferToken = tokenQueue.peek();
        Token.Type type = quantiferToken.getType();
        if (type == Token.Type.UserQuantifier) {
            userQueryParse();
        } else if (type == Token.Type.CategoryQuantifier) {
            categoryQueryParse();
        } else if (type == Token.Type.TypeQuantifier) {
            typeQueryParse();
        } else if (type == Token.Type.IDQuantifier) {
            idQueryParse();
        } else {
            titleQueryParse();
        }
    }

    public void userQueryParse() throws IllegalParseGrammar {
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at userQueryParse");
        Token quantiferToken = tokenQueue.poll();
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at userQueryParse");
        Token wordToken = tokenQueue.poll();
        searchQuery = new UserQuery(wordToken.getTokenString());
        if (!tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Extra tokens at userQueryParse");
    }


    public void categoryQueryParse() throws IllegalParseGrammar {
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at categoryQueryParse");
        Token quantiferToken = tokenQueue.poll();
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at categoryQueryParse");
        Token wordToken = tokenQueue.poll();
        searchQuery = new CategoryQuery(wordToken.getTokenString());
        if (!tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Extra tokens at categoryQueryParse");
    }

    public void typeQueryParse() throws IllegalParseGrammar {
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at typeQueryParse");
        Token quantiferToken = tokenQueue.poll();
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at typeQueryParse");
        Token wordToken = tokenQueue.poll();
        searchQuery = new TypeQuery(wordToken.getTokenString());
        if (!tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Extra tokens at typeQueryParse");
    }


    public void idQueryParse() throws IllegalParseGrammar {
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at idQueryParse");
        Token quantiferToken = tokenQueue.poll();
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at idQueryParse");
        Token wordToken = tokenQueue.poll();
        if (wordToken.getType() != Token.Type.Number)
            throw new IllegalParseGrammar("Non-Number token at idQueryParse");
        searchQuery = new IDQuery(wordToken.getTokenString());
        if (!tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Illegal token at idQueryParse");
    }

    public void titleQueryParse() throws IllegalParseGrammar {
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at titleQueryParse");
        Token quantiferToken = tokenQueue.poll();
        if (tokenQueue.isEmpty())
            throw new IllegalParseGrammar("Empty token queue at titleQueryParse");
        TitleQuery tq = new TitleQuery(quantiferToken.getTokenString());
        boolean flag = false;
        while (!tokenQueue.isEmpty()) {
            Token wordToken = tokenQueue.poll();
            if (wordToken.getType() != Token.Type.Word) {
                flag = true;
                break;
            } else {
                tq.appendQuery(wordToken.getTokenString());
            }
        }
        searchQuery = tq;
        if (flag)
            throw new IllegalParseGrammar("Non-Word token at titleQueryParse");
    }


}


