package main.java.com.example.groupproject24s1.utils;
/**
 * Abstract class representing a search query.
 *
 * @author Leong Yin Chan
 */
public abstract class SearchQuery {
    public abstract String getValue();
    @Override
    public abstract String toString();
}

