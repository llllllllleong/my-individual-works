package main.java.com.example.groupproject24s1.utils;
/**
 * Extension of the search query abstract class.
 *
 * @author Leong Yin Chan
 */

public class CategoryQuery extends SearchQuery {
    private String word;
    public CategoryQuery(String word) {
        this.word = word;
    }
    @Override
    public String getValue() {
        return word;
    }
    @Override
    public String toString() {
        return "Category: " + word;
    }
}
