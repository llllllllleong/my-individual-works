package main.java.com.example.groupproject24s1.utils;
/**
 * Extension of the search query abstract class.
 *
 * @author Leong Yin Chan
 */


public class IDQuery extends SearchQuery {
    private String word;
    public IDQuery(String word) {
        this.word = word;
    }
    public Integer getIntegerValue() {
        return Integer.valueOf(word);
    }
    @Override
    public String getValue() {
        return word;
    }
    @Override
    public String toString() {
        return "ID: " + word;
    }
}
