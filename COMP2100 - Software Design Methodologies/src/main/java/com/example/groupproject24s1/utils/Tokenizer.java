package main.java.com.example.groupproject24s1.utils;


import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Search query tokenizer, according to the following string regex:
 * <UserQuantifier Token>          ::= "@"
 * <CategoryQuantifier Token>      ::= "QC:"
 * <TypeQuantifier Token>          ::= "QT:"
 * <IDQuantifier Token>            ::= "QID:"
 * <Number Token>                  ::= A sequence of digits
 * <Word Token>                    ::= A sequence of alphanumeric characters
 *
 * @author Leong Yin Chan
 */
public class Tokenizer {
    private String inputString;
    private Deque<Token> tokenQueue;
    /**
     * Constructs a new Tokenizer with the specified input string.
     *
     * @param input The input string to be tokenized.
     */

    public Tokenizer(String input) {
        this.inputString = input.trim();
        this.tokenQueue = new ArrayDeque<>();
    }
    /**
     * Tokenizes the input string into a queue of tokens.
     *
     * @return A dequeue of tokens identified from the input string.
     */
    public Deque<Token> tokenize() {
        if (inputString == null || inputString.isEmpty()) return tokenQueue;

        String patternString = "(?i)(@|QC:|QT:|QID:|\\d+|\\w+)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(inputString);

        while (matcher.find()) {
            String match = matcher.group();

            if (match.matches("@")) {
                tokenQueue.add(new Token(Token.Type.UserQuantifier, match));
            } else if (match.toLowerCase().matches("qc:")) {
                tokenQueue.add(new Token(Token.Type.CategoryQuantifier, match));
            } else if (match.toLowerCase().matches("qt:")) {
                tokenQueue.add(new Token(Token.Type.TypeQuantifier, match));
            } else if (match.toLowerCase().matches("qid:")) {
                tokenQueue.add(new Token(Token.Type.IDQuantifier, match));
            } else if (match.matches("\\d+")) {
                tokenQueue.add(new Token(Token.Type.Number, match));
            } else if (match.matches("\\w+")) {
                tokenQueue.add(new Token(Token.Type.Word, match));
            }
        }

        return tokenQueue;
    }
}