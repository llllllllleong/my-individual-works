package main.java.com.example.groupproject24s1.utils;
/**
 * Extension of the search query abstract class.
 *
 * @author Leong Yin Chan
 */


public class TitleQuery extends SearchQuery {
    private String word;
    public TitleQuery(String word) {
        this.word = word;
    }

    public void appendQuery(String additionalWord) {
        this.word += " " + additionalWord;
    }
    @Override
    public String getValue() {
        return word;
    }
    @Override
    public String toString() {
        return "Title: " + word;
    }
}
