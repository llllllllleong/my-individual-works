package main.java.com.example.groupproject24s1.datastructures;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;



/**
 * This class represents a trie data structure, but instead of using char, it uses Strings.
 * It was created to address the char Trie's issues with scalable memory usage
 *
 * This class has not been refined, and there are is no documentation because it is unused.
 *
 * @author Leong Yin Chan
 */

public class StringTrie implements TrieInterface {

    HashMap<String, StringTrie> map;
    int levenshteinThreshold;
    List<Integer> keyList;


    public StringTrie(int levenshteinThreshold) {
        this.map = new HashMap<>();
        this.keyList = new ArrayList<>();
        this.levenshteinThreshold = levenshteinThreshold;
    }


    public void setLevenshteinThreshold(int i) {
        this.levenshteinThreshold = i;
    }

    public String[] inputStringFilter(String s) {
        s = s.trim();
        s = s.toLowerCase();
        s = s.replaceAll("[^a-z ]", "");
        String[] sArray = s.split(" ");
        return s.toLowerCase().replaceAll("[^a-z ]", "").split(" ");
    }


    public void insert(String s, Integer key) {
        String[] sArray = inputStringFilter(s);
        insert(sArray, 0, key);
    }

    public void insert(String[] sArray, int sIndex, Integer key) {
        if (sIndex == sArray.length) keyList.add(key);
        else {
            String s = sArray[sIndex];
            if (!map.containsKey(s)) map.put(s, new StringTrie(levenshteinThreshold));
            map.get(s).insert(sArray, sIndex + 1, key);
        }
    }

    public boolean contains(String s) {
        String[] sArray = inputStringFilter(s);
        return contains(sArray, 0);
    }
    public boolean contains(String[] sArray, int sIndex) {
        if (sIndex == sArray.length) {
            return true;
        } else {
            String s = sArray[sIndex];
            if (!map.containsKey(s)) return false;
            return map.get(s).contains(sArray, sIndex + 1);
        }
    }

    public boolean delete(String s, Integer key) {
        String[] sArray = inputStringFilter(s);
        return delete(sArray, 0, key);
    }

    public boolean delete(String[] sArray, int sIndex, Integer key) {
        if (sIndex == sArray.length) {
            return keyList.remove(key);
        } else {
            String s = sArray[sIndex];
            if (!map.containsKey(s)) return false;
            return map.get(s).delete(sArray, sIndex + 1, key);
        }
    }

    public List<Integer> search(String s) {
        String[] sArray = inputStringFilter(s);
        List<Integer> output = looseSearch(sArray, 0);
        return output;
    }

    public List<Integer> looseSearch(String[] sArray, int sIndex) {
        if (sIndex == sArray.length) {
            List<Integer> output = new ArrayList<>();
            autoFill(output);
            return output;
        } else {
            String s = sArray[sIndex];
            if (!map.containsKey(s)) s = levenshteinMatcher(s, new ArrayList<>(map.keySet()));
            if (!map.containsKey(s)) return new ArrayList<>();
            return map.get(s).looseSearch(sArray, sIndex + 1);
        }
    }

    public void autoFill(List<Integer> output) {
        if (!keyList.isEmpty()) output.addAll(keyList);
        for (StringTrie st : map.values()) st.autoFill(output);
    }




    public String levenshteinMatcher(String s, List<String> matchToList) {
        //Temporary final variable for comparator comparisons
        String finalS = s;
        Collections.sort(matchToList, (a, b) -> {
            int scoreA = similarityScore(a.toCharArray(), finalS.toCharArray());
            int scoreB = similarityScore(b.toCharArray(), finalS.toCharArray());
            return (scoreA - scoreB);
        });
        String bestFit = matchToList.get(0);
        int currentScore = similarityScore(bestFit.toCharArray(), s.toCharArray());
        //Arbitrary threshold of half of the best fit category's length
        int threshold = bestFit.length()/levenshteinThreshold;
        //The best fit string, is too dis-similar to the input string
        if (currentScore > threshold) return s;
        else return bestFit;
    }



    public int similarityScore(char[] a, char[] b) {
        int[][] dpArray = new int[a.length + 1][b.length + 1];
        for (int i = 0; i <= a.length; i++) {
            for (int j = 0; j <= b.length; j++) {
                if (i == 0) {
                    dpArray[i][j] = j;
                } else if (j == 0) {
                    dpArray[i][j] = i;
                } else {
                    int substitutionCost = (a[i - 1] == b[j - 1] ? 0 : 1);
                    int substitution = dpArray[i - 1][j - 1] + substitutionCost;
                    int insertion = dpArray[i - 1][j] + 1;
                    int deletion = dpArray[i][j - 1] + 1;
                    dpArray[i][j] = Math.min(substitution, Math.min(insertion, deletion));
                }
            }
        }
        return dpArray[a.length][b.length];
    }
}
