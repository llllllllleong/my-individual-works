package main.java.com.example.groupproject24s1.datastructures;
import java.util.*;

/**
 * A Trie data structure implementation specialized for storing and retrieving question indices based on titles.
 * It supports insertion, deletion, and search operations optimized for question title handling.
 *
 * @author Leong Yin Chan
 */
public class Trie implements TrieInterface {
    private Node root;
    private int dictionarySize = 0;

    /**
     * Constructs a Trie with an initial empty root node.
     */
    public Trie() {
        root = new Node();
    }

    /**
     * Filters input strings to a consistent format for Trie operations.
     * Removes non-alphabet characters and converts to lowercase.
     *
     * @param s the string to be filtered
     * @return a cleaned version of the string
     */
    public String inputStringFilter(String s) {
        return s.trim().toLowerCase().replaceAll("[^a-z]", "");
    }

    /**
     * Inserts a s and associated question ID into the Trie.
     *
     * @param s the s to insert
     * @param key the ID of the question associated with the s
     */
    public void insert(String s, Integer key) {
        dictionarySize++;
        s = inputStringFilter(s);
        root.insert(s, 0, key);
    }

    /**
     * Checks if the Trie contains a specific s.
     *
     * @param s the s to search for
     * @return true if the s exists in the Trie, false otherwise
     */
    public boolean contains(String s) {
        s = inputStringFilter(s);
        return root.contains(s, 0);
    }

    /**
     * Searches for question IDs associated with a s.
     *
     * @param s the s to search for
     * @return a list of question IDs associated with the s
     */
    public List<Integer> search(String s) {
        s = inputStringFilter(s);
        return getQuestionIndexFromSearch(s);
    }

    /**
     * Retrieves question indices for a given s.
     *
     * @param s the s to search indices for
     * @return a list of question indices associated with the s
     */
    public List<Integer> getQuestionIndexFromSearch(String s) {
        s = inputStringFilter(s);
        return root.getQuestionIndexFromSearch(s, 0);
    }

    /**
     * Deletes a s and its associated question ID from the Trie.
     *
     * @param s the s to delete
     * @param key the ID of the question to remove
     * @return true if the s was deleted, false otherwise
     */
    public boolean delete(String s, Integer key) {
        s = inputStringFilter(s);
        boolean result = root.delete(s, 0, key);
        if (result) dictionarySize--;
        return result;
    }

    /**
     * Represents a single node in the Trie, which contains pointers to child nodes, a flag for end-of-s,
     * and a list of question indices.
     */
    public class Node {
        private Node[] children;
        private boolean eow;
        private List<Integer> questionIndex;

        /**
         * Constructor for Node, initializes an array for child nodes.
         */
        public Node() {
            children = new Node[26];  // 26 English letters
        }

        /**
         * Inserts a string and its associated question ID into the Trie, starting from a given index.
         *
         * @param s the string to insert
         * @param index the current index in the string
         * @param key the question ID associated with the string
         */
        public void insert(String s, int index, int key) {
            if (index == s.length()) {
                eow = true;
                if (questionIndex == null) questionIndex = new ArrayList<>();
                questionIndex.add(key);
            } else {
                char c = s.charAt(index);
                int cIndex = c - 'a';
                if (children[cIndex] == null) {
                    children[cIndex] = new Node();
                }
                children[cIndex].insert(s, index + 1, key);
            }
        }

        /**
         * Checks if the Trie contains a specific string starting from a given index.
         *
         * @param s the string to check
         * @param index the current index in the string
         * @return true if the string exists in the Trie, false otherwise
         */
        public boolean contains(String s, int index) {
            if (index == s.length()) return false;
            char c = s.charAt(index);
            int cIndex = c - 'a';
            if (children[cIndex] == null) return false;
            if (index == s.length() - 1) return children[cIndex].eow;
            return children[cIndex].contains(s, index + 1);
        }

        /**
         * Attempts to delete a string and its associated question ID from the Trie, starting from a given index.
         *
         * @param s the string to delete
         * @param index the current index in the string
         * @param key the question ID associated with the string
         * @return true if the string was deleted, false otherwise
         */
        public boolean delete(String s, int index, Integer key) {
            if (index == s.length()) {
                if (questionIndex != null && questionIndex.remove(key)) {
                    if (questionIndex.isEmpty()) {
                        questionIndex = null;
                        eow = false;
                    }
                    return true;
                }
                return false;
            } else {
                char c = s.charAt(index);
                int cIndex = c - 'a';
                if (children[cIndex] == null) return false;
                return children[cIndex].delete(s, index + 1, key);
            }
        }

        /**
         * Retrieves all question IDs available from this node down, often used for autofill purposes.
         *
         * @return a list of question IDs
         */
        private List<Integer> autoFill() {
            List<Integer> output = new ArrayList<>();
            if (eow) output.addAll(questionIndex);
            for (Node child : children) {
                if (child != null) {
                    output.addAll(child.autoFill());
                }
            }
            return output;
        }

        /**
         * Retrieves question indices for a given string starting from a specific index.
         *
         * @param s the string to search for
         * @param index the current index in the string
         * @return a list of question indices associated with the string, if found
         */
        public List<Integer> getQuestionIndexFromSearch(String s, int index) {
            if (index < s.length()) {
                char c = s.charAt(index);
                int cIndex = c - 'a';
                if (children[cIndex] == null) return new ArrayList<>();
                return children[cIndex].getQuestionIndexFromSearch(s, index + 1);
            } else {
                return autoFill();
            }
        }
    }
}
