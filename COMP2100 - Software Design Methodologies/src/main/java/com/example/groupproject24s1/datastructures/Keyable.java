package main.java.com.example.groupproject24s1.datastructures;

/**
 * An interface to ensure that implementing classes can provide a unique key.
 * This key is typically used for identifying and sorting objects within data structures.
 * @author Leong Yin Chan
 */
public interface Keyable {
    /**
     * Retrieves the unique key associated with an object.
     *
     * @return the unique key of the object
     */
    Integer getKey();
}