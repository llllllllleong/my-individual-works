package main.java.com.example.groupproject24s1.datastructures;


import java.util.ArrayList;
import java.util.List;


/**
 * This class represents an implementation of a mutable AVL tree data structure
 * In it's current state, this AVL's use case is specific to the storage of Question
 * objects. However, the AVL has been implemented to process generic types. With minor
 * modifications to the methods in this class, it can be adapted to store objects with different
 * atrributes and requirements
 *
 * @author Leong Yin Chan
 */
public class AVLMutable<T extends Comparable<T> & Keyable> {
    public AVLNode<T> root;
    int nodeCount = 0;


    /**
     * Constructor for an empty AVLMutable
     */
    public AVLMutable() {
        this.root = null;
    }

    /**
     * Delete everything in the current AVLMutable
     */
    public void deleteAll() {
        this.root = null;
        nodeCount = 0;
    }

    /**
     * Checks if the AVL tree is empty.
     *
     * @return true if the tree is empty, false otherwise
     */
    public boolean isEmpty() {
        return (this.root == null);
    }
    /**
     * Retrieves the height of a specific node in the AVL tree.
     *
     * @param n the node whose height is to be determined
     * @return the height of the node, or -1 if the node is null
     */
    public int getHeight(AVLNode<T> n) {
        return (n == null) ? -1 : n.height;
    }
    /**
     * Calculates the balance factor of a node in the AVL tree.
     *
     * @param n the node whose balance factor is to be calculated
     * @return the balance factor of the node, or 0 if the node is null
     */
    public int getBalanceFactor(AVLNode<T> n) {
        if (n == null) return 0;
        return getHeight(n.leftNode) - getHeight(n.rightNode);
    }
    /**
     * Returns the total number of nodes in the AVL tree.
     *
     * @return the size of the tree
     */
    public int size() {
        return nodeCount;
    }
    /**
     * Retrieves all objects stored in the AVL tree.
     *
     * @return a list containing all objects in the AVL tree
     */
    public List<T> getAll() {
        List<T> output = new ArrayList<>();
        getAll(this.root, output);
        return output;
    }
    /**
     * Helper method to recursively gather all objects in the AVL tree.
     *
     * @param node the current node being visited
     * @param output the list to store the objects in
     */
    public void getAll(AVLNode<T> node, List<T> output) {
        if (node == null) return;
        output.add(node.getObject());
        getAll(node.getLeftNode(), output);
        getAll(node.getRightNode(), output);
    }


    /**
     * Inserts an object into the AVL tree.
     *
     * @param object the object to be inserted
     * @throws IllegalArgumentException if the object is null or the value exists in the tree
     */
    public void insert(T object) {
        if (root == null) {
            root = new AVLNode<>(object);
        } else {
            //AVLNode class will check if the object to be inserted, is null, and throw an exception if it is
            root = insert(root, object);
        }
        nodeCount++;
    }

    /**
     * Recursive method to insert an object into the AVL tree.
     *
     * @param node the current node being examined
     * @param object the object to be inserted
     * @return the new or adjusted node after insertion
     * @throws IllegalArgumentException if the object is null or the value exists in the tree
     */
    public AVLNode<T> insert(AVLNode<T> node, T object) {
        if (node == null) {
            //The current AVLMutable is an empty tree
            return new AVLNode<>(object);
        } else {
            //The current AVLMutable is not an empty tree
            int compareResult = object.getKey() - node.getKey();
            if (compareResult > 0) {
                //Insert to the right
                node.rightNode = insert(node.rightNode, object);
                //Check balance factor
                if (getBalanceFactor(node) == -2) {
                    compareResult = object.getKey() - node.rightNode.getKey();
                    if (compareResult > 0) {
                        //Right-Right Case
                        node = leftRotate(node);
                    } else {
                        //Right-Left Case
                        node = leftRotateDouble(node);
                    }
                }
            } else if (compareResult < 0) {
                //Insert to the left
                node.leftNode = insert(node.leftNode, object);
                //Check balance factor
                if (getBalanceFactor(node) == 2) {
                    compareResult = object.getKey() - node.leftNode.getKey();
                    if (compareResult < 0) {
                        //Left-Left Case
                        node = rightRotate(node);
                    } else {
                        //Left-Right Case
                        node = rightRotateDouble(node);
                    }
                }
            } else {
                //Object already exists in the tree
                return node;
            }
            node.height = Math.max(getHeight(node.leftNode), getHeight(node.rightNode)) + 1;
            return node;
        }
    }

    /**
     * Rotates a node to the left to maintain AVL tree balance.
     *
     * @param node the node to be rotated
     * @return the new root after the rotation
     */
    public AVLNode<T> leftRotate(AVLNode<T> node) {
        AVLNode<T> newRoot = node.rightNode;
        node.rightNode = newRoot.leftNode;
        newRoot.leftNode = node;
        node.height = Math.max(getHeight(node.leftNode), getHeight(node.rightNode)) + 1;
        newRoot.height = Math.max(getHeight(newRoot.rightNode), node.height) + 1;
        return newRoot;
    }
    /**
     * Performs a double rotation (right then left) on a node to maintain AVL tree balance.
     *
     * @param node the node to be rotated
     * @return the new root after the rotation
     */
    public AVLNode<T> leftRotateDouble(AVLNode<T> node) {
        node.rightNode = rightRotate(node.rightNode);
        return leftRotate(node);
    }
    /**
     * Rotates a node to the right to maintain AVL tree balance.
     *
     * @param node the node to be rotated
     * @return the new root after the rotation
     */
    public AVLNode<T> rightRotate(AVLNode<T> node) {
        AVLNode<T> newRoot = node.leftNode;
        node.leftNode = newRoot.rightNode;
        newRoot.rightNode = node;
        node.height = Math.max(getHeight(node.leftNode), getHeight(node.rightNode)) + 1;
        newRoot.height = Math.max(getHeight(newRoot.leftNode), node.height) + 1;
        return newRoot;
    }
    /**
     * Performs a double rotation (left then right) on a node to maintain AVL tree balance.
     *
     * @param node the node to be rotated
     * @return the new root after the rotation
     */
    public AVLNode<T> rightRotateDouble(AVLNode<T> node) {
        node.leftNode = leftRotate(node.leftNode);
        return rightRotate(node);
    }

    /**
     * Checks if a specific key exists within the AVL tree.
     *
     * @param key the key to search for
     * @return true if the key exists in the tree, false otherwise
     */
    public boolean containsKey(Integer key) {
        if (key == null)
            return false;
        return containsKey(root, key);
    }
    /**
     * Recursive helper method to check for the existence of a key in the AVL tree.
     *
     * @param node the current node being examined
     * @param key the key to search for
     * @return true if the key exists, false otherwise
     */
    public boolean containsKey(AVLNode<T> node, Integer key) {
        if (node == null) return false;
        int compareResult = key - node.getKey();
        if (compareResult > 0) return containsKey(node.rightNode, key);
        if (compareResult < 0) return containsKey(node.leftNode, key);
        return true;
    }
    /**
     * Retrieves an object from the AVL tree by its key.
     *
     * @param key the key of the object to retrieve
     * @return the object, or null if no such object exists
     * @throws IllegalArgumentException if the key is null
     */

    public T get(Integer key) {
        if (key == null)
            throw new IllegalArgumentException("get is getting a null key, " +
                    "AVLMutable should not contain null objects");
        if (root == null)
            return null;
        return get(root, key);
    }
    /**
     * Recursive helper method to retrieve an object from the AVL tree by its key.
     *
     * @param node the current node being examined
     * @param key the key of the object to retrieve
     * @return the object, or null if no such object exists
     */
    public T get(AVLNode<T> node, Integer key) {
        if (node == null) return null;
        int compareResult = key - node.getKey();
        if (compareResult > 0) return get(node.rightNode, key);
        if (compareResult < 0) return get(node.leftNode, key);
        return node.getObject();
    }

    /**
     * Safely deletes an object from the AVL tree by its key.
     *
     * @param key the key of the object to delete
     * @return true if the object was found and deleted, false if the object could not be found
     */
    public boolean safeDelete(Integer key) {
        if (!containsKey(root, key)) return false;
        List<T> all = getAll();
        deleteAll();
        boolean foundDeleteObject = false;
        for (T t : all) {
            if (t.getKey() != key) insert(t);
            else foundDeleteObject = true;
        }
        return foundDeleteObject;
    }
    /**
     * Deletes an object from the AVL tree by its key.
     *
     * @param key the key of the object to delete
     * @return true if the deletion was successful, false otherwise
     * @throws IllegalArgumentException if the key is null or the tree is empty
     */
    public boolean delete(Integer key) {
        if (root == null)
            throw new IllegalArgumentException("The root is null, cannot delete from an empty tree");
        if (key == null)
            throw new IllegalArgumentException("Key null object");
        if (!containsKey(key)) return false;
        int compareResult = key - root.getKey();
        if (compareResult == 0) {
            //Base case: deleting the root
            if (root.leftNode == null && root.rightNode == null) {
                deleteAll();
            } else {
                AVLMutable<T> newTree = new AVLMutable<>();
                transferTree(newTree, root.leftNode, key);
                transferTree(newTree, root.rightNode, key);
                root = newTree.root;
                nodeCount = newTree.nodeCount;
            }
        } else {
            AVLMutable<T> newTree = new AVLMutable<>();
            transferTree(newTree, root, key);
            root = newTree.root;
            nodeCount = newTree.nodeCount;
        }
        return true;
    }
    /**
     * Helper method to transfer all nodes, except the one with the specified key, to a new tree.
     * This method is used during the deletion process to maintain tree balance.
     *
     * @param newTree the new tree to transfer nodes to
     * @param node the current node being examined
     * @param deleteKey the key of the node to be excluded from the new tree
     */
    public void transferTree(AVLMutable<T> newTree, AVLNode<T> node, Integer deleteKey) {
        if (node == null) return;
        int compareResult = deleteKey - node.getKey();
        if (compareResult != 0) newTree.insert(node.object);
        transferTree(newTree, node.leftNode, deleteKey);
        transferTree(newTree, node.rightNode, deleteKey);
    }

    /**
     * Class for nodes within the AVL tree.
     * Each node holds an object, pointers to left and right child nodes, and a height indicator.
     */
    public static class AVLNode<T extends Comparable<T> & Keyable> {
        public T object;
        public AVLNode<T> leftNode;
        public AVLNode<T> rightNode;
        public int height;

        /**
         * Constructs a node with the specified object.
         *
         * @param object the object the node will hold
         * @throws IllegalArgumentException if the object is null
         */
        public AVLNode(T object) {
            //Do not allow leaf nodes to be created with null value
            if (object == null)
                throw new IllegalArgumentException("Input cannot be null");
            this.object = object;
            this.height = 0;
            leftNode = null;
            rightNode = null;
        }
        /**
         * Gets the left child node of this AVLNode.
         *
         * @return the left child node of this AVLNode, or null if no left child exists.
         */
        public AVLNode<T> getLeftNode() {
            return leftNode;
        }

        /**
         * Sets the left child node of this AVLNode.
         *
         * @param leftNode the node to set as the left child.
         */
        public void setLeftNode(AVLNode<T> leftNode) {
            this.leftNode = leftNode;
        }

        /**
         * Gets the right child node of this AVLNode.
         *
         * @return the right child node of this AVLNode, or null if no right child exists.
         */
        public AVLNode<T> getRightNode() {
            return rightNode;
        }

        /**
         * Sets the right child node of this AVLNode.
         *
         * @param rightNode the node to set as the right child.
         */
        public void setRightNode(AVLNode<T> rightNode) {
            this.rightNode = rightNode;
        }

        /**
         * Retrieves the object held by this AVLNode.
         *
         * @return the object contained within this node.
         */
        public T getObject() {
            return object;
        }

        /**
         * Retrieves the key associated with the object in this AVLNode.
         * This key is used for inserting, deleting, and locating objects within the AVL tree.
         *
         * @return the key of the object held by this node.
         */
        public Integer getKey() {
            return object.getKey();
        }



    }
    /**
     * Helper method for testing
     * Provides access to the root node of the AVL tree.
     *
     * @return the root node
     */
    public AVLNode<T> getRoot() {
        return root; // Provide access to the root node
    }
}



