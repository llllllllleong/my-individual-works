package main.java.com.example.groupproject24s1.datastructures;

/**
 * Instance of AVLNonMutable, representing an empty AVLNonMutable tree.
 * @author Leong Yin Chan
 */
public class EmptyTreeNonMutable<T extends Comparable<T> & Keyable> extends AVLNonMutable<T> {
    /**
     * Height is defined as the number of edges from current node to leaf
     * As leaf node has height 0, an empty tree cannot have a height of 0
     * Therefore return -1.
     */
    @Override
    public int getHeight() {
        return -1;
    }

    /**
     * toString method
     * @return the string representation of this tree
     */
    @Override
    public String toString() {
        return "{}";
    }
    /**
     * insert method
     * @param node the object to be inserted
     * @return An AVLNonMutable object with the inserted object
     */
    @Override
    public AVLNonMutable<T> insert(T node) {
        return new AVLNonMutable<T>(node);
    }




}
