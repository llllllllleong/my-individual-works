package main.java.com.example.groupproject24s1.datastructures;

import android.util.Log;

import com.example.groupproject24s1.questions.Question;
import com.example.groupproject24s1.questions.QuestionObserver;
import com.example.groupproject24s1.questions.QuestionType;
import com.example.groupproject24s1.utils.CategoryQuery;
import com.example.groupproject24s1.utils.IDQuery;
import com.example.groupproject24s1.utils.Parser;
import com.example.groupproject24s1.utils.SearchQuery;
import com.example.groupproject24s1.utils.TypeQuery;
import com.example.groupproject24s1.utils.UserQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * This class represents a database, with the methods to insert, delete, retrieve and search. It is
 * based on our implementation of a mutable AVL tree data structure, with HashMaps and Trie used
 * for reverse index attribute based query processing.
 * In-Memory: The database operates in RAM, all data is stored and accessed directly from memory,
 * resulting in fast retrieval and manipulation.
 * Reverse Index: Indexing techniques to map attributes of questions, to the questions themselves.
 * This allows for fast and efficient attribute based query processing .
 * In it's current state, the database's use case is specific to the management of Question
 * objects. However, the AVL tree has been implemented to process generic types. With minor
 * adjustments, this Database is able to hold any generic type object.
 * <p>
 * A unique Integer key is assigned to every object successfully inserted into the data structure.
 * Keys may not be reused. Keys of deleted items may not be reused.
 *
 * @author Leong Yin Chan
 */
public class Database {
    private final List<QuestionObserver> observers = new ArrayList<>();
    private boolean notifyObservers;


    /**
     * Custom exception for handling illegal database queries.
     */
    public static class IllegalDatabaseQuery extends IllegalArgumentException {
        public IllegalDatabaseQuery(String errorMessage) {
            super(errorMessage);
        }
    }

    /**
     * Main storage: A mutable AVL tree data structure.
     * Alternative option: A Non-Mutable AVL tree data structure.
     */
    private AVLMutable<Question> mAVL;
    private AVLNonMutable<Question> nmAVL;

    /**
     * Reverse Index Map (Trie): Question title to key.
     * Alternative option: StringTrie
     */
    private StringTrie stringTrie;
    private Trie titleTrie;

    /**
     * Reverse Index Map (HashMap): Question category to key.
     */
    private HashMap<String, List<Integer>> categoryIndexMap;

    /**
     * Reverse Index Map (HashMap): Question type to key.
     */
    private HashMap<QuestionType, List<Integer>> typeIndexMap;

    /**
     * levenshteinThreshold: A local int variable, used to determine string similarity matching
     * outcome. Decrease this variable to allow for more lenient matching.
     * Minimum value: 1
     */
    private double levenshteinThreshold = 2;

    /**
     * size: A local int variable, representing the number of objects inserted into the database.
     */
    private int size = 0;

    /**
     * keyCounter: A local Integer variable, used to assign unique keys to inserted objects.
     */
    private Integer keyCounter = 1;

    /**
     * questionTypeToMatch: A local String[] variable, used for flexibility in question type search
     * queries
     */
    private List<String> questionTypeToMatchList = Arrays.asList(
            "Multi",
            "Multiple Choice",
            "MCQ",
            "Short Answer",
            "Short",
            "TorF",
            "True",
            "True or False",
            "Fill in the blank",
            "FITB");

    /**
     * Constructs a new instance of Database.
     */
    public Database() {
//        nmAVL = new EmptyTreeNonMutable<>();
//        stringTrie = new StringTrie(levenshteinThreshold);
        mAVL = new AVLMutable<>();
        titleTrie = new Trie();
        categoryIndexMap = new HashMap<>();
        typeIndexMap = new HashMap<>();
    }
    /**
     * Fundamental Database Methods: Insert
     * Inserts a question into the database if it does not already exist.
     *
     * @param q the question to insert
     */
    public void insert(Question q) {
        if (containsQuestion(q)) return;
        //Assign a unique key to the question
        Integer key = keyCounter;
        q.setKey(key);
        size++;
        //Increment keyCounter to ensure the next question inserted will have a unique key
        keyCounter++;
        //Insert into AVL tree
        mAVL.insert(q);
        //        nmAVL = nmAVL.insert(q);

        //Insert into other question index maps
        String questionTitle = q.getText();
        String[] categories = q.getCategory().split(", ");

        titleTrie.insert(questionTitle, key);
        for (String category : categories) {
            category = category.trim();
            if (!categoryIndexMap.containsKey(category))
                categoryIndexMap.put(category, new ArrayList<>());
            categoryIndexMap.get(category).add(key);
        }

        QuestionType qt = q.getType();
        if (!typeIndexMap.containsKey(qt)) typeIndexMap.put(qt, new ArrayList<>());
        typeIndexMap.get(qt).add(key);

        if (notifyObservers) {
            for (QuestionObserver observer : observers) {
                Log.d("Database", "Notifying observers about new question");
                observer.onQuestionAdded(q);
            }
        }
    }

    /**
     * Fundamental Database Methods: containsQuestion
     * Checks if the question already exists in the database.
     *
     * @param q the question to check
     * @return true if the question exists, false otherwise
     */
    public boolean containsQuestion(Question q) {
        if (q.getKey() != null) return containsKey(q.getKey());
        String questionTitle = q.getText();
        List<Question> searchQuestionsFromTitle = searchQuestionsFromTitle(questionTitle);
        for (Question currentQ : searchQuestionsFromTitle)
            if (currentQ.equals(q)) return true;
        return false;
    }

    /**
     * Fundamental Database Methods: containsKey
     * Checks if the key exists in the database.
     *
     * @param key the key to check
     * @return true if the key exists, false otherwise, or if key is null
     */
    public boolean containsKey(Integer key) {
        return mAVL.containsKey(key);
    }

    /**
     * Fundamental Database Methods: isEmpty
     * Checks if the database is empty.
     *
     * @return true if the database is empty, false otherwise
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Fundamental Database Methods: getSize
     * Returns the number of questions in the database.
     *
     * @return the size of the database
     */
    public int getSize() {
        return size;
    }

    /**
     * Fundamental Database Methods: getAllQuestions
     * Retrieves all questions from the database.
     *
     * @return a list of all questions
     */
    public List<Question> getAllQuestions() {
        return mAVL.getAll();
    }

    /**
     * Fundamental Database Methods: getCategories
     * Retrieves all categories from the database.
     *
     * @return a list of all question categories
     */
    public List<String> getCategories() {
        return new ArrayList<>(categoryIndexMap.keySet());
    }

    /**
     * Search and Retrieve Methods: searchCategories
     * Searches for categories that match a given search input based on substring matching or Levenshtein distance.
     *
     * @param input the search string to match against category names
     * @return a list of categories that contain the search string or are closely matched
     *
     * @author Haynes Crossley
     */
    public List<String> searchCategories(String input) {
        List<String> matchedCategories = new ArrayList<>();
        input = input.toLowerCase().trim();  // Normalize input for comparison

        // Iterate through each category and check if the search term is a substring of the category
        for (String category : categoryIndexMap.keySet()) {
            if (category.toLowerCase().contains(input)) {
                matchedCategories.add(category);
            } else {
                // Use a similarity score to add categories that are close matches
                String bestMatch = levenshteinMatcher(input, Collections.singletonList(category));
                if (!bestMatch.equals(input) && !matchedCategories.contains(bestMatch)) {  // Only add if the best match is different from the input and it's a close match
                    matchedCategories.add(bestMatch);
                }
            }
        }

        return matchedCategories;
    }




    /**
     * Search and Retrieve Methods: searchQuestions
     * Performs an search on the database
     * It processes a query string and returns a list of questions that match the criteria.
     *
     * @param input the query string containing different search tokens
     * @return a list of questions that match all the search criteria
     */
    public List<Question> searchQuestions(String input) {
        Parser parser = new Parser(input);
        SearchQuery query = parser.parse();
        if (query == null) return new ArrayList<>();
        if (query instanceof UserQuery) {
            //No implementation for searching user profiles yet
            return new ArrayList<>();
        } else if (query instanceof CategoryQuery) {
            return searchQuestionsFromCategory(query.getValue());
        } else if (query instanceof TypeQuery) {
            return searchQuestionFromType(query.getValue());
        } else if (query instanceof IDQuery) {
            Integer key = ((IDQuery) query).getIntegerValue();
            if (!containsKey(key)) return new ArrayList<>();
            else {
                List<Question> output = new ArrayList<>();
                output.add(searchQuestionFromKey(key));
                return output;
            }
        } else {
            return searchQuestionsFromTitle(query.getValue());
        }
    }



    /**
     * Search and Retrieve Methods: searchQuestionsFromCategory
     * Searches for questions from a specific category.
     *
     * @param category the category to search for
     * @return a list of questions that belong to the specified category
     */
    public List<Question> searchQuestionsFromCategory(String category) {
        List<Question> output = new ArrayList<>();
        category = category.trim();
        if (!categoryIndexMap.containsKey(category))
            category = levenshteinMatcher(category, new ArrayList<>(categoryIndexMap.keySet()));
        if (!categoryIndexMap.containsKey(category)) return output;
        List<Integer> questionKeys = categoryIndexMap.get(category);
        for (Integer key : questionKeys) {
            Question q = searchQuestionFromKey(key);
            if (q != null) output.add(q);
        }
        return output;
    }

    /**
     * Search and Retrieve Methods: searchQuestionFromKey
     * Searches for a question by its key.
     *
     * @param key the key/QuestionID of the question to search for
     * @return the question if found, null otherwise
     */
    public Question searchQuestionFromKey(Integer key) {
        Question q = mAVL.get(key);
        return q;
    }

    /**
     * Search and Retrieve Methods: searchQuestionFromType
     * Searches for questions based on their type, utilizing levenshteinMatcher.
     *
     * @param input the type of question to search for
     * @return a list of questions that match the specified type
     */
    public List<Question> searchQuestionFromType(String input) {
        List<Question> output = new ArrayList<>();
        String bestMatch = levenshteinMatcher(input, questionTypeToMatchList);
        //If the input string is too dissimilar to the strings in questionTypeToMatchList
        List<Integer> questionKeys;
        if (bestMatch.equals("Multi")
                || bestMatch.equals("Multiple Choice")
                || bestMatch.equals("MCQ")) {
            questionKeys = typeIndexMap.get(QuestionType.MULTIPLE_CHOICE);
        } else if (bestMatch.equals("Short Answer")
                || bestMatch.equals("Short")) {
            questionKeys = typeIndexMap.get(QuestionType.SHORT_ANSWER);
        } else if (bestMatch.equals("TorF")
                || bestMatch.equals("True")
                || bestMatch.equals("True or False")) {
            questionKeys = typeIndexMap.get(QuestionType.TRUE_FALSE);
        } else if (bestMatch.equals("Fill in the blank")
                || bestMatch.equals("FITB")) {
            questionKeys = typeIndexMap.get(QuestionType.FILL_IN_THE_BLANK);
        } else {
            return output;
        }
        for (Integer key : questionKeys) {
            Question q = searchQuestionFromKey(key);
            if (q != null) output.add(q);
        }
        return output;
    }

    /**
     * Search and Retrieve Methods: searchQuestionsFromTitle
     * Searches for questions based on keywords in their titles.
     *
     * @param input the title keywords to search for
     * @return a list of questions that match the title keywords
     */
    public List<Question> searchQuestionsFromTitle(String input) {
        List<Question> output = new ArrayList<>();
        input = input.trim().toLowerCase().replaceAll("[^a-z]", "");
        List<Integer> questionKeys = titleTrie.search(input);
        for (Integer key : questionKeys) {
            Question q = searchQuestionFromKey(key);
            if (q != null) output.add(q);
        }
        return output;
    }


    /**
     * Deletion Methods: delete
     * Deletes a question from the database using its unique key.
     * If the key does not exist, it returns false. If deletion is successful, it updates
     * the database size and cleans up related indexes. It throws an exception if there are issues with deletion.
     *
     * @param key the unique key of the question to delete
     * @return true if the deletion was successful, false otherwise
     * @throws IllegalDatabaseQuery if there is an issue with deleting the question from the AVL tree or the trie
     */
    public boolean delete(Integer key) {
        if (!containsKey(key)) return false;
        Question q = mAVL.get(key);
        boolean avlDelete = mAVL.safeDelete(key);
        boolean trieDelete = titleTrie.delete(q.getText(), key);
        //Throw exceptions if there was an issue with deletion
        //If the object doesn't exist, return false;
        if (!avlDelete)
            throw new IllegalDatabaseQuery("Mutable AVL Safe-Delete method returned false");
        if (!trieDelete) {
            throw new IllegalDatabaseQuery("Trie delete method returned false. Note: if avlDelete did not " +
                    "throw an exception, then the question is already deleted from the AVL Tree");
        }
        size--;
        sweepMaps(q);
        return true;
    }

    /**
     * Cleanup Methods: sweepMaps
     * Cleans up the index maps after a question has been deleted.
     * It removes the question's references from the category and type index maps.
     *
     * @param q the question that has been deleted
     */
    public void sweepMaps(Question q) {
        Integer key = q.getKey();
        String questionTitle = q.getText().toLowerCase().replaceAll("[^a-z]", "");
        String[] categories = q.getCategory().split(", ");
        QuestionType qt = q.getType();
        for (String category : categories) {
            category = category.trim();
            categoryIndexMap.get(category).remove(key);
        }
        typeIndexMap.get(qt).remove(key);
        sweep();
    }

    /**
     * Cleanup Methods: sweep
     * Performs a cleanup of the category and type index maps.
     * Removes any categories or types that no longer have any questions associated with them.
     */
    public void sweep() {
        List<QuestionType> qtListToDelete = new ArrayList<>();
        List<String> categoryListToDelete = new ArrayList<>();
        for (QuestionType qt : typeIndexMap.keySet())
            if (typeIndexMap.get(qt).isEmpty()) qtListToDelete.add(qt);
        for (String category : categoryIndexMap.keySet())
            if (categoryIndexMap.get(category).isEmpty()) categoryListToDelete.add(category);
        for (QuestionType qt : qtListToDelete)
            typeIndexMap.remove(qt);
        for (String category : categoryListToDelete)
            categoryIndexMap.remove(category);
    }

    /**
     * Deletion Methods: deleteAll
     * Clears all data from the database, resetting the AVL tree, all index maps, and counters.
     */
    public void deleteAll() {
//        nmAVL = new EmptyTreeNonMutable<>();
        mAVL = new AVLMutable<>();
        titleTrie = new Trie();
        categoryIndexMap = new HashMap<>();
        typeIndexMap = new HashMap<>();
        size = 0;
        keyCounter = 1;
    }


    /**
     * Matching Methods: levenshteinMatcher
     * Matches a string to the closest string in a list based on the Levenshtein distance.
     * Returns the closest match if the distance is below a threshold, otherwise returns the original string.
     *
     * @param s the string to match
     * @param matchToList the list of strings to match against
     * @return the best match or the original string if no suitable match is found
     */
    public String levenshteinMatcher(String s, List<String> matchToList) {
        //Temporary final variable for comparator comparisons
        String ss = s;
        Collections.sort(matchToList, (a, b) -> {
            int scoreA = similarityScore(a, ss);
            int scoreB = similarityScore(b, ss);
            return (scoreA - scoreB);
        });
        String bestFit = matchToList.get(0);
        double currentScore = (double) similarityScore(bestFit, s);
        //Arbitrary threshold of half of the best fit category's length
        double threshold = (double) bestFit.length() / levenshteinThreshold;
        //The best fit string, is too dis-similar to the input string
        if (currentScore > threshold) return s;
        else return bestFit;
    }

    /**
     * Matching Methods: similarityScore
     * Computes the Levenshtein distance between two character arrays to determine their similarity.
     *
     * @param aString the first String
     * @param bString the second String
     * @return the Levenshtein distance between the two arrays
     */
    public int similarityScore(String aString, String bString) {
        char[] a = aString.toLowerCase().toCharArray();
        char[] b = bString.toLowerCase().toCharArray();
        int[][] dpArray = new int[a.length + 1][b.length + 1];
        for (int i = 0; i <= a.length; i++) {
            for (int j = 0; j <= b.length; j++) {
                if (i == 0) {
                    dpArray[i][j] = j;
                } else if (j == 0) {
                    dpArray[i][j] = i;
                } else {
                    int substitutionCost = (a[i - 1] == b[j - 1] ? 0 : 1);
                    int substitution = dpArray[i - 1][j - 1] + substitutionCost;
                    int insertion = dpArray[i - 1][j] + 1;
                    int deletion = dpArray[i][j - 1] + 1;
                    dpArray[i][j] = Math.min(substitution, Math.min(insertion, deletion));
                }
            }
        }
        return dpArray[a.length][b.length];
    }


    public void addObserver(QuestionObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(QuestionObserver observer) {
        observers.remove(observer);
    }

    public void bulkInsert(List<Question> questions) {
        // Temporarily disable notifications
        boolean originalState = notifyObservers;
        setNotifyObservers(false);

        for (Question question : questions) {
            insert(question);  // This call will not notify observers due to the flag
        }

        // Restore original notification state
        setNotifyObservers(originalState);
    }

    public void setNotifyObservers(boolean notify) {
        this.notifyObservers = notify;
    }


}

