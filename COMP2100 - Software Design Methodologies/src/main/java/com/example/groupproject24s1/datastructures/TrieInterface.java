package main.java.com.example.groupproject24s1.datastructures;

import java.util.List;

/**
 * Interface for testing purposes. Allows performance test methods to recieve a TrieInterface input
 * Can also be used for future substitution of the char Trie for the String trie
 *
 * @author Leong Yin Chan
 */
public interface TrieInterface {
    /**
     * Inserts a word associated with a specific key into the Trie.
     * This method is primarily used to add new entries to the Trie.
     *
     * @param word the word to be inserted into the Trie.
     * @param key  the key associated with the word, typically used as an identifier or index.
     */
    void insert(String word, Integer key);

    /**
     * Deletes an entry from the Trie based on the provided string and its associated key.
     * This method is used to remove entries that are no longer needed.
     *
     * @param s   the string whose associated entry is to be deleted.
     * @param key the key associated with the string to be deleted.
     * @return true if the entry was successfully deleted, false if the entry could not be found.
     */
    boolean delete(String s, Integer key);

    /**
     * Checks if the Trie contains a specific string.
     * This method is used to verify the presence of a string in the Trie.
     *
     * @param s the string to be checked.
     * @return true if the string is present in the Trie, false otherwise.
     */
    boolean contains(String s);

    /**
     * Searches the Trie for entries associated with a specific string.
     * This method is typically used to retrieve all keys associated with the string, often for lookup or validation purposes.
     *
     * @param s the string for which associated keys are to be retrieved.
     * @return a list of integers representing the keys associated with the provided string.
     */
    List<Integer> search(String s);
}
