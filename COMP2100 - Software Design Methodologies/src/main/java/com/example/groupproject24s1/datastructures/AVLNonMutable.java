package main.java.com.example.groupproject24s1.datastructures;

import java.util.ArrayDeque;
import java.util.Deque;



/**
 * This class represents a Non-Mutable AVL tree data structure
 *
 * This class has not been refined, and there is inconsistent documentation because it is unused.
 *
 * @author Leong Yin Chan
 */
public class AVLNonMutable<T extends Comparable<T> & Keyable> {
    public T node;
    public AVLNonMutable<T> leftNode;
    public AVLNonMutable<T> rightNode;
    public int balanceFactor;


    /**
     * Constructor for an empty tree
     */
    public AVLNonMutable() {
        this.node = null;
    }


    /**
     * Constructor for a new leaf node
     * @param node to set as this node's value.
     */
    public AVLNonMutable(T node) {
        //Insertion of null value for nodes is not allowed
        if (node == null)
            throw new IllegalArgumentException("Input cannot be null");
        this.node = node;
        this.leftNode = new EmptyTreeNonMutable<>();
        this.rightNode = new EmptyTreeNonMutable<>();
        this.balanceFactor = 0;
    }

    /**
     * Constructor for a new parent node, with child nodes
     * @param node to set as this node's value.
     * @param leftNode to set as the node's left child.
     * @param rightNode to set as the node's right child.
     */
    public AVLNonMutable(T node, AVLNonMutable<T> leftNode, AVLNonMutable<T> rightNode) {
        //Insertion of null value for nodes is not allowed
        if (node == null)
            throw new IllegalArgumentException("Input cannot be null");
        this.node = node;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }


    /**
     * Find the maximum height from this node.
     * @return The maximum height of either children.
     */
    public int getHeight() {
        if (this.node == null)
            throw new IllegalArgumentException("The current tree is a empty AVLNonMutable Tree");
        int leftNodeHeight = leftNode instanceof EmptyTreeNonMutable ? 0 : 1 + leftNode.getHeight();
        int rightNodeHeight = rightNode instanceof EmptyTreeNonMutable ? 0 : 1 + rightNode.getHeight();
        return Math.max(leftNodeHeight, rightNodeHeight);
    }


    public void updateBalanceFactor() {
        int i = leftNode.getHeight() - rightNode.getHeight();
        this.balanceFactor = i;
    }






    public AVLNonMutable<T> insert(T newNode) {
        // Ensure input is not null.
        if (newNode == null)
            throw new IllegalArgumentException("Input cannot be null");
        int compareResult = newNode.getKey() - node.getKey();
        if (compareResult > 0) {
            //Insert into right side
            AVLNonMutable<T> a = new AVLNonMutable<>(node, leftNode, rightNode.insert(newNode));
            int currentBalance = a.getBalanceFactor();
            if (currentBalance == -2) {
                AVLNonMutable<T> b = a.leftRotate();
                currentBalance = b.getBalanceFactor();
                if (currentBalance == 2) {
                    //Right Left case
                    AVLNonMutable<T> rotatedSub =  a.rightNode;
                    rotatedSub = rotatedSub.rightRotate();
                    AVLNonMutable<T> c = new AVLNonMutable<>(node, leftNode, rotatedSub);
                    return c.leftRotate();
                }
                return b;
            }
            return a;
        } else if (compareResult < 0) {
            //Insert into left side
            AVLNonMutable<T> a = new AVLNonMutable<>(node, leftNode.insert(newNode), rightNode);
            int currentBalance = a.getBalanceFactor();
            if (currentBalance == 2) {
                AVLNonMutable<T> b = a.rightRotate();
                currentBalance = b.getBalanceFactor();
                if (currentBalance == -2) {
                    //Left Right case
                    AVLNonMutable<T> rotatedSub = a.leftNode;
                    rotatedSub = rotatedSub.leftRotate();
                    AVLNonMutable<T> c = new AVLNonMutable<>(node, rotatedSub, rightNode);
                    return c.rightRotate();
                }
                return b;
            }
            return a;
        } else {
            //Object already exists in the tree
            return this;
        }
    }


    public AVLNonMutable<T> leftRotate() {
        AVLNonMutable<T> current = this.rightNode;
        AVLNonMutable<T> currentLeft = current.leftNode;
        AVLNonMutable<T> currentRight = current.rightNode;
        AVLNonMutable<T> newLeft = new AVLNonMutable<>(node, leftNode, currentLeft);
        AVLNonMutable<T> newTree = new AVLNonMutable<>(current.node, newLeft, currentRight);
        return newTree;
    }

    public AVLNonMutable<T> rightRotate() {
        AVLNonMutable<T> current = this.leftNode;
        AVLNonMutable<T> currentLeft = current.leftNode;
        AVLNonMutable<T> currentRight = current.rightNode;
        AVLNonMutable<T> newRight = new AVLNonMutable<>(this.node, currentRight, this.rightNode);
        AVLNonMutable<T> newTree = new AVLNonMutable<>(current.node, currentLeft, newRight);
        return newTree;
    }

    public int getBalanceFactor() {
        updateBalanceFactor();
        return balanceFactor;
    }

    public boolean containsKey(Integer key) {
        if (node == null) return false;
        int compareResult = key - node.getKey();
        if (compareResult == 0) return true;
        if (compareResult < 0) return (leftNode instanceof EmptyTreeNonMutable) ? false : leftNode.containsKey(key);
        else return (rightNode instanceof EmptyTreeNonMutable) ? false : rightNode.containsKey(key);
    }

    public T get(Integer key) {
        if (key == null)
            throw new IllegalArgumentException("key is null. nmAVL cannot have null key");
        if (this.node == null) {
            return null;
        }
        int compareResult = key - node.getKey();
        if (compareResult == 0) return this.node;
        if (compareResult < 0) return (leftNode instanceof EmptyTreeNonMutable) ? null : leftNode.get(key);
        else return (rightNode instanceof EmptyTreeNonMutable) ? null : rightNode.get(key);
    }



    public AVLNonMutable<T> safeDelete(Integer key) {
        if (!containsKey(key)) return this;
        Deque<AVLNonMutable<T>> dq = new ArrayDeque<>();
        int compareResult = key - node.getKey();
        if (compareResult == 0) {
            if (leftNode instanceof EmptyTreeNonMutable && rightNode instanceof EmptyTreeNonMutable) {
                return new EmptyTreeNonMutable<>();
            } else {
                if (!(leftNode instanceof EmptyTreeNonMutable)) dq.addLast(leftNode);
                if (!(rightNode instanceof EmptyTreeNonMutable)) dq.addLast(rightNode);
            }
        } else {
            dq.addFirst(this);
        }
        AVLNonMutable<T> first = dq.pollFirst();
        if (!(first.leftNode instanceof EmptyTreeNonMutable)) dq.addLast(first.leftNode);
        if (!(first.rightNode instanceof EmptyTreeNonMutable)) dq.addLast(first.rightNode);
        AVLNonMutable<T> newTree = new AVLNonMutable<>(first.node);
        while (!dq.isEmpty()) {
            AVLNonMutable<T> current = dq.pollFirst();
            if (current.node.getKey() != key) newTree = newTree.insert(current.node);
            if (!(current.leftNode instanceof EmptyTreeNonMutable)) dq.addLast(current.leftNode);
            if (!(current.rightNode instanceof EmptyTreeNonMutable)) dq.addLast(current.rightNode);
        }
        return newTree;
    }


    public AVLNonMutable<T> delete(T value) {
        if (node == null) {
            return this;
        }
        int compareResult = value.compareTo(node);
        if (compareResult < 0) {
            if (!(leftNode instanceof EmptyTreeNonMutable)) {
                leftNode = leftNode.delete(value);
            }
        } else if (compareResult > 0) {
            if (!(rightNode instanceof EmptyTreeNonMutable)) {
                rightNode = rightNode.delete(value);
            }
        } else {
            // The current node is the node to be deleted
            if (leftNode instanceof EmptyTreeNonMutable && rightNode instanceof EmptyTreeNonMutable) {
                //The current node is a leaf
                return new EmptyTreeNonMutable<>();
            } else if (leftNode instanceof EmptyTreeNonMutable) {
                return rightNode; // Node has one child (right)
            } else if (rightNode instanceof EmptyTreeNonMutable) {
                return leftNode; // Node has one child (left)
            } else {
                // Node has two children, replace this node with the smallest node in the right subtree
                AVLNonMutable<T> minLargerNode = rightNode.findMin();
                return new AVLNonMutable<>(minLargerNode.node, leftNode, rightNode.delete(minLargerNode.node));
            }
        }
        return rebalanceAfterDeletion();
    }

    public AVLNonMutable<T> findMin() {
        AVLNonMutable<T> current = this;
        AVLNonMutable<T> nextLeft = current.leftNode;
        while (!(nextLeft instanceof EmptyTreeNonMutable)) {
            current = current.leftNode;
            nextLeft = current.leftNode;
        }
        return current;
    }
    private AVLNonMutable<T> rebalanceAfterDeletion() {
        updateBalanceFactor();
        if (balanceFactor >= 2) {
            leftNode.updateBalanceFactor();
            if (leftNode.balanceFactor < 0) {
                // Left-Right Case
                leftNode = leftNode.leftRotate();
            }
            return rightRotate();
        } else if (balanceFactor <= -2) {
            if (rightNode.balanceFactor > 0) {
                //Right-Left Case
                rightNode = rightNode.rightRotate();
            }
            return leftRotate();
        }
        return new AVLNonMutable<>(node, leftNode, rightNode);
    }



    /**
     * toString method
     * @return the string representation of this tree
     */
    @Override
    public String toString() {
        return "{" +
                "node=" + node +
                ", leftNode=" + leftNode +
                ", rightNode=" + rightNode +
                '}';
    }


}

