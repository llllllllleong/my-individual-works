### Basic App and custom Features
- `Login`: Login works well, could be extended by having a persisted element in the case that login fails.
- `Data Instances`: Good range of questions, it would be good to see data instances with greater variety however.
- `Load & Show Data`: Data displays appropriately.
- `Data Stream`: Data appears to update periodically with a toast to indicate.
- `Search, Tokeniser, Parser, Grammar`: Cannot demonstrate this feature in application or see a documented example, only default search works. I can see the implementation in the code however.
- `Custom Features`: done on these features, implementation is done well and your search functions well with partial queries.

### Core Aspects
- `Code Quality`: Good documentation and code layout, clear to navigate and easy to read.
- `UI`: Good overall app design, some design choices such as text size could be made consistent across a few different areas. For example the search interface uses different font size between the two inputs.
- `Creativity`: Good app idea, chosen features are useful for the context of your app.
- `Data Structure`: Great choices for data structures, definitely improve performance of searches for strings.
- `Report`: Great report, all items covered.
- `Teamwork`: Clear evidence of teamwork
- `Testing`: Good number of tests overall, testing should demonstrate coverage in your report as a percentage available through the testing interface.
- `Surprise Feature`: Not implemented.
- `Overall`: Good work overall, application is well executed. I did find that it would become unresponsive quickly, however it still functioned.



### Notes on Individual assessment: 
- Your work have been assessed as a special case given your contribution level and/or number of active members in the group. Your contribution is much appreciated and your marks have been increased.