# STAT2001

| Course                                           | Coursework |  Mark  |
|--------------------------------------------------|:----------:|:------:|
| STAT 2001 - Introductory Mathematical Statistics |    1/2     |   92   |
| STAT 2001 - Introductory Mathematical Statistics |    2/2     |  100   |

This was my first "real" statistics courses. I completed the entire semester 
remotely in Singapore, 2020.

At the time I was also enrolled in COMP1100, which taught me algorithm-heavy 
programming. I felt that these were the first two courses where I had to 
squeeze my brain and problem solve.


