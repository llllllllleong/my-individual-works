/**
 * Game.java
 * Authors:
 * - Leong Yin Chan: u6858120
 * - Shihao Wang: u7516343
 **/

import comp1110.ass2.BoardState;
import comp1110.ass2.CatanDice;
import comp1110.ass2.GameState;
import comp1110.ass2.ResourceState;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static comp1110.ass2.CatanDice.rollDice;

public class Game extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        //Load the fxml document for the UI design
        Parent root = FXMLLoader.load(getClass().getResource("/comp1110/ass2/gui/ui.fxml"));

        //Find out all the buttons in the gaming ui
        Button roll_dice = (Button) root.lookup("#roll_dice");
        Button trade = (Button) root.lookup("#trade");
        Button swap = (Button) root.lookup("#swap");
        Button endturn = (Button) root.lookup("#endturn");
        Button firstroll = (Button) root.lookup("#firstroll");

        trade.setDisable(true);
        swap.setDisable(true);
        endturn.setDisable(true);
        roll_dice.setDisable(true);
        //Find out all the text fields in the gaming ui
        TextField tradeidex = (TextField) root.lookup("#tradeidex");
        TextField swapfrom = (TextField) root.lookup("#swapfrom");
        TextField swapto = (TextField) root.lookup("#swapto");

        //Find out all the toggle buttons for the re-roll functionality
        CheckBox t1 = (CheckBox) root.lookup("#t1");
        CheckBox t2 = (CheckBox) root.lookup("#t2");
        CheckBox t3 = (CheckBox) root.lookup("#t3");
        CheckBox t4 = (CheckBox) root.lookup("#t4");
        CheckBox t5 = (CheckBox) root.lookup("#t5");
        CheckBox t6 = (CheckBox) root.lookup("#t6");

        //Find out all the labels for outputs in the gaming ui
        Label resourceState = (Label) root.lookup("#label");
        Label error = (Label) root.lookup("#p_situ");
        Label tradeerror = (Label) root.lookup("#tradeerror");
        Label swaperror = (Label) root.lookup("#swaperror");
        Label turn = (Label) root.lookup("#turn");
        Label score = (Label) root.lookup("#score");
        Label dice_num = (Label) root.lookup("#dice_num");

        //Find out all the roads in the gaming ui
        Rectangle R0 = (Rectangle) root.lookup("#R0");
        Rectangle R1 = (Rectangle) root.lookup("#R1");
        Rectangle R2 = (Rectangle) root.lookup("#R2");
        Rectangle R3 = (Rectangle) root.lookup("#R3");
        Rectangle R4 = (Rectangle) root.lookup("#R4");
        Rectangle R5 = (Rectangle) root.lookup("#R5");
        Rectangle R6 = (Rectangle) root.lookup("#R6");
        Rectangle R7 = (Rectangle) root.lookup("#R7");
        Rectangle R8 = (Rectangle) root.lookup("#R8");
        Rectangle R9 = (Rectangle) root.lookup("#R9");
        Rectangle R10 = (Rectangle) root.lookup("#R10");
        Rectangle R11 = (Rectangle) root.lookup("#R11");
        Rectangle R12 = (Rectangle) root.lookup("#R12");
        Rectangle R13 = (Rectangle) root.lookup("#R13");
        Rectangle R14 = (Rectangle) root.lookup("#R14");
        Rectangle R15 = (Rectangle) root.lookup("#R15");
        Rectangle rt = (Rectangle) root.lookup("#resource_table");

        //Find out all the knights in the gaming ui
        Polygon J1 = (Polygon) root.lookup("#J1");
        Polygon J2 = (Polygon) root.lookup("#J2");
        Polygon J3 = (Polygon) root.lookup("#J3");
        Polygon J4 = (Polygon) root.lookup("#J4");
        Polygon J5 = (Polygon) root.lookup("#J5");
        Polygon J6 = (Polygon) root.lookup("#J6");

        //Find out all the settlements in the gaming ui
        Polygon S3 = (Polygon) root.lookup("#S3");
        Polygon S4 = (Polygon) root.lookup("#S4");
        Polygon S5 = (Polygon) root.lookup("#S5");
        Polygon S7 = (Polygon) root.lookup("#S7");
        Polygon S9 = (Polygon) root.lookup("#S9");
        Polygon S11 = (Polygon) root.lookup("#S11");

        //Find out all the cities in the gaming ui
        Polygon C7 = (Polygon) root.lookup("#C7");
        Polygon C12 = (Polygon) root.lookup("#C12");
        Polygon C20 = (Polygon) root.lookup("#C20");
        Polygon C30 = (Polygon) root.lookup("#C30");

        //Find out all the images for toggle buttons in the gaming ui
        Rectangle tt1 = (Rectangle) root.lookup("#1");
        Rectangle tt2 = (Rectangle) root.lookup("#2");
        Rectangle tt3 = (Rectangle) root.lookup("#3");
        Rectangle tt4 = (Rectangle) root.lookup("#4");
        Rectangle tt5 = (Rectangle) root.lookup("#5");
        Rectangle tt6 = (Rectangle) root.lookup("#6");


        //Images for the jokers and resources
        Image ore = new Image("/images/ore.png");
        Image oreBuilt = new Image("/images/oreBuilt.png");
        Image oreUsed = new Image("/images/oreUsed.png");
        Image grain = new Image("/images/grain.png");
        Image grainBuilt = new Image("/images/grainBuilt.png");
        Image grainUsed = new Image("/images/grainUsed.png");
        Image wool = new Image("/images/wool.png");
        Image woolBuilt = new Image("/images/woolBuilt.png");
        Image woolUsed = new Image("/images/woolUsed.png");
        Image wood = new Image("/images/wood.png");
        Image woodBuilt = new Image("/images/woodBuilt.png");
        Image woodUsed = new Image("/images/woodUsed.png");
        Image brick = new Image("/images/brick.png");
        Image brickBuilt = new Image("/images/brickBuilt.png");
        Image brickUsed = new Image("/images/brickUsed.png");
        Image wild = new Image("/images/wild.png");
        Image wildBuilt = new Image("/images/wildBuilt.png");
        Image wildUsed = new Image("/images/wildUsed.png");
        Image gold = new Image("/images/gold.png");
        Image resource_table = new Image("images/resource_need.png");
        rt.setFill(new ImagePattern(resource_table));
        J1.setFill(new ImagePattern(ore));
        J2.setFill(new ImagePattern(grain));
        J3.setFill(new ImagePattern(wool));
        J4.setFill(new ImagePattern(wood));
        J5.setFill(new ImagePattern(brick));
        J6.setFill(new ImagePattern(wild));
        Image oreBlue = new Image("/images/oreBlue.png");
        Image grainBlue = new Image("/images/grainBlue.png");
        Image woolBlue = new Image("/images/woolBlue.png");
        Image woodBlue = new Image("/images/woodBlue.png");
        Image brickBlue = new Image("/images/brickBlue.png");
        Image goldBlue = new Image("/images/goldBlue.png");
/*        Rectangle oreID1 = (Rectangle) root.lookup("#oreID1");
        oreID1.setFill(new ImagePattern(oreBlue));
        Rectangle grainID1 = (Rectangle) root.lookup("#grainID1");
        grainID1.setFill(new ImagePattern(grainBlue));
        Rectangle woolID1 = (Rectangle) root.lookup("#woolID1");
        woolID1.setFill(new ImagePattern(woolBlue));
        Rectangle woodID1 = (Rectangle) root.lookup("#woodID1");
        woodID1.setFill(new ImagePattern(woodBlue));
        Rectangle brickID1 = (Rectangle) root.lookup("#brickID1");
        brickID1.setFill(new ImagePattern(brickBlue));
        Rectangle goldID1 = (Rectangle) root.lookup("#goldID1");
        goldID1.setFill(new ImagePattern(goldBlue));*/
        Label actions = (Label) root.lookup("#actions");





        //initial board state, resource state and turn number
        final String[] boardState = {""};
        int[] resState = {0, 0, 0, 0, 0, 0};
        final int[] turn_number = {1};
        final int[] turn_without_build = {0};
        final boolean[] buildornot = {false};
        final int[] reroll_time = {0};
        final int[][] reroll_index = {{0, 0, 0, 0, 0, 0}};
        Image[] image = {ore,grain,wool,wood,brick,gold};
        Rectangle[] tt = {tt1,tt2,tt3,tt4,tt5,tt6};

        //function of roll_dice first time
        firstroll.setOnAction(actionEvent -> {
            //Roll the dice and add the corresponding resources
            rollDice(6, resState);
//            For testing purposes: roll 100 dice
//            rollDice(100, resState);
            resourceState.setText(Arrays.toString(resState));
            firstroll.setDisable(true);
            roll_dice.setDisable(false);
            t1.setDisable(false);
            t2.setDisable(false);
            t3.setDisable(false);
            t4.setDisable(false);
            t5.setDisable(false);
            t6.setDisable(false);
            if (resState[5] > 1) {
                trade.setDisable(false);
            }
            endturn.setDisable(false);
            BoardState bs = new BoardState(boardState[0]);
            if (!bs.getBoardState().equals("") && !bs.jokersAvailable().isEmpty()) {
                swap.setDisable(false);
            }
            GameState gs = new GameState(boardState[0], resState);
            List<String> actionList = gs.allAllowedActionsTS();
            int a = 0;
            for(int i = 0; i<6;i++){
                for(int j = 0; j<resState[i];j++){
                    reroll_index[0][a+j]=i;
                }
                a+=resState[i];
            }
            tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
            tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
            tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
            tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
            tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
            tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));


            if (actionList == null || actionList.isEmpty()) {
                actions.setText("Nothing you can do! You should re-roll!");
            } else {
                actions.setText("Reroll, or: " + actionList.toString());
            }
        });


        roll_dice.setOnAction(actionEvent -> {
            //Re-roll the dice and add the corresponding resources
            String reRoll = "";
            if(t1.isSelected()) reRoll += Integer.toString(reroll_index[0][0])+",";
            if(t2.isSelected()) reRoll += Integer.toString(reroll_index[0][1])+",";
            if(t3.isSelected()) reRoll += Integer.toString(reroll_index[0][2])+",";
            if(t4.isSelected()) reRoll += Integer.toString(reroll_index[0][3])+",";
            if(t5.isSelected()) reRoll += Integer.toString(reroll_index[0][4])+",";
            if(t6.isSelected()) reRoll += Integer.toString(reroll_index[0][5]);

            int[] tempRS = {0,0,0,0,0,0};
            try {
                System.arraycopy(resState, 0, tempRS, 0, resState.length);
                String[] reRollArr = reRoll.split(",");
                int noOfDiceToReroll = reRollArr.length;
                for (String s :reRollArr) {
                    int i = Integer.parseInt(s);
                    tempRS[i] -= 1;
                }
                if (ResourceState.wellFormedRS(tempRS)) {
                    System.arraycopy(tempRS, 0, resState, 0, resState.length);
                    reroll_time[0] += 1;
                    dice_num.setText("");
                    rollDice(noOfDiceToReroll, resState);
                    resourceState.setText(Arrays.toString(resState));
                    GameState gs = new GameState(boardState[0], resState);
                    List<String> actionList = gs.allAllowedActionsTS();
                    int a = 0;
                    int[] empty = {0,0,0,0,0,0};
                    reroll_index[0] = empty.clone();
                    for(int i = 0; i<6;i++){
                        for(int j = 0; j<resState[i];j++){
                            reroll_index[0][a+j]=i;
                        }
                        a+=resState[i];
                    }
                    tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                    tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                    tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                    tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                    tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                    tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));
                    t1.setSelected(false);
                    t2.setSelected(false);
                    t3.setSelected(false);
                    t4.setSelected(false);
                    t5.setSelected(false);
                    t6.setSelected(false);
                    if (reroll_time[0] == 2) {
                        roll_dice.setDisable(true);
                        t1.setDisable(true);
                        t2.setDisable(true);
                        t3.setDisable(true);
                        t4.setDisable(true);
                        t5.setDisable(true);
                        t6.setDisable(true);
                        if (actionList == null || actionList.isEmpty()) {
                            actions.setText("None! You'll have to end your turn!");
                        } else {
                            actions.setText(actionList.toString());
                        }
                    } else {
                        if (actionList == null || actionList.isEmpty()) {
                            actions.setText("Nothing you can do! You should re-roll!");
                        } else {
                            actions.setText("Reroll, or: " + actionList.toString());
                        }
                    }
                    if (resState[5] < 2) {
                        trade.setDisable(true);
                    } else {
                        trade.setDisable(false);
                    }
                } else {
                    dice_num.setText("Invalid input!");
                }
            } catch (Exception e) {
                dice_num.setText("Invalid input!");
            }
        });


        //Method for click to build
        //Click building roads
        Rectangle[] recButtons = {R0, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14, R15};
        String[] recNames = {"R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8", "R9", "R10", "R11", "R12", "R13", "R14", "R15"};
        for (int i = 0; i < recButtons.length; i++) {
            Rectangle b = recButtons[i];
            String bName = recNames[i];
            b.setOnMouseClicked(mouseEvent -> {
                if (CatanDice.canDoAction("build " + bName, boardState[0], resState)) {
                    b.setFill(Color.LIGHTGREEN);
                    resState[3] -= 1;
                    resState[4] -= 1;
                    resourceState.setText(Arrays.toString(resState));
                    if (boardState[0].equals("")) boardState[0] += bName;
                    else boardState[0] = boardState[0] + "," + bName;
                    error.setText("");
                    buildornot[0] = true;
                    GameState temp = new GameState(boardState[0], resState);
                    score.setText(Integer.toString(temp.score() - turn_without_build[0] * 2));
                    roll_dice.setDisable(true);
                    int a = 0;
                    for(int h = 0; h<6;h++){
                        for(int j = 0; j<resState[h];j++){
                            reroll_index[0][a+j]=h;
                        }
                        a+=resState[h];
                    }
                    int sum = 0;
                    for(int h = 0; h<6;h++){
                        sum+=resState[h];
                    }
                    tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                    tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                    tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                    tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                    tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                    tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                    for(int h = 5; h>sum-1;h--){
                        tt[h].setFill(Color.WHITE);
                    }
                    GameState gs = new GameState(boardState[0], resState);
                    List<String> actionList = gs.allAllowedActionsTS();
                    if (actionList == null || actionList.isEmpty()) {
                        actions.setText("None! You'll have to end your turn!");
                    } else {
                        actions.setText(actionList.toString());
                    }
                    //Game completion
                    String[] bsArr = boardState[0].split(",");
                    if (bsArr.length == 32) {
                        turn.setText("You finished the game!");
                        actions.setText("");
                        for (int k = 0; k < resState.length; k++) {
                            resState[k] -= resState[k];
                        }
                        resourceState.setText("");
                        roll_dice.setDisable(true);
                        trade.setDisable(true);
                        swap.setDisable(true);
                        endturn.setDisable(true);
                    }
                } else {
                    error.setText("Cannot build!");
                }
            });
        }
        //Click building Jokers, Settlements and Cities
        Polygon[] polyButtons = {J1, J2, J3, J4, J5, J6, S3, S4, S5, S7, S9, S11, C7, C12, C20, C30};
        String[] polyNames = {"J1", "J2", "J3", "J4", "J5", "J6", "S3", "S4", "S5", "S7", "S9", "S11", "C7", "C12", "C20", "C30"};
        for (int i = 0; i < polyButtons.length; i++) {
            Polygon p = polyButtons[i];
            String pName = polyNames[i];
            p.setOnMouseClicked(mouseEvent -> {
                if (CatanDice.canDoAction("build " + pName, boardState[0], resState)) {
                    p.setFill(Color.LIGHTGREEN);
                    String ze1 = "[J]+[1-6]";
                    String ze2 = "[S]+[3-9]";
                    if (pName.matches(ze1)) {
                        resState[0] -= 1;
                        resState[2] -= 1;
                        resState[1] -= 1;
                        resourceState.setText(Arrays.toString(resState));
                        int a = 0;
                        for(int h = 0; h<6;h++){
                            for(int j = 0; j<resState[h];j++){
                                reroll_index[0][a+j]=h;
                            }
                            a+=resState[h];
                        }
                        int sum = 0;
                        for(int h = 0; h<6;h++){
                            sum+=resState[h];
                        }
                        tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                        tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                        tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                        tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                        tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                        tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                        for(int h = 5; h>sum-1;h--){
                            tt[h].setFill(Color.WHITE);
                        }
                    } else if (pName.matches(ze2) || pName.equals("S11")) {
                        resState[3] -= 1;
                        resState[4] -= 1;
                        resState[2] -= 1;
                        resState[1] -= 1;
                        resourceState.setText(Arrays.toString(resState));
                        int a = 0;
                        for(int h = 0; h<6;h++){
                            for(int j = 0; j<resState[h];j++){
                                reroll_index[0][a+j]=h;
                            }
                            a+=resState[h];
                        }
                        int sum = 0;
                        for(int h = 0; h<6;h++){
                            sum+=resState[h];
                        }
                        tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                        tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                        tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                        tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                        tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                        tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                        for(int h = 5; h>sum-1;h--){
                            tt[h].setFill(Color.WHITE);
                        }
                    } else {
                        resState[0] -= 3;
                        resState[1] -= 2;
                        resourceState.setText(Arrays.toString(resState));
                        int a = 0;
                        for(int h = 0; h<6;h++){
                            for(int j = 0; j<resState[h];j++){
                                reroll_index[0][a+j]=h;
                            }
                            a+=resState[h];
                        }
                        int sum = 0;
                        for(int h = 0; h<6;h++){
                            sum+=resState[h];
                        }
                        tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                        tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                        tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                        tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                        tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                        tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                        for(int h = 5; h>sum-1;h--){
                            tt[h].setFill(Color.WHITE);
                        }
                    }
                    switch (pName) {
                        case ("J1") -> J1.setFill(new ImagePattern(oreBuilt));
                        case ("J2") -> J2.setFill(new ImagePattern(grainBuilt));
                        case ("J3") -> J3.setFill(new ImagePattern(woolBuilt));
                        case ("J4") -> J4.setFill(new ImagePattern(woodBuilt));
                        case ("J5") -> J5.setFill(new ImagePattern(brickBuilt));
                        case ("J6") -> J6.setFill(new ImagePattern(wildBuilt));
                    }

                    if (boardState[0].equals("")) boardState[0] += pName;
                    else boardState[0] = boardState[0] + "," + pName;
                    error.setText("");
                    buildornot[0] = true;
                    GameState temp = new GameState(boardState[0], resState);
                    score.setText(Integer.toString(temp.score() - turn_without_build[0] * 2));
                    roll_dice.setDisable(true);
                    BoardState bs = new BoardState(boardState[0]);
                    if (!bs.jokersAvailable().isEmpty()) {
                        swap.setDisable(false);
                    }
                    GameState gs = new GameState(boardState[0], resState);
                    List<String> actionList = gs.allAllowedActionsTS();
                    if (actionList == null || actionList.isEmpty()) {
                        actions.setText("None! You'll have to end your turn!");
                    } else {
                        actions.setText(actionList.toString());
                    }
                    //Game completion
                    String[] bsArr = boardState[0].split(",");
                    System.out.println(bsArr.length);
                    if (bsArr.length == 32) {
                        turn.setText("You finished the game!");
                        actions.setText("");
                        for (int k = 0; k < resState.length; k++) {
                            resState[k] -= resState[k];
                        }
                        resourceState.setText("");
                        roll_dice.setDisable(true);
                        trade.setDisable(true);
                        swap.setDisable(true);
                        endturn.setDisable(true);
                    }
                } else {
                    error.setText("Cannot build!");
                }
            });
        }


        //function of trade
        trade.setOnAction(actionEvent -> {
            String action = "";
            int tradeindex = 5;
            String exchange = tradeidex.getText();
            if(exchange.equals("ore")) tradeindex = 0;
            else if (exchange.equals("grain")) tradeindex = 1;
            else if (exchange.equals("wool")) tradeindex = 2;
            else if (exchange.equals("timber")) tradeindex = 3;
            else if (exchange.equals("brick")) tradeindex = 4;

            action = "trade " + Integer.toString(tradeindex);

            //Find out whether it is valid to trade
            if (!CatanDice.canDoAction(action, boardState[0], resState) ||
                    (!exchange.equals("grain") &&
                    !exchange.equals("wool") &&
                    !exchange.equals("timber") &&
                    !exchange.equals("brick")&&
                            !exchange.equals("ore")))  {
                tradeerror.setText("Cannot trade!");
            }


            else {
                tradeerror.setText("");
                //Add the corresponding resource and reduce the amount of gold
                resState[tradeindex] += 1;
                resState[5] -= 2;
                resourceState.setText(Arrays.toString(resState));
                int a = 0;
                for(int h = 0; h<6;h++){
                    for(int j = 0; j<resState[h];j++){
                        reroll_index[0][a+j]=h;
                    }
                    a+=resState[h];
                }
                int sum = 0;
                for(int h = 0; h<6;h++){
                    sum+=resState[h];
                }
                tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                for(int h = 5; h>sum-1;h--){
                    tt[h].setFill(Color.WHITE);
                }
                if (resState[5] < 2) {
                    trade.setDisable(true);
                }
                roll_dice.setDisable(true);
                GameState gs = new GameState(boardState[0], resState);
                List<String> actionList = gs.allAllowedActionsTS();
                if (actionList == null || actionList.isEmpty()) {
                    actions.setText("None! You'll have to end your turn!");
                } else {
                    actions.setText(actionList.toString());
                }
            }
        });

        swap.setOnAction(actionEvent -> {
            int swapin = 6;
            int swapout = 6;
            //Find out index of the original resource
            if(swapfrom.getText().equals("ore")) swapin = 0;
            else if (swapfrom.getText().equals("grain")) swapin = 1;
            else if (swapfrom.getText().equals("wool")) swapin = 2;
            else if (swapfrom.getText().equals("timber")) swapin = 3;
            else if (swapfrom.getText().equals("brick")) swapin = 4;
            else if (swapfrom.getText().equals("gold")) swapin = 5;

            //Find out index of the resource after swap
            if(swapto.getText().equals("ore")) swapout = 0;
            else if (swapto.getText().equals("grain")) swapout = 1;
            else if (swapto.getText().equals("wool")) swapout = 2;
            else if (swapto.getText().equals("timber")) swapout = 3;
            else if (swapto.getText().equals("brick")) swapout = 4;
            else if (swapto.getText().equals("gold")) swapout = 5;

            String action = "swap " + Integer.toString(swapin) + " " + Integer.toString(swapout);
            //Find out whether it is valid to swap
            if (!CatanDice.canDoAction(action, boardState[0], resState)) swaperror.setText("Cannot swap!");
            else {
                swaperror.setText("");
                //Add amd reduce the corresponding resources
                resState[swapout] += 1;
                resState[swapin] -= 1;
                resourceState.setText(Arrays.toString(resState));
                int a = 0;
                for(int h = 0; h<6;h++){
                    for(int j = 0; j<resState[h];j++){
                        reroll_index[0][a+j]=h;
                    }
                    a+=resState[h];
                }
                int sum = 0;
                for(int h = 0; h<6;h++){
                    sum+=resState[h];
                }
                tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                for(int h = 5; h>sum-1;h--){
                    tt[h].setFill(Color.WHITE);
                }
                //Update the board state
                String[] res1 = boardState[0].split(",");
                String usedknight = "K" + String.valueOf(swapout + 1);
                String originknight = "J" + String.valueOf(swapout + 1);
                System.out.println(usedknight);
                System.out.println(originknight);
                if (Arrays.asList(res1).contains(usedknight)) {
                    usedknight = "K6";
                    originknight = "J6";
                }
                System.out.println(usedknight);
                System.out.println(originknight);
                System.out.println(res1);
                List<String> list = new ArrayList<String>(Arrays.asList(res1));
                list.remove(originknight);
                list.add(usedknight);
                String str = "";
                for (int i = 0; i < res1.length - 1; i++) {
                    str += list.get(i) + ",";
                }
                str += list.get(res1.length - 1);
                boardState[0] = str;
                System.out.println(boardState[0]);
                System.out.println(list);
                //Change the color of knights
                String[] res = boardState[0].split(",");

                if (Arrays.asList(res).contains("J1")) J1.setFill(new ImagePattern(oreBuilt));
                else if (Arrays.asList(res).contains("K1")) J1.setFill(new ImagePattern(oreUsed));
                else J1.setFill(new ImagePattern(ore));

                if (Arrays.asList(res).contains("J2")) J2.setFill(new ImagePattern(grainBuilt));
                else if (Arrays.asList(res).contains("K2")) J2.setFill(new ImagePattern(grainUsed));
                else J2.setFill(new ImagePattern(grain));

                if (Arrays.asList(res).contains("J3")) J3.setFill(new ImagePattern(woolBuilt));
                else if (Arrays.asList(res).contains("K3")) J3.setFill(new ImagePattern(woolUsed));
                else J3.setFill(new ImagePattern(wool));

                if (Arrays.asList(res).contains("J4")) J4.setFill(new ImagePattern(woodBuilt));
                else if (Arrays.asList(res).contains("K4")) J4.setFill(new ImagePattern(woodUsed));
                else J4.setFill(new ImagePattern(wood));

                if (Arrays.asList(res).contains("J5")) J5.setFill(new ImagePattern(brickBuilt));
                else if (Arrays.asList(res).contains("K5")) J5.setFill(new ImagePattern(brickUsed));
                else J5.setFill(new ImagePattern(brick));

                if (Arrays.asList(res).contains("J6")) J6.setFill(new ImagePattern(wildBuilt));
                else if (Arrays.asList(res).contains("K6")) J6.setFill(new ImagePattern(wildUsed));
                else J6.setFill(new ImagePattern(wild));

                BoardState bs = new BoardState(boardState[0]);
                if (bs.jokersAvailable().isEmpty()) {
                    swap.setDisable(true);
                }
                GameState gs = new GameState(boardState[0], resState);
                List<String> actionList = gs.allAllowedActionsTS();
                if (actionList == null || actionList.isEmpty()) {
                    actions.setText("None! You'll have to end your turn!");
                } else {
                    actions.setText(actionList.toString());
                }
            }
        });

        //Count turns and situation of game end
        endturn.setOnAction(actionEvent -> {
            turn_number[0] += 1;
            if (turn_number[0] > 15) {
                turn.setText("Game over!");
                if (buildornot[0] == false) {
                    turn_without_build[0] += 1;
                    GameState temp = new GameState(boardState[0], resState);
                    score.setText(Integer.toString(temp.score() - turn_without_build[0] * 2));

                }
                for (int i = 0; i < resState.length; i++) {
                    resState[i] -= resState[i];
                }
                resourceState.setText("");

                roll_dice.setDisable(true);
                trade.setDisable(true);
                swap.setDisable(true);
                endturn.setDisable(true);
                actions.setText("");
                int a = 0;
                for(int h = 0; h<6;h++){
                    for(int j = 0; j<resState[h];j++){
                        reroll_index[0][a+j]=h;
                    }
                    a+=resState[h];
                }
                int sum = 0;
                for(int h = 0; h<6;h++){
                    sum+=resState[h];
                }
                tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                for(int h = 5; h>sum-1;h--){
                    tt[h].setFill(Color.WHITE);
                }

            } else {
                turn.setText(Integer.toString(turn_number[0]));
                if (buildornot[0] == false) {
                    turn_without_build[0] += 1;
                    GameState temp = new GameState(boardState[0], resState);
                    score.setText(Integer.toString(temp.score() - turn_without_build[0] * 2));

                }
                for (int i = 0; i < resState.length; i++) {
                    resState[i] -= resState[i];
                }
                resourceState.setText(Arrays.toString(resState));
                buildornot[0] = false;
                reroll_time[0] = 0;
                firstroll.setDisable(false);
                roll_dice.setDisable(true);
                trade.setDisable(true);
                swap.setDisable(true);
                endturn.setDisable(true);

                tradeidex.clear();
                swapfrom.clear();
                swapto.clear();

                resourceState.setText("");
                error.setText("");
                tradeerror.setText("");
                swaperror.setText("");
                dice_num.setText("");
                actions.setText("Roll the dice!");

                int a = 0;
                for(int h = 0; h<6;h++){
                    for(int j = 0; j<resState[h];j++){
                        reroll_index[0][a+j]=h;
                    }
                    a+=resState[h];
                }
                int sum = 0;
                for(int h = 0; h<6;h++){
                    sum+=resState[h];
                }
                tt1.setFill(new ImagePattern(image[reroll_index[0][0]]));
                tt2.setFill(new ImagePattern(image[reroll_index[0][1]]));
                tt3.setFill(new ImagePattern(image[reroll_index[0][2]]));
                tt4.setFill(new ImagePattern(image[reroll_index[0][3]]));
                tt5.setFill(new ImagePattern(image[reroll_index[0][4]]));
                tt6.setFill(new ImagePattern(image[reroll_index[0][5]]));

                for(int h = 5; h>sum-1;h--){
                    tt[h].setFill(Color.WHITE);
                }
            }
        });




        //Show the whole image of the game
        stage.setScene(new Scene(root));
        stage.setTitle("Catan Dice Game");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
