/**
 * Viewer.java
 * Authors:
 * - Shihao Wang: u7516343
 * - Patrik Haslum
 **/

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;


import java.util.Arrays;

public class Viewer extends Application {

    private static final int VIEWER_WIDTH = 1200;
    private static final int VIEWER_HEIGHT = 700;

    private final Group root = new Group();
    private final Group controls = new Group();
    private TextField playerTextField;
    private TextField boardTextField;


    /**
     * Show the state of a (single player's) board in the window.
     *
     * @param board_state: The string representation of the board state.
     */
    void displayState(String board_state) {
        //Split the board state
        String[] res = board_state.split(",");

        //create class for structure Knight
        class Knight extends Polygon {
            Knight(double x, double y, double side) {
                super(0.0, 0.0,
                        side, 0.0,
                        side + side / 2, Math.sqrt(Math.pow(side, 2) - Math.pow(side / 2, 2)),
                        side, 2 * Math.sqrt(Math.pow(side, 2) - Math.pow(side / 2, 2)),
                        0.0, 2 * Math.sqrt(Math.pow(side, 2) - Math.pow(side / 2, 2)),
                        (-1) * side / 2, Math.sqrt(Math.pow(side, 2) - Math.pow(side / 2, 2)));
                this.setLayoutX(x);
                this.setLayoutY(y);
                this.setFill(Color.LIGHTGRAY);
            }


        }

        //create class for structure Road
        class Road extends Polygon {
            Road(double x, double y) {
                super((-1)*40.0,(-1)*7.5,
                                    40,(-1)*7.5,
                                    40,7.5,
                        (-1)*40.0,7.5);
                this.setLayoutX(x);
                this.setLayoutY(y);
                this.setFill(Color.LIGHTGRAY);
            }


        }

        //create class for structure Settlement
        class Settlement extends Polygon {
            Settlement(double x, double y) {
                super(0.0, 0.0,
                        15.0,15.0,
                        10.0,15.0,
                        10.0,30.0,
                        (-1)*10.0,30.0,
                        (-1)*10.0,15.0,
                        (-1)*15.0,15.0);
                this.setLayoutX(x);
                this.setLayoutY(y);
                this.setFill(Color.LIGHTGRAY);
            }


        }

        //create class for structure City
        class City extends Polygon {
            City(double x, double y) {
                super(0.0, 0.0,
                        15.0,15.0,
                        10.0,15.0,
                        10.0,45.0,
                        (-1)*30.0,45.0,
                        (-1)*30.0,25.0,
                        (-1)*10.0,25.0,
                        (-1)*10.0,15.0,
                        (-1)*15.0,15.0);
                this.setLayoutX(x);
                this.setLayoutY(y);
                this.setFill(Color.LIGHTGRAY);
            }


        }
        //add all the knights to the root
        Knight K6 = new Knight(530,25,110);
        if (Arrays.asList(res).contains("K6"))  K6.setFill(Color.RED);
        else if (Arrays.asList(res).contains("J6")) K6.setFill(Color.LIGHTGREEN);
        root.getChildren().add(K6);

        Knight K1 = new Knight(340,130,110);
        if (Arrays.asList(res).contains("K1"))  K1.setFill(Color.RED);
        else if (Arrays.asList(res).contains("J1")) K1.setFill(Color.LIGHTGREEN);
        root.getChildren().add(K1);

        Knight K5 = new Knight(720,130,110);
        if (Arrays.asList(res).contains("K5"))  K5.setFill(Color.RED);
        else if (Arrays.asList(res).contains("J5")) K5.setFill(Color.LIGHTGREEN);
        root.getChildren().add(K5);

        Knight K2 = new Knight(340,340,110);
        if (Arrays.asList(res).contains("K2"))  K2.setFill(Color.RED);
        else if (Arrays.asList(res).contains("J2")) K2.setFill(Color.LIGHTGREEN);
        root.getChildren().add(K2);

        Knight K4 = new Knight(720,340,110);
        if (Arrays.asList(res).contains("K4"))  K4.setFill(Color.RED);
        else if (Arrays.asList(res).contains("J4")) K4.setFill(Color.LIGHTGREEN);
        root.getChildren().add(K4);

        Knight K3 = new Knight(530,455,110);
        if (Arrays.asList(res).contains("K3"))  K3.setFill(Color.RED);
        else if (Arrays.asList(res).contains("J3")) K3.setFill(Color.LIGHTGREEN);
        root.getChildren().add(K3);

        //add all the roads to the root
        Road initial = new Road(492.5,177.5);
        initial.setFill(Color.PURPLE);
        initial.setRotate(60);
        root.getChildren().add(initial);

        Road R0 = new Road(492.5,272.5);
        if (Arrays.asList(res).contains("R0"))  R0.setFill(Color.LIGHTGREEN);
        R0.setRotate(120);
        root.getChildren().add(R0);

        Road R1 = new Road(395,330);
        if (Arrays.asList(res).contains("R1"))  R1.setFill(Color.LIGHTGREEN);
        root.getChildren().add(R1);

        Road R2 = new Road(492.5,387.5);
        if (Arrays.asList(res).contains("R2"))  R2.setFill(Color.LIGHTGREEN);
        R2.setRotate(60);
        root.getChildren().add(R2);

        Road R3 = new Road(492.5,492.5);
        if (Arrays.asList(res).contains("R3"))  R3.setFill(Color.LIGHTGREEN);
        R3.setRotate(120);
        root.getChildren().add(R3);

        Road R4 = new Road(395,540);
        if (Arrays.asList(res).contains("R4"))  R4.setFill(Color.LIGHTGREEN);
        root.getChildren().add(R4);

        Road R5 = new Road(492.5,607.5);
        if (Arrays.asList(res).contains("R5"))  R5.setFill(Color.LIGHTGREEN);
        R5.setRotate(60);
        root.getChildren().add(R5);

        Road R6 = new Road(585,655);
        if (Arrays.asList(res).contains("R6"))  R6.setFill(Color.LIGHTGREEN);
        root.getChildren().add(R6);

        Road R7 = new Road(682.5,597.5);
        if (Arrays.asList(res).contains("R7"))  R7.setFill(Color.LIGHTGREEN);
        R7.setRotate(120);
        root.getChildren().add(R7);

        Road R8 = new Road(682.5,492.5);
        if (Arrays.asList(res).contains("R8"))  R8.setFill(Color.LIGHTGREEN);
        R8.setRotate(60);
        root.getChildren().add(R8);

        Road R9 = new Road(682.5,372.5);
        if (Arrays.asList(res).contains("R9"))  R9.setFill(Color.LIGHTGREEN);
        R9.setRotate(120);
        root.getChildren().add(R9);

        Road R10 = new Road(682.5,282.5);
        if (Arrays.asList(res).contains("R10"))  R10.setFill(Color.LIGHTGREEN);
        R10.setRotate(60);
        root.getChildren().add(R10);

        Road R11 = new Road(682.5,167.5);
        if (Arrays.asList(res).contains("R11"))  R11.setFill(Color.LIGHTGREEN);
        R11.setRotate(120);
        root.getChildren().add(R11);

        Road R12 = new Road(775,540);
        if (Arrays.asList(res).contains("R12"))  R12.setFill(Color.LIGHTGREEN);
        root.getChildren().add(R12);

        Road R13 = new Road(872.5,482.5);
        if (Arrays.asList(res).contains("R13"))  R13.setFill(Color.LIGHTGREEN);
        R13.setRotate(120);
        root.getChildren().add(R13);

        Road R14 = new Road(872.5,387.5);
        if (Arrays.asList(res).contains("R14"))  R14.setFill(Color.LIGHTGREEN);
        R14.setRotate(60);
        root.getChildren().add(R14);

        Road R15 = new Road(872.5,272.5);
        if (Arrays.asList(res).contains("R15"))  R15.setFill(Color.LIGHTGREEN);
        R15.setRotate(120);
        root.getChildren().add(R15);

        //add all the settlements to the root
        Settlement S3 = new Settlement(525,212);
        if (Arrays.asList(res).contains("S3"))  S3.setFill(Color.LIGHTGREEN);
        root.getChildren().add(S3);

        Settlement S4 = new Settlement(520,425);
        if (Arrays.asList(res).contains("S4"))  S4.setFill(Color.LIGHTGREEN);
        root.getChildren().add(S4);

        Settlement S5 = new Settlement(520,640);
        if (Arrays.asList(res).contains("S5"))  S5.setFill(Color.LIGHTGREEN);
        root.getChildren().add(S5);

        Settlement S7 = new Settlement(710,530);
        if (Arrays.asList(res).contains("S7"))  S7.setFill(Color.LIGHTGREEN);
        root.getChildren().add(S7);

        Settlement S9 = new Settlement(710,310);
        if (Arrays.asList(res).contains("S9"))  S9.setFill(Color.LIGHTGREEN);
        root.getChildren().add(S9);

        Settlement S11 = new Settlement(710,100);
        if (Arrays.asList(res).contains("S11"))  S11.setFill(Color.LIGHTGREEN);
        root.getChildren().add(S11);

        //add all the cities to the root
        City C7 = new City(320,310);
        if (Arrays.asList(res).contains("C7"))  C7.setFill(Color.LIGHTGREEN);
        root.getChildren().add(C7);

        City C12 = new City(320,520);
        if (Arrays.asList(res).contains("C12"))  C12.setFill(Color.LIGHTGREEN);
        root.getChildren().add(C12);

        City C20 = new City(915,400);
        if (Arrays.asList(res).contains("C20"))  C20.setFill(Color.LIGHTGREEN);
        root.getChildren().add(C20);

        City C30 = new City(915,190);
        if (Arrays.asList(res).contains("C30"))  C30.setFill(Color.LIGHTGREEN);
        root.getChildren().add(C30);

    }
    /**
     * Create a basic text field for input and a refresh button.
     */
    private void makeControls() {
        Label boardLabel = new Label("Board State:");
        boardTextField = new TextField();
        boardTextField.setPrefWidth(500);
        Button button = new Button("Show");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                displayState(boardTextField.getText());
            }
        });
        HBox hb = new HBox();
        hb.getChildren().addAll(boardLabel, boardTextField, button);
        hb.setSpacing(10);
        controls.getChildren().add(hb);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Board State Viewer");
        Scene scene = new Scene(root, VIEWER_WIDTH, VIEWER_HEIGHT);

        root.getChildren().add(controls);

        makeControls();

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
