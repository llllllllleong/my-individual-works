/**
 * AIPlayer.java
 * Authors:
 * - Leong Yin Chan: u6858120
 * Unfinished
 **/

import java.util.List;

public class AIPlayer {
    //A string of actions, in order from actions which will give the most points, to the least
    public static String actionsInPointOrder = "build C30, build C20, build C12, build C7," +
            " build S11, build S9, build S7, build S5, build S4, build S3," +
            " build R16, build R15, build R14, build R13, build R12, build R11, build R10, build R9, build R8, build R7, build R6, build R5, build R4, build R3, build R2, build R1," +
            " build ";


    //The output used to indicate the end of the turn
    //I guess that it could be null? Not sure.
    public static String[] emptyStringArr = null;


    //I assume the only input required would be the board state and resource state.
    //Is there any other input required?
    public static String[] AINextMove(String board_state, int[] resource_state) {
        BoardState bs = new BoardState(board_state);
        ResourceState rs = new ResourceState(resource_state);


        //Todo: Create a method to decide what dice to reroll or keep
        /*
        Idea 1:
        Target getting at least one of every resource
        If a you already have one of all resources, try to get equal amounts of all resources
        e.g
        [2,1,0,0,1]
        Target 1: Try to get [2,1,1,1,1]
        If all resources non zero,
        Target 2: Try to get [2,2,2,2,2]

        Idea 2:
        Target getting gold, to allow for swaps
         */


        //Update the resource state with the outcome of the dice roll
        //rs.updateAResourceIndex(index of resource, int(how much to update it);


        //Once you have finished rolling dice, decide on what action to pursue
        String[] orderedActionsArr = actionsInPointOrder.split(", ");
        List<String> outputBuildPlan;
        int[] rsAfterRoll = rs.getResources();
        for (String targetAction : orderedActionsArr) {
            GameState gs = new GameState(board_state, rsAfterRoll);
            //Find the path in the new game state
            outputBuildPlan = gs.gsBuildPlan(targetAction);

            //If there is no valid build plan
            if (outputBuildPlan.isEmpty()) {
                return emptyStringArr;
            }
            //Else, return the first valid build plan, as the for loop is ordered from max to min
            //points
            else {
                return outputBuildPlan.toArray(new String[0]);
            }
        }
        return emptyStringArr;


        //Maybe need to change gsPathTo, to use the ordered string.
    }


}
