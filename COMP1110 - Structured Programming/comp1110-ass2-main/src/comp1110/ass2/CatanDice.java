/**
 * CatanDice.java
 * Authors:
 * - Adarsh Mohan Ninganur: u7544253
 * - Leong Yin Chan: u6858120
 * - Shihao Wang: u7516343
 **/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CatanDice {
    /**
     * Returns if the structure and the number is valid
     *
     * @param structure The type of structure
     * @param number    The number associated with the structure
     * @return true if valid, else false
     */
    public static boolean check_num(char structure, int number) {
        if (structure == 'R') {
            if (number >= 0 && number <= 15) {
                return true;
            }
        }
        if (structure == 'S') {
            if (number == 3 || number == 4 || number == 5 || number == 7 || number == 9 || number == 11) {
                return true;
            }
        }
        if (structure == 'C') {
            if (number == 7 || number == 12 || number == 20 || number == 30) {
                return true;
            }
        }
        if (structure == 'J' || structure == 'K') {
            return number >= 1 && number <= 6;
        }
        return false;
    }

    /**
     * Check if the string encoding of a board state is well formed.
     * Note that this does not mean checking if the state is valid
     * (represents a state that the player could get to in game play),
     * only that the string representation is syntactically well formed.
     *
     * @param board_state: The string representation of the board state.
     * @return true iff the string is a well-formed representation of
     * a board state, false otherwise.
     */
    public static boolean isBoardStateWellFormed(String board_state) {
        if (board_state == null || board_state.equals("")) {
            return true;
        }
        String[] res = board_state.split(",");
        int num;
        char struct;
        for (int i = 0; i < res.length; i++) {
            struct = res[i].charAt(0);
            try {
                num = Integer.parseInt(res[i].substring(1));
            } catch (NumberFormatException e) {
                // If it is not a number, exception is thrown and it is catched here.
                return false;
            }
            // Check if the number is appropriate for the particular structure
            if (!check_num(struct, num)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Check if the string encoding of a player action is well formed.
     *
     * @param action: The string representation of the action.
     * @return true iff the string is a well-formed representation of
     * a board state, false otherwise.
     */
    public static boolean isActionWellFormed(String action) {
        if (action == null) {
            return false;
        }
        String[] temp = action.split(" ");
        String param1 = temp[0];
        int param2;
        int param3;
        //Build filter (uses isBoardStateWellFormed)
        if (param1.equals("build")) {
            //Includes 'K' filter
            char a = temp[1].charAt(0);
            return ((a != 'K') && (isBoardStateWellFormed(temp[1])));
        }
        //Trade filter
        if (param1.equals("trade")) {
            param2 = Integer.parseInt(temp[1]);
            return (param2 >= 0 && param2 <= 5);
        }
        //Swap Filter
        if (param1.equals("swap")) {
            param2 = Integer.parseInt(temp[1]);
            param3 = Integer.parseInt(temp[2]);
            return (param2 >= 0 && param2 <= 5 && param3 >= 0 && param3 <= 5);
        }
        return false;
    }


    /**
     * Roll the specified number of dice and add the result to the
     * resource state.
     * <p>
     * The resource state on input is not necessarily empty. This
     * method should only _add_ the outcome of the dice rolled to
     * the resource state, not remove or clear the resources already
     * represented in it.
     *
     * @param n_dice:         The number of dice to roll (>= 0).
     * @param resource_state: The available resources that the dice
     *                        roll will be added to.
     *                        <p>
     *                        This method does not return any value. It should update the given
     *                        resource_state.
     */
    public static void rollDice(int n_dice, int[] resource_state) {
        //Roll n_dice times dice and add the corresponding resource to resource_state
        if (n_dice != 0) {
            for (int i = 0; i < n_dice; i++) {
                //Use Math.random to get the dice rolling result and add the number of corresponding resource
                resource_state[(int) (0 + Math.random() * 6)] += 1;
            }
        }
    }

    /**
     * Check if the specified structure can be built next, given the
     * current board state. This method should check that the build
     * meets the constraints described in section "Building Constraints"
     * of the README file.
     *
     * @param structure:   The string representation of the structure to
     *                     be built.
     * @param board_state: The string representation of the board state.
     * @return true iff the structure is a possible next build, false
     * otherwise.
     */
    public static boolean checkBuildConstraints(String structure, String board_state) {
        BoardState bs = new BoardState(board_state);
        return (bs.checkBuildConstraints(structure));
    }


    /**
     * Check if the available resources are sufficient to build the
     * specified structure, without considering trades or swaps.
     *
     * @param structure:      The string representation of the structure to
     *                        be built.
     * @param resource_state: The available resources.
     * @return true iff the structure can be built with the available
     * resources, false otherwise.
     */
    public static boolean checkResources(String structure, int[] resource_state) {
        if (structure == null || resource_state == null) {
            return false;
        }
        if (structure.length() == 0 || resource_state.length != 6 || !isBoardStateWellFormed(structure)) {
            return false;
        }
        ResourceState rs = new ResourceState(resource_state);
        return (rs.checkBuildResourceConstraint(structure));
    }

    /**
     * Check if the available resources are sufficient to build the
     * specified structure, considering also trades and/or swaps.
     * This method needs access to the current board state because the
     * board state encodes which Knights are available to perform swaps.
     *
     * @param structure:      The string representation of the structure to
     *                        be built.
     * @param board_state:    The string representation of the board state.
     * @param resource_state: The available resources.
     * @return true iff the structure can be built with the available
     * resources, false otherwise.
     */
    public static boolean checkTrade(String structure,
                                     String board_state,
                                     int[] resource_state) {
        if (checkResources(structure, resource_state)) {
            return true;
        }
        BoardState bs = new BoardState(board_state);
        ResourceState rs = new ResourceState(resource_state);
        int gold = rs.getAResources(5);
        //If you can only trade
        if (gold >= 2) {
            //For every trade option, try to build
            for (int i = 0; i < 5; i++) {
                ResourceState firstTrade = new ResourceState(resource_state);
                //Execute the trade
                firstTrade.updateAResourceIndex(i, 1);
                firstTrade.updateAResourceIndex(5, (-2));
                //Check if you can build
                if (checkResources(structure, firstTrade.getResources())) {
                    return true;
                }
                //If you can trade twice
                if (gold >= 4) {
                    for (int j = 0; j < 5; j++) {
                        ResourceState secondTrade = new ResourceState(firstTrade.getResources());
                        //Execute the trade
                        secondTrade.updateAResourceIndex(j, 1);
                        secondTrade.updateAResourceIndex(5, (-2));
                        //Check if you can build
                        if (checkResources(structure, secondTrade.getResources())) {
                            return true;
                        }
                    }
                }
            }
        }
        //If you cannot trade (gold less than 2)
        return false;
    }


    public static boolean checkResourcesWithTradeAndSwap(String structure,
                                                         String board_state,
                                                         int[] resource_state) {
        //Check the build constraints
        if (!checkBuildConstraints(structure, board_state)) {
            return false;
        }
        //Check if the resources and constraints allow for building
        if (checkResources(structure, resource_state)) {
            return true;
        }
        BoardState bs = new BoardState(board_state);
        List<String> jokers = bs.jokersAvailable();
        //Check if you can trade, then build
        if (checkTrade(structure, board_state, resource_state)) {
            return true;
        }
        //If you can swap, trade, then build
        if (jokers.size() != 0) {
            ResourceState swapAllRS = new ResourceState(resource_state);
            BoardState swapAllBS = new BoardState(board_state);
            //Swap all the jokers except J6 and add Knights to the board state
            //Also update resources
            for (String s : jokers) {
                if (!s.equals("J6")) {
                    swapAllRS.swapJokers(s);
                    int swapFor = (Integer.parseInt(String.valueOf(s.charAt(1))) - 1);
                    swapAllBS.updateBSForSwap(swapFor);
                }
            }
            //After swapping all jokers, check if you can trade, then build
            if (checkTrade(structure, swapAllBS.getBoardState(), swapAllRS.getResources())) {
                return true;
            }
            //Otherwise, check if J6 is available
            if (jokers.contains("J6")) {
                //For all the possible J6 swaps, check if you can build, or trade and build
                for (int i = 0; i < 6; i++) {
                    ResourceState swapAllRSJ6 = new ResourceState(swapAllRS.getResources());
                    BoardState swapAllBSJ6 = new BoardState(swapAllBS.getBoardState());
                    swapAllRSJ6.updateAResourceIndex(i, 1);
                    swapAllBSJ6.updateBSForSwap(i);
                    if (checkTrade(structure, swapAllBSJ6.getBoardState(), swapAllRSJ6.getResources())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    /**
     * Check if a player action (build, trade or swap) is executable in the
     * given board and resource state.
     *
     * @param action:         String representatiion of the action to check.
     * @param board_state:    The string representation of the board state.
     * @param resource_state: The available resources.
     * @return true iff the action is applicable, false otherwise.
     */
    public static boolean canDoAction(String action, String board_state, int[] resource_state) {
        BoardState bs = new BoardState(board_state);
        ResourceState rs = new ResourceState(resource_state);
        String[] temp = action.split(" ");
        String param1 = temp[0];
        String param2 = temp[1];
        //Check if can build
        if (param1.equals("build")) {
            return (rs.checkBuildResourceConstraint(param2) &&
                    bs.checkBuildConstraints(param2));
        }
        //Check if can trade
        if (param1.equals("trade")) {
            int gold = rs.getAResources(5);
            return (2 <= gold);
        }
        //Check if can swap
        if (param1.equals("swap")) {
            int param3 = Integer.parseInt(temp[2]);
            //Check if you have the resource to swap
            if (1 <= rs.getAResources(Integer.parseInt(param2))) {
                //Check if you have the corresponding joker available
                return switch (param3) {
                    case (0) -> (bs.isAlreadyBuilt("J1") ||
                            bs.isAlreadyBuilt("J6"));
                    case (1) -> (bs.isAlreadyBuilt("J2") ||
                            bs.isAlreadyBuilt("J6"));
                    case (2) -> (bs.isAlreadyBuilt("J3") ||
                            bs.isAlreadyBuilt("J6"));
                    case (3) -> (bs.isAlreadyBuilt("J4") ||
                            bs.isAlreadyBuilt("J6"));
                    case (4) -> (bs.isAlreadyBuilt("J5") ||
                            bs.isAlreadyBuilt("J6"));
                    default -> false;
                };
            }
            return false;
        }
        return false;
    }

    /**
     * Check if the specified sequence of player actions is executable
     * from the given board and resource state.
     *
     * @param actions:        The sequence of (string representatins of) actions.
     * @param board_state:    The string representation of the board state.
     * @param resource_state: The available resources.
     * @return true iff the action sequence is executable, false otherwise.
     */
    public static boolean canDoSequence(String[] actions, String board_state, int[] resource_state) {
        ResourceState rs = new ResourceState(resource_state);
        BoardState bs = new BoardState(board_state);
        while (0 < actions.length) {
            //The first action
            String firstAction = actions[0];
            String[] detailsSplit = firstAction.split(" ");
            //The specific first action parameter(build, trade, swap)
            String param1 = detailsSplit[0];
            //The second parameter
            String param2 = detailsSplit[1];
            if (param1.equals("build") && canDoAction(firstAction, bs.getBoardState(), rs.getResources())) {
                //Deduct the resources needed
                char structureChar = param2.charAt(0);
                rs.buildDeductResources(structureChar);
                //Add the built structure to the board state
                bs.addToBoardState(param2);
            } else if (param1.equals("trade") && canDoAction(firstAction, bs.getBoardState(), rs.getResources())) {
                //Execute the trade
                rs.updateAResource("gold", -2);
                rs.updateAResourceIndex((Integer.parseInt(param2)), +1);
            } else if (param1.equals("swap") && canDoAction(firstAction, bs.getBoardState(), rs.getResources())) {
                int param3 = Integer.parseInt(String.valueOf(detailsSplit[2]));
                //Change the joker to a knight, on the board state
                bs.updateBSForSwap(param3);
                rs.updateAResourceIndex((Integer.parseInt(param2)), -1);
                rs.updateAResourceIndex(param3, +1);
            } else {
                return false;
            }
            actions = Arrays.copyOfRange(actions, 1, actions.length);
        }
        return true;
    }

//

    /**
     * Used to check if the structure appears already in the board state.
     *
     * @param arr:    The array of the structures in the board structure
     * @param tofind: The structure to find in the array
     * @return true if the tofind structure appears in the array, else returns false.
     */
    public static boolean check_struct(String[] arr, String tofind) {
        if (arr.length == 0) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(tofind)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Find the path of roads that need to be built to reach a specified
     * (unbuilt) structure in the current board state. The roads should
     * be returned as an array of their string representation, in the
     * order in which they have to be built. The array should _not_ include
     * the target structure (even if it is a road). If the target structure
     * is reachable via the already built roads, the method should return
     * an empty array.
     * <p>
     * Note that on the Island One map, there is a unique path to every
     * structure.
     *
     * @param target_structure: The string representation of the structure
     *                          to reach.
     * @param board_state:      The string representation of the board state.
     * @return An array of string representations of the roads along the
     * path.
     */
    public static String[] pathTo(String target_structure, String board_state) {

        // Board state must be well-formed
        if (!isBoardStateWellFormed(board_state)) {
            return new String[0];
        }

        if (target_structure.charAt(0) == 'J') {
            int targetJokerID = Integer.parseInt(String.valueOf(target_structure.charAt(1)));
            BoardState bs = new BoardState(board_state);
            String s = "";
            for (int i = 1; i < targetJokerID; i++) {
                if (!bs.isAlreadyBuilt("J" + i) && !bs.isAlreadyBuilt("K" + i)) {
                    s = s + "J" + i + ",";
                }
            }
            if (s.equals("")) {
                return null;
            }
            s = s.substring(0, s.length() - 1);
            String[] output = s.split(",");
            return output;
        }


        List<String> result = new ArrayList<String>();
        List<String> rbranch = new ArrayList<String>();

        // Split the board state into structures
        String[] structs = board_state.split(",");

        Traverse x = new Traverse();
        // head is the starting node of the path
        Node head;
        // Calling the function createList in Traverse Class
        head = x.createList();
        // temp points to head of the Linked List
        Node temp = head;
        // branch is used to traverse the branched parts of the path
        Node branch;

        // The string array (of the path) returned by the function
        String[] rfinal;

        // Loop till the main path does not reach null node
        while (temp != null) {
            if (temp.structure.charAt(0) == 'R') {
                // If a road is encountered in the path
                if (!temp.structure.equals(target_structure)) {
                    // The target structure should not be in the path
                    result.add(temp.structure);
                }
                if (check_struct(structs, temp.structure)) {
                    // If the structure already exists on the board, clear the resultant list
                    // As the path can be started from here
                    result.clear();
                }
            }
            if (temp.structure.equals(target_structure)) {
                // If the target structure is found
                rfinal = result.toArray(String[]::new);
                // Convert the List to an Array and return
                return rfinal;
            }
            temp = temp.n1;
            if (temp != null && temp.n2 != null) {
                if (temp.structure.charAt(0) == 'R') {
                    if (!temp.structure.equals(target_structure)) {
                        // The target structure should not be in the path
                        result.add(temp.structure);
                    }
                    if (check_struct(structs, temp.structure)) {
                        // If the structure already exists on the board, clear the resultant list
                        result.clear();
                    }
                }
                if (temp.structure.equals(target_structure)) {
                    // If the target structure is found
                    rfinal = result.toArray(String[]::new);
                    // Convert the List to an Array and return
                    return rfinal;
                }
                branch = temp.n2;
                while (branch != null) {
                    if (branch.structure.charAt(0) == 'R') {
                        if (!branch.structure.equals(target_structure)) {
                            // The target structure should not be in the path
                            rbranch.add(branch.structure);
                        }
                        if (check_struct(structs, branch.structure)) {
                            // If the structure already exists on the board, clear the list
                            rbranch.clear();
                        }
                    }
                    if (branch.structure.equals(target_structure)) {
                        // If the target structure is found in the branch
                        // Merge the two lists, convert the List to an Array and return it
                        result.addAll(rbranch);
                        rfinal = result.toArray(String[]::new);
                        return rfinal;
                    }
                    // Pointing to the next Node
                    branch = branch.n1;
                }
                // Clearing the branch list as the target structure was not found in the previous branch
                rbranch.clear();

                // Pointing temp to the next Node
                temp = temp.n1;
            }
        }

        // When the path is not found
        return new String[0];
    }

    /**
     * Generate a plan (sequence of player actions) to build the target
     * structure from the given board and resource state. The plan may
     * include trades and swaps, as well as building other structures if
     * needed to reach the target structure or to satisfy the build order
     * constraints.
     * <p>
     * However, the plan must not have redundant actions. This means it
     * must not build any other structure that is not necessary to meet
     * the building constraints for the target structure, and it must not
     * trade or swap for resources if those resources are not needed.
     * <p>
     * If there is no valid build plan for the target structure from the
     * specified state, return null.
     *
     * @param target_structure: The string representation of the structure
     *                          to be built.
     * @param board_state:      The string representation of the board state.
     * @param resource_state:   The available resources.
     * @return An array of string representations of player actions. If
     * there exists no valid build plan for the target structure,
     * the method should return null.
     */
    public static String[] buildPlan(String target_structure,
                                     String board_state,
                                     int[] resource_state) {
        if (target_structure == null || board_state == null || resource_state == null) {
            return null;
        }
        List<String> outputBuildPlan;
        //Create a new game state
        GameState gs = new GameState(board_state, resource_state);
        //Find the path in the new game state
        outputBuildPlan = gs.gsBuildPlan(target_structure);

        //If there is no valid build plan
        if (outputBuildPlan == null || outputBuildPlan.size() == 0) {
            return null;
        }
        //Convert the list into a string array, and return
        return outputBuildPlan.toArray(new String[0]);
    }


}
