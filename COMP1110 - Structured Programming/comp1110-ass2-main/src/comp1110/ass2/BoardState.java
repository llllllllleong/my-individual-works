/**
 * BoardState.java
 * Authors:
 * - Leong Yin Chan: u6858120
 **/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class BoardState {
    private String builtStructures;


    public BoardState(String board_state) {
        builtStructures = board_state;
    }

    public String getBoardState() {
        return builtStructures;
    }

    //Must be a two character input, e.g. J3
    public void addToBoardState(String item) {
        builtStructures = builtStructures + "," + item;
    }

    public void removeFromBoardState(String item) {
        String[] temp = builtStructures.split(",");
        if (isAlreadyBuilt(item)) {
            int index = 0;
            for (int i = 0; i < temp.length; i++) {
                if (!Objects.equals(temp[i], item)) {
                    temp[index++] = temp[i];
                }
            }
            String[] newBoardStateArr = Arrays.copyOf(temp, index);
            String newBoardState = "";
            for (int i = 0; i < newBoardStateArr.length; i++) {
                newBoardState = newBoardState + newBoardStateArr[i] + ",";
            }
            newBoardState = newBoardState.substring(0, newBoardState.length() - 1);
            builtStructures = newBoardState;
        }

    }

    public boolean isAlreadyBuilt(String structure) {
        String[] temp = builtStructures.split(",");
        for (String element : temp) {
            if (element.equals(structure)) {
                return true;
            }
        }
        return false;
    }


    //Returns true if you can build
    public boolean checkBuildConstraints(String structure) {
        char structureChar = structure.charAt(0);
        int ID = Integer.parseInt(structure.substring(1));
        //First check if the structure is already build
        if (isAlreadyBuilt(structure)) {
            return false;
        }
        switch (structureChar) {
            case ('R'):
                if (ID == 2 || ID == 5) {
                    String temp = new StringBuilder().append(structureChar).append(ID - 2).toString();
                    return (isAlreadyBuilt(temp));
                } else if (ID == 12) {
                    String temp = new StringBuilder().append(structureChar).append(ID - 5).toString();
                    return (isAlreadyBuilt(temp));
                } else if (ID == 0) {
                    return true;
                } else {
                    String temp = new StringBuilder().append(structureChar).append(ID - 1).toString();
                    return (isAlreadyBuilt(temp));
                }
            case ('S'):
                if (ID == 3) {
                    return true;
                } else if (ID == 4) {
                    return (isAlreadyBuilt("R2") && isAlreadyBuilt("S3"));
                } else if (ID == 5) {
                    return (isAlreadyBuilt("R5") && isAlreadyBuilt("S4"));
                } else if (ID == 7) {
                    return (isAlreadyBuilt("R7") && isAlreadyBuilt("S5"));
                } else if (ID == 9) {
                    return (isAlreadyBuilt("R9") && isAlreadyBuilt("S7"));
                } else if (ID == 11) {
                    return (isAlreadyBuilt("R11") && isAlreadyBuilt("S9"));
                } else {
                    return false;
                }
            case ('C'):
                if (ID == 7) {
                    return (isAlreadyBuilt("R1"));
                } else if (ID == 12) {
                    return (isAlreadyBuilt("R4") && isAlreadyBuilt("C7"));
                } else if (ID == 20) {
                    return (isAlreadyBuilt("R13") && isAlreadyBuilt("C12"));
                } else if (ID == 30) {
                    return (isAlreadyBuilt("R15") && isAlreadyBuilt("C20"));
                } else {
                    return false;
                }
            case ('J'):
                if (ID == 1) {
                    return (!(isAlreadyBuilt("J1") || isAlreadyBuilt("K1")));
                } else if (ID == 2) {
                    return ((isAlreadyBuilt("J1") || isAlreadyBuilt("K1")) &&
                            !((isAlreadyBuilt("J2") || isAlreadyBuilt("K2"))));
                } else if (ID == 3) {
                    return ((isAlreadyBuilt("J2") || isAlreadyBuilt("K2")) &&
                            !((isAlreadyBuilt("J3") || isAlreadyBuilt("K3"))));
                } else if (ID == 4) {
                    return ((isAlreadyBuilt("J3") || isAlreadyBuilt("K3")) &&
                            !((isAlreadyBuilt("J4") || isAlreadyBuilt("K4"))));
                } else if (ID == 5) {
                    return ((isAlreadyBuilt("J4") || isAlreadyBuilt("K4")) &&
                            !((isAlreadyBuilt("J5") || isAlreadyBuilt("K5"))));
                } else if (ID == 6) {
                    return ((isAlreadyBuilt("J5") || isAlreadyBuilt("K5")) &&
                            !((isAlreadyBuilt("J6") || isAlreadyBuilt("K6"))));
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

    public void updateBSForSwap(int swapGet) {
        switch (swapGet) {
            case (0):
                if (isAlreadyBuilt("J1")) {
                    removeFromBoardState("J1");
                    addToBoardState("K1");
                } else {
                    removeFromBoardState("J6");
                    addToBoardState("K6");
                }
                break;
            case (1):
                if (isAlreadyBuilt("J2")) {
                    removeFromBoardState("J2");
                    addToBoardState("K2");
                } else {
                    removeFromBoardState("J6");
                    addToBoardState("K6");
                }
                break;
            case (2):
                if (isAlreadyBuilt("J3")) {
                    removeFromBoardState("J3");
                    addToBoardState("K3");
                } else {
                    removeFromBoardState("J6");
                    addToBoardState("K6");
                }
                break;
            case (3):
                if (isAlreadyBuilt("J4")) {
                    removeFromBoardState("J4");
                    addToBoardState("K4");
                } else {
                    removeFromBoardState("J6");
                    addToBoardState("K6");
                }
                break;
            case (4):
                if (isAlreadyBuilt("J5")) {
                    removeFromBoardState("J5");
                    addToBoardState("K5");
                } else {
                    removeFromBoardState("J6");
                    addToBoardState("K6");
                }
                break;
        }
    }

    public List<String> jokersAvailable() {
        List<String> output = new ArrayList<>();
        String[] temp = builtStructures.split(",");
        for (String current : temp) {
            String firstChar = String.valueOf(current.charAt(0));
            if (firstChar.equals("J")) {
                output.add(current);
            }
        }
        return output;
    }

}
