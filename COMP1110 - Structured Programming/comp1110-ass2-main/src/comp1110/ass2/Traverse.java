/**
 * Traverse.java
 * Authors:
 * - Adarsh Mohan Ninganur: u7544253
 **/

public class Traverse {

    // head is the starting Node of the entire path
    Node head;

    // Method to create the structure of the game
    public Node createList()
    {
        // Starting Settlement
        this.head = new Node("S3");
        Node curr = head, branch;

        // First road
        curr.n1 = new Node("R0");
        curr = curr.n1;

        // Two nodes branching out here
        curr.n1 = new Node("R2");
        curr.n2 = new Node("R1");

        // Branch here refers to the shorter end of the path
        // R1 part
        branch = curr.n2;
        branch.n1 = new Node("C7");

        // R2 part
        curr = curr.n1;
        // the current pointer is set to the node which the curr is pointing to
        curr.n1 = new Node("S4");

        curr = curr.n1;
        // New node is initialized using parameterized constructor
        curr.n1 = new Node("R3");
        curr = curr.n1;

        // Two nodes branching out here
        curr.n1 = new Node("R5");
        curr.n2 = new Node("R4");

        // R4 part
        branch = curr.n2;
        branch.n1 = new Node("C12");

        // R5 part
        curr = curr.n1;
        curr.n1 = new Node("S5");

        curr = curr.n1;
        curr.n1 = new Node("R6");

        curr = curr.n1;
        curr.n1 = new Node("R7");

        curr = curr.n1;
        curr.n1 = new Node("S7");
        curr = curr.n1;

        // Two nodes branching out here
        curr.n1 = new Node("R8");
        curr.n2 = new Node("R12");

        // R12 part
        branch = curr.n2;
        branch.n1 = new Node("R13");

        branch = branch.n1;
        branch.n1 = new Node("C20");

        branch = branch.n1;
        branch.n1 = new Node("R14");

        branch = branch.n1;
        branch.n1 = new Node("R15");

        branch = branch.n1;
        branch.n1 = new Node("C30");

        // R8 part
        curr = curr.n1;
        curr.n1 = new Node("R9");

        curr = curr.n1;
        curr.n1 = new Node("S9");

        curr = curr.n1;
        curr.n1 = new Node("R10");

        curr = curr.n1;
        curr.n1 = new Node("R11");

        curr = curr.n1;
        curr.n1 = new Node("S11");

        // returns the starting node
        return head;
    }
}