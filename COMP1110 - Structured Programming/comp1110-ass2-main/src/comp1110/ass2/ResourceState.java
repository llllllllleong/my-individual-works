/**
 * ResourceState.java
 * Authors:
 * - Leong Yin Chan: u6858120
 **/

public class ResourceState {
    private int ore;
    private int grain;
    private int wool;
    private int timber;
    private int brick;
    private int gold;

    public ResourceState(int[] resource_state) {
        ore = resource_state[0];
        grain = resource_state[1];
        wool = resource_state[2];
        timber = resource_state[3];
        brick = resource_state[4];
        gold = resource_state[5];
    }

    public int[] getResources() {
        int[] resources = {ore, grain, wool, timber, brick, gold};
        return resources;
    }

    public int getAResources(int index) {
        return switch (index) {
            case 0 -> ore;
            case 1 -> grain;
            case 2 -> wool;
            case 3 -> timber;
            case 4 -> brick;
            case 5 -> gold;
            default -> 1000;
        };
    }

    public void updateAResource(String resource, int update) {
        switch (resource) {
            case "ore":
                ore = ore + update;
                break;
            case "grain":
                grain = grain + update;
                break;
            case "wool":
                wool = wool + update;
                break;
            case "timber":
                timber = timber + update;
                break;
            case "brick":
                brick = brick + update;
                break;
            case "gold":
                gold = gold + update;
                break;
        }
    }

    public void updateAResourceIndex(int index, int update) {
        switch (index) {
            case 0:
                ore = ore + update;
                break;
            case 1:
                grain = grain + update;
                break;
            case 2:
                wool = wool + update;
                break;
            case 3:
                timber = timber + update;
                break;
            case 4:
                brick = brick + update;
                break;
            case 5:
                gold = gold + update;
                break;
        }
    }

    public boolean checkBuildResourceConstraint(String structure) {
        char structureChar = structure.charAt(0);
        switch (structureChar) {
            case ('R'):
                return (timber >= 1 && brick >= 1);
            case ('S'):
                return (grain >= 1 && wool >= 1 && timber >= 1 && brick >= 1);
            case ('C'):
                return (ore >= 3 && grain >= 2);
            case ('J'):
                return (ore >= 1 && grain >= 1 && wool >= 1);
            default:
                return false;
        }
    }

    public void buildDeductResources(char structureChar) {
        switch (structureChar) {
            case ('R'):
                updateAResource("timber", -1);
                updateAResource("brick", -1);
                break;
            case ('S'):
                updateAResource("grain", -1);
                updateAResource("wool", -1);
                updateAResource("timber", -1);
                updateAResource("brick", -1);
                break;
            case ('C'):
                updateAResource("ore", -3);
                updateAResource("grain", -2);
                break;
            case ('J'):
                updateAResource("ore", -1);
                updateAResource("grain", -1);
                updateAResource("wool", -1);
                break;
        }
    }


    //Update the resource state when the corresponding joker is used
    //Does not work for J6
    public void swapJokers(String joker) {
        switch (joker) {
            case ("J1") -> updateAResourceIndex(0, 1);
            case ("J2") -> updateAResourceIndex(1, 1);
            case ("J3") -> updateAResourceIndex(2, 1);
            case ("J4") -> updateAResourceIndex(3, 1);
            case ("J5") -> updateAResourceIndex(4, 1);
        }
    }

//Checks a resource state for negative numbers
    public static boolean wellFormedRS(int[] rs) {
        for (int i : rs) {
            if (i < 0) {
                return false;
            }
        }
        return true;
    }




}
