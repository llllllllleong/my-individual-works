/**
 * Node.java
 * Authors:
 * - Adarsh Mohan Ninganur: u7544253
 **/

public class Node {

    // Structure, for example can be of type R2/S5/C20
    String structure;
    // Indicates the next Node pointer: next1
    Node n1;
    // Indicates the next Node pointer: next2
    Node n2;


    Node(String structure){
        this.structure = structure;
        // Set the n1 and n2 pointer of this Node to null by default.
        this.n1 = null;
        this.n2 = null;
    }

    public String toString(){
        // If a Node object is printed, the Node's structure is printed
        return this.structure;
    }
}