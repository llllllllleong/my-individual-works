/**
 * GameState.java
 * Authors:
 * - Leong Yin Chan: u6858120
 **/

import java.util.ArrayList;
import java.util.List;

import static CatanDice.*;

public class GameState {
    private String boardState;
    private int[] resourceState;

    public GameState(String a, int[] b) {
        boardState = a;
        resourceState = b;
    }

    public String getBoardState() {
        return boardState;
    }

    public int[] getResourceState() {
        return resourceState;
    }

    //Finds the sequence of actions required to execute a final action target
    //Returns an ordered list of actions to be executed, if the final action target is possible
    //Returns null if the final action target is not possible
    public List<String> gsBuildPlan(String targetStructure) {
        //Change the target structure into a target action
        String targetAction = "build " + targetStructure;
        List<String> output = new ArrayList<>();
        //Terminating condition
        if (canDoAction(targetAction, boardState, resourceState)) {
            output.add(targetAction);
            return output;
        }
        String firstStructure = "";
        //Create a pathTo
        String[] pathTo = pathTo(targetStructure, getBoardState());
        if (pathTo == null || pathTo.length == 0) {
            firstStructure = targetStructure;
        } else {
            firstStructure = pathTo[0];
        }
        //Try to build the first action of the pathTo
        String firstAction = "build " + firstStructure;
        //If you can build, add the first action to the output, create a new game
        //state with the updated board state after trading, and recurse the method
        if (canDoAction(firstAction, getBoardState(), getResourceState())) {
            GameState gss = new GameState(getBoardState(), getResourceState());
            gss.updateGameState(firstAction);
            List<String> gssOutput = gss.gsBuildPlan(targetStructure);
            //If no build plan available
            if (gssOutput == null || gssOutput.isEmpty()) {
                return null;
            } else {
                gssOutput.add(0, firstAction);
                output = gssOutput;
                return output;
            }
        }
        //If there are no allowed trade or swaps available, then it is not possible to reach the
        //target, therefore return null
        List<String> rsActions = allAllowedActions();
        if (rsActions == null) {
            return null;
        }
        //If there are allowed actions available
        List<String> currentPath = new ArrayList<>();
        int maxActionLength = 100;
        for (String s : rsActions) {
            GameState gss = new GameState(getBoardState(), getResourceState());
            gss.updateGameState(s);
            List<String> path = gss.gsBuildPlan(targetStructure);
            if (path != null && !path.isEmpty() && path.get(path.size() - 1).equals(targetAction)) {
                path.add(0, s);
                if (path.size() <= maxActionLength) {
                    maxActionLength = path.size();
                    currentPath = path;
                }
            }
        }
        return currentPath;
    }


    //Update the game state with a given action, assuming the given action is valid
    public void updateGameState(String action) {
        BoardState bs = new BoardState(getBoardState());
        ResourceState rs = new ResourceState(getResourceState());
        String[] detailsSplit = action.split(" ");
        //The specific first action parameter(build, trade, swap)
        String param1 = detailsSplit[0];
        //The second parameter
        String param2 = detailsSplit[1];
        switch (param1) {
            case "build" -> {
                //Deduct the resources needed
                char structureChar = param2.charAt(0);
                rs.buildDeductResources(structureChar);
                //Add the built structure to the board state
                bs.addToBoardState(param2);
            }
            case "trade" -> {
                //Execute the trade
                rs.updateAResource("gold", -2);
                rs.updateAResourceIndex((Integer.parseInt(param2)), +1);
            }
            case "swap" -> {
                int param3 = Integer.parseInt(String.valueOf(detailsSplit[2]));
                //Change the joker to a knight, on the board state
                bs.updateBSForSwap(param3);
                rs.updateAResourceIndex((Integer.parseInt(param2)), -1);
                rs.updateAResourceIndex(param3, +1);
            }
        }
        boardState = bs.getBoardState();
        resourceState = rs.getResources();

    }


    //Generates a list of all actions allowed, including actions with do not lead to the
    //target action
    public List<String> allAllowedActions() {
        //Generate all possible actions
        List<String> allActions = generateAllActions();
        //Filter the actions which are valid
        List<String> allAllowedActions = new ArrayList<>();
        for (String s : allActions) {
            if (canDoAction(s, getBoardState(), getResourceState())) {
                allAllowedActions.add(s);
            }
        }
        return allAllowedActions;
    }

    public List<String> allAllowedActionsTS() {
        List<String> allActions = allAllowedActions();
        List<String> output = new ArrayList<>();
        for (String s : allActions) {
            String[] temp = s.split(" ");
            String param1 = temp[0];
            int param2;
            int param3;
            String out = "";
            if (param1.equals("build")) {
                char a = temp[1].charAt(0);
                switch (a) {
                    case ('R'):
                        out = "Build  Road " + temp[1].substring(1);
                        output.add(out);
                        break;
                    case ('S'):
                        out = "Build  Settlement " + temp[1].substring(1);
                        output.add(out);
                        break;
                    case ('C'):
                        out = "Build  City " + temp[1].substring(1);
                        output.add(out);
                        break;
                    case ('J'):
                        int b = Integer.valueOf(String.valueOf(temp[1].charAt(1)));
                        switch (b) {
                            case (1):
                                out = "Build Ore Joker";
                                output.add(out);
                                break;
                            case (2):
                                out = "Build Grain Joker";
                                output.add(out);
                                break;
                            case (3):
                                out = "Build Wool Joker";
                                output.add(out);
                                break;
                            case (4):
                                out = "Build Timber Joker";
                                output.add(out);
                                break;
                            case (5):
                                out = "Build Brick Joker";
                                output.add(out);
                                break;
                            case (6):
                                out = "Build Wildcard Joker";
                                output.add(out);
                                break;
                        }
                }
            }
            if (param1.equals("trade")) {
                param2 = Integer.parseInt(temp[1]);
                switch (param2) {
                    case (0):
                        out = "Trade for Ore";
                        output.add(out);
                        break;
                    case (1):
                        out = "Trade for Grain";
                        output.add(out);
                        break;
                    case (2):
                        out = "Trade for Wool";
                        output.add(out);
                        break;
                    case (3):
                        out = "Trade for Timber";
                        output.add(out);
                        break;
                    case (4):
                        out = "Trade for Brick";
                        output.add(out);
                        break;
                }
            }
            if (param1.equals("swap")) {
                param2 = Integer.parseInt(temp[1]);
                param3 = Integer.parseInt(temp[2]);
                String out2 = "";
                switch (param2) {
                    case (0):
                        out2 = "Swap Ore for ";
                        break;
                    case (1):
                        out2 = "Swap Grain for ";
                        break;
                    case (2):
                        out2 = "Swap Wool for ";
                        break;
                    case (3):
                        out2 = "Swap Timber for ";
                        break;
                    case (4):
                        out2 = "Swap Brick for ";
                        break;
                    case (5):
                        out2 = "Swap Gold for ";
                        break;
                }
                switch (param3) {
                    case (0):
                        out = out2 + "Ore";
                        output.add(out);
                        break;
                    case (1):
                        out = out2 + "Grain";
                        output.add(out);
                        break;
                    case (2):
                        out = out2 + "Wool";
                        output.add(out);
                        break;
                    case (3):
                        out = out2 + "Timber";
                        output.add(out);
                        break;
                    case (4):
                        out = out2 + "Brick";
                        output.add(out);
                        break;
                    case (5):
                        out = out2 + "Gold";
                        output.add(out);
                        break;
                }
            }
        }
        return output;
    }






    //Generates all possible resource actions, valid and invalid
    public static List<String> generateAllActions() {
        List<String> allActions = new ArrayList<>();
        for (int i = 30; i > -1; i--) {
            allActions.add("build C" + i);
        }
        for (int i = 11; i > 2; i--) {
            allActions.add("build S" + i);
        }

        for (int i = 15; i > -1; i--) {
            allActions.add("build R" + i);
        }
        for (int i = 6; i > 0; i--) {
            allActions.add("build J" + i);
        }
        for (int i = 0; i < 5; i++) {
            allActions.add("trade " + i);
        }
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (i != j) {
                    allActions.add("swap " + j + " " + i);
                }
            }
        }
        return allActions;
    }


    //Returns the score of the boardState
    public int score() {
        if (boardState.equals("")) {
            return 0;
        }
        int score = 0;
        String[] temp = boardState.split(",");
        for (String element : temp) {
            char structureChar = element.charAt(0);
            int value = Integer.parseInt(element.substring(1));
            if (structureChar == 'R') {
                score++;
            } else {
                score = score + value;
            }
        }
        return score;
    }


    public static void main(String[] args) {
        String targetStructure = "C7";
        //Change the target structure into a target action
        String targetAction = "build " + targetStructure;
        System.out.println(targetAction);
    }
}
