/**
 * U6858120Tests.java
 * Authors:
 * - Leong Yin Chan: u6858120
 **/


import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Game;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import static org.junit.jupiter.api.Assertions.assertEquals;

// @org.junit.jupiter.api.Timeout(value = 1000, unit = MILLISECONDS)

@Timeout(
        value = 1000L,
        unit = TimeUnit.MILLISECONDS
)

public class U6858120Tests {
    int[] emptyRS = {0, 0, 0, 0, 0, 0};


    private void testTask4(String in, boolean expected) {
        String errorPrefix = "Test for Task4: isActionWellFormed(\"" + in + "\")";
        boolean out = CatanDice.isActionWellFormed(in);
        assertEquals(expected, out, errorPrefix);
    }

    @Test
    public void task4Tests() {
        //Test for empty string input
        testTask4("", false);
        //Test for null input
        testTask4(null, false);
        //Normal build
        testTask4("build R12", true);
        //Building out of bounds
        testTask4("build R23", false);
        testTask4("build R-2", false);
        testTask4("build R100", false);
        //Should not be able to build knights
        testTask4("build K1", false);
        testTask4("build K2", false);
        testTask4("build K3", false);
        testTask4("build K4", false);
        testTask4("build K5", false);
        testTask4("build K6", false);
        //Should be able to build jokers
        testTask4("build J1", true);
        testTask4("build J2", true);
        testTask4("build J3", true);
        testTask4("build J4", true);
        testTask4("build J5", true);
        testTask4("build J6", true);
    }


    private void testTask7(String structure, int[] resourceState, boolean expected) {
        String errorPrefix = "Test for Task7: checkResources(\"" + structure +
                "and" + Arrays.toString(resourceState) + "\")";
        boolean out = CatanDice.checkResources(structure, resourceState);
        assertEquals(expected, out, errorPrefix);
    }

    @Test
    public void task7Tests() {
        int[] testRS1 = {1, 1, 1, 1, 1, 1};
        int[] maxRS = {5, 5, 5, 5, 5, 5};
        int[] wrongLengthRS = {5, 5, 5};
        int[] wrongLengthRS2 = {5, 5, 5, 5, 5, 5, 5};
        //Test for empty string input
        testTask7("", testRS1, false);
        //Test for null input
        testTask7(null, testRS1, false);
        testTask7("R1", null, false);
        //Incorrect resource state input
        testTask7("R1", wrongLengthRS, false);
        testTask7("R1", wrongLengthRS2, false);
        testTask7("R1", maxRS, true);
        //Incorrect build parameter
        testTask7("R122", maxRS, false);
        testTask7("C1", maxRS, false);
    }


    private void testTask11(String structure,
                            String board_state,
                            int[] resource_state,
                            boolean expected) {
        String errorPrefix = "Error Task11:(\"" + structure
                + "\", \"" + board_state
                + "\", \"" + Arrays.toString(resource_state)
                + "\"";
        boolean out = CatanDice.checkResourcesWithTradeAndSwap(
                structure,
                board_state,
                resource_state);
        assertEquals(expected, out, errorPrefix);
    }

    @Test
    public void task11Tests() {
        String allJokersBS = "J1,J2,J3,J4,J5,J6";
        int[] tradeThenSwapRS = {0, 0, 0, 0, 0, 2};

        //Test for trade and swap
        String tradeThenSwapBS = "J1,J2,J3,J4,J5,J6,R0,S3";
        testTask11("R1", tradeThenSwapBS, tradeThenSwapRS, true);

        //Test for trade twice, and swap
        int[] tradeThenSwapRS2 = {0, 0, 0, 0, 0, 4};
        String tradeThenSwapBS2 = "R0,S3";
        testTask11("R1", tradeThenSwapBS2, tradeThenSwapRS2, true);

        //Test for swap twice, then build
        int[] tradeThenSwapRS3 = {0, 0, 1, 0, 0, 0};
        String tradeThenSwapBS3 = "R0,S3,R1,C7,R2,S4,R3,R4,R5,R6,J1,J2";
        testTask11("J3", tradeThenSwapBS3, tradeThenSwapRS3, true);

        //Test for swap twice, then trade, then build
        int[] tradeThenSwapRS4 = {0, 0, 0, 0, 0, 2};
        String tradeThenSwapBS4 = "R0,S3,R1,C7,R2,S4,R3,R4,R5,R6,J1,J2";
        testTask11("J3", tradeThenSwapBS4, tradeThenSwapRS4, true);

        //Test for swap twice using J6, then trade, then build
        int[] tradeThenSwapRS5 = {0, 0, 1, 0, 0, 0};
        String tradeThenSwapBS5 = "R0,S3,R1,C7,R2,S4,R3,R4,R5,R6,J1,J6";
        testTask11("J2", tradeThenSwapBS5, tradeThenSwapRS5, true);

        //Test for swap twice, then trade twice, then build
        //J1 and J2 give 1 ore and 1 grain, total rs is 2 ore 1 grain,
        //Then trade twice to get 3 ore and 2 grain to build a city
        int[] tradeThenSwapRS6 = {1, 0, 0, 0, 0, 4};
        String tradeThenSwapBS6 = "R0,S3,R1,C7,R2,S4,R3,R4,J1,J2";
        testTask11("C12", tradeThenSwapBS6, tradeThenSwapRS6, true);
        //If can only trade once
        int[] tradeThenSwapRS7 = {1, 0, 0, 0, 0, 3};
        String tradeThenSwapBS7 = "R0,S3,R1,C7,R2,S4,R3,R4,J1,J2";
        testTask11("C12", tradeThenSwapBS7, tradeThenSwapRS7, false);

        //Test for using J6 to get gold, then trade twice (actually doesn't matter because
        // you can trade J6 for anything, and trade once.)
        int[] tradeThenSwapRS8 = {0, 0, 0, 0, 0, 3};
        String tradeThenSwapBS8 = "R0,S3,R1,C7,R2,S4,R3,R4,J6";
        testTask11("R5", tradeThenSwapBS8, tradeThenSwapRS8, true);
    }

    private void testTask9(String action, String board_state, int[] resource_state, boolean expected) {
        String errorPrefix = "Test for Task9: canDoAction(\"" + action + " with BS: " + board_state +
                " and " + Arrays.toString(resource_state) + "\")";
        boolean out = CatanDice.canDoAction(action, board_state, resource_state);
        assertEquals(expected, out, errorPrefix);
    }

    @Test
    public void task9Tests() {
        int[] maxRS = {5, 5, 5, 5, 5, 5};
        testTask9("build R1", "R0", maxRS, true);
        testTask9("build R1", "R1", maxRS, false);
        testTask9("build R1", "", maxRS, false);
        testTask9("build C1", "", maxRS, false);

    }

    private void testGameStateScore(String board_state, int expected) {
        String errorPrefix = "Test for GSScore(\"" + board_state;
        GameState test = new GameState(board_state, emptyRS);
        int out = test.score();
        assertEquals(expected, out, errorPrefix);
    }

    @Test
    public void GameStateScoreTests() {
        testGameStateScore("R0,S3", 4);
        testGameStateScore("R0,R1,R2,R3", 4);
        testGameStateScore("R0,R1,S3,J1,J2,J3", 11);
        testGameStateScore("R0,S3,R1,C7,R2,S4,R3,R4,J6", 25);
        testGameStateScore("R0,S3,R1,C7,R2,S4,R3,R4,J6", 25);
        testGameStateScore("R0", 1);
    }


    public static void main(String[] args) {
        int[] maxRS = {5, 4, 4, 5, 5, 0};
        GameState gs = new GameState("J1,J2,J4,J5", maxRS);
        String targetStructure = "C12";


        int[] rs = {0, 0, 0, 0, 0, 0};
        System.out.println(CatanDice.canDoAction("build J2", "R0,J1", rs));
//
//        System.out.println(gs.allAllowedActions());

//        String bs = "R0,S3,R1,J1,J2";
//        int[] rs = {2,2,2,0,0};
//        String targetStructure = "J4";
//        System.out.println(Arrays.toString(CatanDice.pathTo(targetStructure,bs)));


    }


}
