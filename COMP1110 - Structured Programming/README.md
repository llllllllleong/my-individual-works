# COMP1110

| Course                             |    Coursework    | Mark | ANU Grade |
|------------------------------------|:----------------:|:----:|:---------:|
| COMP1110 - Structured Programming  | Group Assignment |  91  |    HD     |



![](../Images/CatanDicePreview.jpg)


## Hindsight Reflection
This was the second programming course I took (the first one being COMP1100 - Haskell) and
I completed it back in 2022. Although we scored a high mark, I won't bother highlighting and commentating on
my work in detail because, the code quality/standard is appalling. I recall now, that I actually worked on
this project before learning the fundamentals of OOP, hence the poor code standard.

The reason why we still scored high, is that the marking criteria was "Checkbox style" where marks are
awarded if the task is completed, rather than being based on the work quality.

I've also uploaded our video submission for D2G (one of the assessable component on the marking criteria), cut 
down to include only my portion of the video. My contribution to the video was a demo and commentary of the game.




### My contributions:
Class:
- BoardState.java
- GameState.java
- ResourceState.java
- U6858120Tests.java
- AIPlayer.java (Unfinished)

Methods:
- isActionWellFormed
- checkResources
- checkBuildConstraint
- canDoAction
- canDoSequence
- checkResourcesWithTradeAndSwap
- buildPlan
- Dice re-roll method

Others:
- Method for click building in Game.java
- res/images


## Features
Additional noteworthy features beyond those in the assignment
specification that we have added:
- Aesthetically pleasing and easy to use GUI
- Click to build feature
- Live score counter
- Turn counter
- Custom images for Jokers/Knights and Resources
- End turn button, to clear resources and increase the turn count
- Warning/Advice messages for when illegal actions are played
- Game ends after 15 turns
- Automatic Joker usage: prioritises use of jokers 1-5, before using joker 6
- Automatic button disabling. When a swap, trade, roll, or re-roll cannot be made, the corresponding button is disabled
- Legal Action text field: Lists possible actions, including dice actions, in the current state. Prioritises Build actions first, where structures are listed in order of their point value. Then trade actions, then swap actions. This is useful for new players who are unable to identify what to do next
- toString method for the Legal Actions, for increased legibility. e.g. "build J1" is parsed as "Build Ore Joker"
- Intuitive checkbox re-roll UI. Click the checkboxes below the resources you want to re-roll, and click re-roll


## Player instructions
The numerical ID of resources are:
- Ore = 0
- Grain = 1
- Wool = 2
- Timber = 3
- Brick = 4
- Gold = 5

Instructions:
1. Click roll to roll 6 dice. If you want to re-roll, click the checkbox below the resources you want to re-roll, and click re-roll.
2. To build, click on the structure you would like to build on the map. If there are building or resource constraints, an error message will advise that it is not possible to build.
3. The total score indicates the current score game.
4. The Current turn indicates the turn number.
5. To trade, input the name of the resource you would like to receive e.g "ore" without the quotation marks. If the player has 2 Gold, the trade will be successful.
   Otherwise, a "Cannot Trade" message will be shown.
6. To swap, input the name of the resource which you want to swap away e.g "ore" without the quotation marks,
   input the name of the resource you want to receive, and click Trade.
7. When you have not enough resources to do anything, or if you want to end your turn early, click End Turn



## Privacy
For all content shared, I do not claim credit or ownership as it was a group effort. I do not intend to form any link between the shared content, and the marks I was awarded. My reason for sharing is strictly limited to documentation of my learning process. Should any group members of this project wish to omit our work, do get in touch.