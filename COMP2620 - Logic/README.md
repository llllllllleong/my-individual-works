# COMP2620
| Course           |   Coursework   | Mark | ANU Grade |
|------------------|:--------------:|:----:|:---------:|
| COMP2620 - Logic |  Mid-semester  |  76  |     D     |
| COMP2620 - Logic |     Final      |  86  |    HD     |

I enrolled in this course assuming it would be an extension of COMP1600, which 
is one of my all-time favorite courses. I really enjoyed formulating proofs 
and representing formal languages in automatons and Turing machines.

However, this course turned out to be heavily philosophical and essay based. I 
struggled because my ability to write essays and present meaningful
philosophical arguments is very poor. I was not able to apply my strengths of 
statistical report writing and analytical/through thinking.

Note: Extension was granted for the final assessment


### Scratch code: Logic Checker
In Q3 of the take-home final exam, I was tasked with developing three sequents. 
With respect to a three-valued semantics for each type of logic, I had to create:

1. A sequent that is valid in relevance logic, but invalid in fuzzy logic.
2. A sequent that is valid in fuzzy logic, but invalid in intuitionist logic.
3. A sequent that is valid in intuitionist logic, but invalid in relevance logic.

There were several occasions where I had manually verified my sequents to be 
appropriate, only to later realise that I had made an error in my verification. 
Eventually I got fed up of the monotonous task of verification, and I decided 
to hard-code the three-valued semantics and sequents in java. Doing this 
allowed me to test all permutations of value assignment to ensure the sequent 
was appropriate.

I created the logic checker because I was in a loop of making mistakes and 
getting agitated (which likely led to further mistakes). In hindsight, a better 
solution would most likely be to have a good night's rest, and develop a deeper 
understanding of the question, before jumping to a solution.