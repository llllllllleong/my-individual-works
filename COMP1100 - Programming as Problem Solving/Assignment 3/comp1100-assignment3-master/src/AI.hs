{-|
Module      : AI
Description : AIs for Checkers
Copyright   : (c) 2020 Your Name Here
License     : AllRightsReserved
-}
module AI where

import          Checkers
import          Data.List
import          AITests 

data AIFunc
  = NoLookahead (GameState -> Move)
  | WithLookahead (GameState -> Int -> Move)

ais :: [(String, AIFunc)]
ais = [("firstLegalMove", NoLookahead firstLegalMove),
    ("greedyMove", NoLookahead greedyMove),
    ("greedyMove2", NoLookahead greedy2Move),
    ("minimax", WithLookahead minimax),
    ("minimax2", WithLookahead minimax2),
    ("minimax2h2", WithLookahead minimax2h2),
    ("newABP", WithLookahead newABP),
    ("finalABP", WithLookahead finalABP),
    ("default", WithLookahead finalABPRev)
    ]

data Tree a = Node a [Tree a]
    deriving Show

type GSM = (GameState,Move)

type GSMS = ([Move],Int)

type GSInt = (GameState, Int)

nullMove :: [Move]
nullMove = [Move (Location 0 0) (Location 0 0)]

nullNode :: Tree GSMS
nullNode = Node (nullMove,-100) []
-- To avoid a warning from importing AITests. Needed for testing with testboards
extraGS :: GameState
extraGS = testgs3

-- Final iteration of AI
finalABP :: GameState -> Int -> Move
finalABP gs lookahead = gsToMove gs newgs (allMoves gs)
    where
        newgs = finalABP2 (prune lookahead (treeABP gs turnn))
        turnn = turnCheck gs

finalABP2 :: Tree GSInt -> GameState
finalABP2 gametree = case (finalMax gametree alpha beta) of
    (Node (gs,_) _)     -> gs
    where
        alpha = (-10000)
        beta = (10000)

finalMax :: Tree GSInt -> Int -> Int -> Tree GSInt
finalMax a@(Node (gs,score) gss) alpha beta = case gss of
    []      -> a
    x:xs    -> case x of
        Node (gs2,score2) [] 
            | beta < score2     -> Node (gs2,score2) []
            | alpha < score2    -> finalMax (Node (gs2,score2) xs) score2 beta
            | otherwise         -> finalMax (Node (gs,score) xs) alpha beta
        Node (_,_) _        -> finalMax (Node (gs,score) (z:xs)) alpha beta
        where
            z = finalMin x alpha (10000)


finalMin :: Tree GSInt -> Int -> Int -> Tree GSInt
finalMin a@(Node (gs,score) gss) alpha beta = case gss of
    []      -> a
    x:xs    -> case x of
        Node (_,score2) [] 
            | score2 < alpha    -> Node (gs,score2) []
            | score2 < beta     -> finalMin (Node (gs,score2) xs) alpha score2
            | otherwise         -> finalMin (Node (gs,score) xs) alpha beta
        Node (_,_) _        -> finalMin (Node (gs,score) (z:xs)) alpha beta
        where
            z = finalMax x (-10000) beta


treeABP :: GameState -> Turn -> Tree GSInt
treeABP gs turnn = case moves of
    []      -> Node (gs,score) []
    _       -> Node (gs,score) [treeABP gss turnn  |  gss <- (map (applyMoves gs) moves)]
    where
        moves = allMoves gs
        score = (heuristicII gs turnn)


-- Version of finalABP which processes captures first
finalABPRev :: GameState -> Int -> Move
finalABPRev gs lookahead = gsToMove gs newgs (allMoves gs)
    where
        newgs = finalABP2 (prune lookahead (treeABPRev gs turnn))
        turnn = turnCheck gs

    
treeABPRev :: GameState -> Turn -> Tree GSInt
treeABPRev gs turnn = case moves of
    []      -> Node (gs,score) []
    _       -> Node (gs,score) [treeABPRev gss turnn  |  gss <- (map (applyMoves gs) moves)]
    where
        moves = reverse (lsort (allMoves gs))
        score = (heuristicII gs turnn)


comparing :: (Ord a) => (b -> a) -> b -> b -> Ordering
comparing p x y = compare (p x) (p y)


lsort :: [[a]] -> [[a]]
lsort = sortBy (comparing length)




-- Second attempt at ABP
newABP :: GameState -> Int -> Move 
newABP gs lookahead = gsToMove gs newgs (allMoves gs)
    where
        newgs = newABP2 (head gss) (-10000) gss (turnCheck gs) (lookahead)
        gss = (map (applyMoves gs) (allMoves gs))


newABP2 :: GameState -> Int -> [GameState] -> Turn -> Int -> GameState
newABP2 gs score gss turnn lookahead = case gss of
    []          -> gs
    x:xs        
        | score < nextgsscore   -> newABP2 x nextgsscore xs turnn lookahead
        | otherwise             -> newABP2 gs score xs turnn lookahead
        where
            nextgsscore = (abpMini nextgss turnn (-10000) (10000) (lookahead-1))
            nextgss = (map (applyMoves x) (allMoves x))
           

abpMax :: [GameState] -> Turn -> Int -> Int -> Int -> Int
abpMax gs turnn alpha beta lookahead = case lookahead of
    0   -> case gs of
        []      -> alpha
        x:xs   
            | beta < newalpha   -> newalpha
            | alpha < newalpha  -> abpMax xs turnn newalpha beta lookahead
            | otherwise         -> abpMax xs turnn alpha beta lookahead
                where
                    newalpha = (heuristicII x turnn)
    _   -> case gs of
        []      -> alpha
        x:xs
            | beta < minialpha  -> minialpha
            | alpha < minialpha -> abpMax xs turnn minialpha beta lookahead
            | otherwise         -> abpMax xs turnn alpha beta lookahead
                where
                    minialpha = (abpMini minigss turnn alpha beta (lookahead-1))
                    minigss = map (applyMoves x) (allMoves x)


abpMini :: [GameState] -> Turn -> Int -> Int -> Int -> Int
abpMini gs turnn alpha beta lookahead = case lookahead of
    0   -> case gs of
        []      -> beta
        x:xs   
            | newbeta < alpha   -> newbeta
            | newbeta < beta    -> abpMini xs turnn alpha newbeta lookahead
            | otherwise         -> abpMini xs turnn alpha beta lookahead
                where
                    newbeta = (heuristicII x turnn)
    _   -> case gs of
        []      -> beta
        x:xs
            | maxibeta < alpha  -> maxibeta
            | maxibeta < beta   -> abpMini xs turnn alpha maxibeta lookahead
            | otherwise         -> abpMini xs turnn alpha beta lookahead
                where
                    maxibeta = (abpMax maxgss turnn alpha beta (lookahead-1))
                    maxgss = map (applyMoves x) (allMoves x)


-- Compares a prior GS, and post GS and determines what moves are used
-- to get to the post GS
gsToMove :: GameState -> GameState -> [[Move]] -> Move
gsToMove oldgs newgs moves = case moves of
    x:xs
        | (applyMoves oldgs x) == (newgs)   -> head x
        | otherwise     -> (gsToMove oldgs newgs xs)
    []  -> error "gsToMove Empty list"




-- Second version of minimax, using heuristicII
minimax2h2 :: GameState -> Int -> Move
minimax2h2 gs lookahead = case (bestmove) of
    Node (moves,_) _    -> head moves
    where
        bestmove = maxiOf (gTreeh2 gs lookahead)


gTreeh2 :: GameState -> Int -> Tree GSMS
gTreeh2 gs lookahead = Node (nullMove,-1000) [gTree2h2 gs turnn (lookahead-1) moves | moves <- allMoves gs]
    where
        turnn = turnCheck gs


gTree2h2 :: GameState -> Turn -> Int -> [Move] -> Tree GSMS
gTree2h2 gs turnn lookahead currentmoves = case nextmoves of
    []      -> Node (currentmoves,score) []
    _       -> case lookahead of 
        0       -> Node (currentmoves,score) []
        _       -> Node (currentmoves,100) [gTree3h2 gss turnn (lookahead-1) moves 1 | moves <- allMoves gss]
    where
        gss = applyMoves gs currentmoves
        score = heuristicII gss turnn
        nextmoves = allMoves gss
        

gTree3h2 :: GameState -> Turn -> Int -> [Move] -> Int -> Tree GSMS
gTree3h2 gs turnn lookahead currentmoves status = case nextmoves of
    []      -> Node (currentmoves,score) []
    _       -> case lookahead of 
        0       -> Node ([],score) []
        _ 
            | status == 1
                -> Node ([],-100) [gTree3h2 gss turnn (lookahead-1) moves 0 | moves <- allMoves gss]
            | otherwise
                -> Node ([],100) [gTree3h2 gss turnn (lookahead-1) moves 1 | moves <- allMoves gss]
    where
        gss = applyMoves gs currentmoves
        score = heuristicII gss turnn
        nextmoves = allMoves gss




-- Second attempt at minimax
minimax2 :: GameState -> Int -> Move
minimax2 gs lookahead = case (bestmove) of
    Node (moves,_) _    -> head moves
    where
        bestmove = maxiOf (gTree gs lookahead)


-- Includes pruning
gTree :: GameState -> Int -> Tree GSMS
gTree gs lookahead = Node (nullMove,-100) [gTree2 gs turnn (lookahead-1) moves | moves <- allMoves gs]
    where
        turnn = turnCheck gs


--Creates the nodes for the first move. Moves are only stored for the first move
gTree2 :: GameState -> Turn -> Int -> [Move] -> Tree GSMS
gTree2 gs turnn lookahead currentmoves = case nextmoves of
    []      -> Node (currentmoves,score) []
    _       -> case lookahead of 
        0       -> Node (currentmoves,score) []
        _       -> Node (currentmoves,100) [gTree3 gss turnn (lookahead-1) moves 1 | moves <- allMoves gss]
    where
        gss = applyMoves gs currentmoves
        score = boardScore gss turnn
        nextmoves = allMoves gss

        
-- Creates the rest of the tree. Will calculate board scores only for the last nodes
gTree3 :: GameState -> Turn -> Int -> [Move] -> Int -> Tree GSMS
gTree3 gs turnn lookahead currentmoves status = case nextmoves of
    []      -> Node (currentmoves,score) []
    _       -> case lookahead of 
        0       -> Node ([],score) []
        _ 
            | status == 1
                -> Node ([],-100) [gTree3 gss turnn (lookahead-1) moves 0 | moves <- allMoves gss]
            | otherwise
                -> Node ([],100) [gTree3 gss turnn (lookahead-1) moves 1 | moves <- allMoves gss]
    where
        gss = applyMoves gs currentmoves
        score = boardScore gss turnn
        nextmoves = allMoves gss


-- Will return the move of the highest scoring node
-- One by one evaluation, does not actually find the maximum, just if the next value is higher than 
-- the current value
maxiOf :: Tree GSMS -> Tree GSMS
maxiOf a@(Node (bestmove,bestscore) b) = case b of
    []      -> a
    x:xs    -> case x of
        Node (move,score) []
            | score > bestscore     -> maxiOf (Node (move,score) xs)
            | otherwise             -> maxiOf (Node (bestmove,bestscore) xs)
        Node (move,score) c         
            | score < bestscore     -> maxiOf (Node (bestmove,bestscore) xs)
            | otherwise             
                -> maxiOf (Node (bestmove,bestscore) ((miniOf (Node (move,score) c)):xs))


-- Does not return moves, only returns score
miniOf :: Tree GSMS -> Tree GSMS
miniOf a@(Node (move,score) b) = case b of
    []      -> a
    x:xs    -> case x of
        Node (_,newscore) []
            | newscore < score      -> (Node (move,newscore) xs)
            | otherwise             -> (Node (move,score) xs)
        Node (newmove,newscore) c   
            -> miniOf (Node (move,score) ((maxiOf (Node (newmove,newscore) c)):xs))




-- Returns a list of all possible single, and consecutive moves. Must use 
--'applyMoves' to apply consecutive moves
allMoves :: GameState -> [[Move]]
allMoves gs = concatMap treeToMove (allMoves1 gs)

allMoves1 :: GameState -> [Tree Move]
allMoves1 gs = [allMoves2 (turn gs) (gs,moves)  | moves <- legalMoves gs]

allMoves2 :: Turn -> GSM -> Tree Move
allMoves2 player (gs,move) 
    | turn gss == player    = Node move [allMoves2 player (gss,moves) | moves <- legalMoves gss]
    | otherwise             = Node move []
        where
            gss = applyMoves2 gs move

treeToMove :: Tree Move -> [[Move]]
treeToMove (Node move moves) = case moves of
    []      -> [[move]]
    _    -> [[move] ++ nextmove | nextmove <- concatMap treeToMove moves]


-- Used to return the Turn of a GameState
turnCheck :: GameState -> Turn
turnCheck gs = turn gs 


-- A version of 'applyMove' which is able to process consecutive moves
applyMoves :: GameState -> [Move] -> GameState
applyMoves gs moves = case moves of
    []      -> error "applyMoves: No moves!"
    x:[]    -> applyMoves2 gs x
    x:xs    -> applyMoves (applyMoves2 gs x) xs

applyMoves2 :: GameState -> Move -> GameState
applyMoves2 gs move = case (applyMove move gs) of
    Just a  -> a
    _       -> error "applyMoves2 error"


-- Prune function to prune recursive tree generating functions at a given depth
prune :: Int -> Tree a -> Tree a
prune 0 (Node x _) = Node x []
prune n (Node x ts) = Node x [prune (n - 1) t | t <- ts]


-- First attempt at minimax
gameTree1 :: GameState -> Turn -> Tree GSMS
gameTree1 gs turnn = Node (nullMove,-100) [gameTree2 gs turnn moves | moves <- allMoves gs]

gameTree2 :: GameState -> Turn -> [Move] -> Tree GSMS
gameTree2 gs turnn currentmoves = Node (currentmoves,score) [gameTree2 gss turnn moves | moves <- allMoves gss]
    where
        gss = applyMoves gs currentmoves
        score = boardScore gss turnn

minimax :: GameState -> Int -> Move
minimax gs lookahead = case (bestmove) of
    Node (moves,_) _    -> head moves
    where
        bestmove = maximumOf (prune lookahead (gameTree1 (gs) (turnCheck gs))) 

maximumOf :: Tree GSMS -> Tree GSMS
maximumOf a@(Node (bestmove,bestscore) b) = case b of
    []      -> a
    x:xs    -> case x of
        Node (move,score) []
            | score > bestscore     -> maximumOf (Node (move,score) xs)
            | otherwise             -> maximumOf (Node (bestmove,bestscore) xs)
        Node (move,_) c         -> maximumOf (Node (bestmove,bestscore) ((minimumOf (Node (move,100) c)):xs))                               
        
minimumOf :: Tree GSMS -> Tree GSMS
minimumOf a@(Node (move,score) b) = case b of
    []      -> a
    x:xs    -> case x of
        Node (_,newscore) []
            | newscore < score      -> minimumOf (Node (move,newscore) xs)
            | otherwise             -> minimumOf (Node (move,score) xs)
        Node (newmove,newscore) c   -> minimumOf (Node (move,score) ((maximumOf (Node (newmove,newscore) c)):xs))


-- Greedy Strategy AI 2
-- Chooses the move with the highest score
greedy2Move :: GameState -> Move
greedy2Move gs = greedy2Move2 gs (allMoves gs) nullMove (-100)

greedy2Move2 :: GameState -> [[Move]] -> [Move] -> Int -> Move
greedy2Move2 gs moves bestmove score = case moves of
    []      -> head bestmove
    x:xs    
        | newscore >= score
                    -> greedy2Move2 gs xs x newscore
        | otherwise -> greedy2Move2 gs xs bestmove score
            where 
                newscore = boardScore (applyMoves gs (head moves)) (turnCheck gs)
 

-- Greedy Strategy AI
-- Chooses the first move which captures
greedyMove :: GameState -> Move
greedyMove gs = greedyMove2 (legalMoves gs)

greedyMove2 :: [Move] -> Move
greedyMove2 moves = case moves of
    x:[]    -> x
    x:xs    
        | captureTest x     -> x
        | otherwise         -> greedyMove2 xs
    [] -> error "greedyMove2 empty list"

captureTest :: Move -> Bool
captureTest (Move (Location x1 y1) (Location x2 y2))
    | (abs (x1 - x2) == 2) && (abs (y1 - y2) == 2) = True
    | otherwise = False


-- | A very simple AI, which picks the first move returned by the
-- 'legalMoves' function. AIs can rely on the 'legalMoves' list being
-- non-empty; if there were no legal moves, the framework would have
-- ended the game.
firstLegalMove :: GameState -> Move
firstLegalMove st = head (legalMoves st)












-- Heuristics
heuristicIII :: GameState -> Turn -> Int
heuristicIII gs turnn = case turnn of
    (Turn Player1)  -> case (turnCheck gs) of
        GameOver (Winner Player1)   -> (-1000000)
        GameOver _                  -> (1000000)
        _                           -> a
    (Turn Player2)  -> case (turnCheck gs) of
        GameOver (Winner Player2)   -> (-1000000)
        GameOver _                  -> (1000000)
        _                           -> (negate a)
    _   -> error "heuristicIII: turn input gameover"
    where
        a = sum (map mySide (takeEnd 32 (concat (board gs)))) + sum (map oppSide (take 32 (concat (board gs))))

heuristicII :: GameState -> Turn -> Int
heuristicII gs turnn = case turnn of
    (Turn Player1)    -> a
    (Turn Player2)    -> (negate a)
    _               -> error "heuristicII: GameOver"
    where
        a = sum (map mySide (takeEnd 32 (concat (board gs)))) + sum (map oppSide (take 32 (concat (board gs))))

oppSide :: Square -> Int
oppSide sq = case sq of
    Piece Player1 King  -> 10
    Piece Player1 _     -> 7
    Piece Player2 King  -> (-10)
    Piece Player2 _     -> (-5)
    _                   -> 0

mySide :: Square -> Int
mySide sq = case sq of
    Piece Player1 King  -> 10
    Piece Player1 _     -> 5
    Piece Player2 King  -> (-10)
    Piece Player2 _     -> (-7)
    _                   -> 0





takeEnd :: Int -> [a] -> [a]
takeEnd n xs = foldl' (const . drop 1) xs (drop n xs)

boardScore :: GameState -> Turn -> Int
boardScore gs turnn = case turnn of 
    Turn Player1    
--        | turnCheck gs == (Turn GameOver Winner Player1)   -> 1000
--        | turnCheck gs == (Turn GameOver Winner Player2)   -> -1000
        | otherwise -> boardCount (boardCount2 gs) (Player1)
    Turn Player2
--        | turnCheck gs == GameOver Winner Player2   -> 1000
--        | turnCheck gs == GameOver Winner Player1   -> -1000
        | otherwise -> boardCount (boardCount2 gs) (Player2)
    _   -> error "boardScore: GameOver"

boardCount :: (Int,Int) -> Player -> Int
boardCount (a,b) player = case player of
    Player1     -> a-b 
    _           -> b-a

boardCount2 :: GameState -> (Int, Int)
boardCount2 st = foldl' count2 (0, 0) (concat (board st))
  where
    count2 :: (Int, Int) -> Square -> (Int, Int)
    count2 (p1,p2) sq = case sq of
      Piece Player1 King -> (p1 + 3, p2)
      Piece Player1 _ -> (p1 + 1, p2)
      Piece Player2 King -> (p1, p2 + 3)
      Piece Player2 _ -> (p1, p2 + 1)
      _               -> (p1, p2)
