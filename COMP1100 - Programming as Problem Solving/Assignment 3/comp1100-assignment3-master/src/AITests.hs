{-|
Module      : AITests
Description : Tests for your AI functions
Copyright   : (c) 2020 Your Name Here
License     : AllRightsReserved
-}
module AITests where

--import           AI
--import           Checkers
import           Testing
import           Checkers


-- Lack of tests explained in report
aiTests :: Test
aiTests = TestGroup "AI"
  [
  ]


{-
Even this test would involve firstly manually figuring out the coordinates of the move.
By then, the expected outcome is already known and creating a test would be pointless.
Tests functions are not usefull because they are too specific.

movesTest :: Test
movesTest = TestGroup "Consecutive move, with multiple future captures"
  [ Test "Two lists"
      (assertEqual (allMoves testgs11)
        ([[Move (Location 4 7) (Location 2 5),Move (Location 2 5) (Location 0 3)],[Move (Location 4 7) (Location 2 5),Move (Location 2 5) (Location 4 3)]]))
  ]
-}
testBoard1 :: Board
testBoard1 = toBoard1
  [ "#x#x#x# "
  , " #o#o# #"
  , "#o# #o# "
  , " #X#o#o#"
  , "#o# # # "
  ]

testBoard2 :: Board
testBoard2 = toBoard1
  [ "# #x#x# "
  , " # #o# #"
  , "#o#x#o# "
  , " #X#o#o#"
  , "#o# # # "
  ]
testBoard3 :: Board
testBoard3 = toBoard1
  [ "# #x#x# "
  , " # #o# #"
  , "#o# #o# "
  , " #X# #o#"
  , "#o# #X# "
  ]

testBoard4 :: Board
testBoard4 = toBoard1
  [ "# #x#x# "
  , " # #o# #"
  , "#o# #o#X"
  , " #X# # #"
  , "#o# # # "
  ]

testBoard5 :: Board
testBoard5 = toBoard1
  [ "#x#x#x# "
  , " #o#o# #"
  , "# # # # "
  , " #o#o#o#"
  , "# #X# # "
  ]

testBoard6 :: Board
testBoard6 = toBoard1
  [ "#x#x#x#x"
  , " #x#x#x#"
  , "# #x#x#x"
  , "x# # # #"
  , "# # # # "
  , "o#o#o#o#"
  , "#o#o#o#o"
  , "o#o#o#o#"
  ]

-- (State (Turn Player1) None (8,8) testBoard7 (History 0 []))
testBoard7 :: Board
testBoard7 = toBoard1
  [ "#X#O#x#o"
  , " # # # #"
  , "# # # # "
  , "o#x#x#o#"
  , "# # # # "
  , "x#x#o# #"
  , "# # # # "
  , "X#X#O#O#"
  ]

testBoard8 :: Board
testBoard8 = toBoard1
  [ "# # # # "
  , " # # # #"
  , "# # # # "
  , "O# # # #"
  , "#X# # # "
  , " # # # #"
  , "# #x# # "
  , " #X#X# #"
  ]

testBoard9 :: Board
testBoard9 = toBoard1
  [ "# # # # "
  , " # # # #"
  , "# # # # "
  , "O# # # #"
  , "# # # # "
  , " # # # #"
  , "# #x# # "
  , " # # # #"
  ]

testBoard10 :: Board
testBoard10 = toBoard1
  [ "# # # # "
  , " # # # #"
  , "# #x# # "
  , " #x#x# #"
  , "# # # # "
  , "o# #x#o#"
  , "#o# # #o"
  , " # # # #"
  ]

-- Testing multiple possible consecutive captures
-- Should return two possible moves
testBoard11 :: Board
testBoard11 = toBoard1
  [ "#x# # # "
  , " # # # #"
  , "# # # # "
  , " # # # #"
  , "#x#x# # "
  , " # # # #"
  , "# #x# # "
  , " # #o# #"
  ]

testBoardGameOver :: Board
testBoardGameOver = toBoard1
  [ "# # # # "
  , " # # # #"
  , "#x# # # "
  , "o# # # #"
  , "# #x# # "
  , " #o# # #"
  , "# # #x# "
  , " # #o#o#"
  ]

testBoardGameOver2 :: Board
testBoardGameOver2 = toBoard1
  [ "# # # # "
  , " # # # #"
  , "# # # # "
  , " # # # #"
  , "# # #x# "
  , " # # # #"
  , "# #x# # "
  , " #o#o# #"
  ]

-- Trap boards
-- Player 2 turn
-- Player 2 should win
trap1 :: Board
trap1 = toBoard1
  [ "# #x# #x"
  , "x#x#x#x#"
  , "# #x# #x"
  , "o# #x#x#"
  , "# #x#o# "
  , "o#o#o#o#"
  , "#o# #o#o"
  , " #o# #o#"
  ]

-- P1 turn
-- Player 1 should win
trap2 :: Board
trap2 = toBoard1
  [ "#x# #x# "
  , "x# #x# #"
  , "# #x#x#x"
  , "x#x#x# #"
  , "# #x#o#o"
  , "o#o#o#o#"
  , "#o# #o# "
  , "o#o# #o#"
  ]

-- P2 turn
-- Player 2 should win
trap3 :: Board
trap3 = toBoard1
  [ "#x#x# # "
  , "x#x# #x#"
  , "#x# #x#x"
  , " #x#o#x#"
  , "#o#o# #x"
  , "o# #o# #"
  , "#o#o# #o"
  , " #o#o#o#"
  ]


toBoard1 :: [String] -> Board
toBoard1 = (map . map) parsePiece
  where
   parsePiece c = case c of
    ' ' -> Empty
    'x' -> Piece Player2 Pawn
    'X' -> Piece Player2 King
    'o' -> Piece Player1 Pawn
    'O' -> Piece Player1 King
    _   -> Blank




testgs :: GameState
testgs = State (Turn Player2) None (8,5) testBoard5 (History 0 [])

testgs2 :: GameState
testgs2 = State (Turn Player2) None (8,8) testBoard7 (History 0 [])

testgs3 :: GameState
testgs3 = State (Turn Player1) None (8,8) testBoard6 (History 0 [])
--Lots of moves
testgs6 :: GameState
testgs6 = State (Turn Player1) None (8,8) testBoard6 (History 0 [])

testgs8 :: GameState
testgs8 = State (Turn Player1) None (8,8) testBoard8 (History 0 [])

testgs9 :: GameState
testgs9 = State (Turn Player1) None (8,8) testBoard9 (History 0 [])

testgs10 :: GameState
testgs10 = State (Turn Player1) None (8,8) testBoard10 (History 0 [])

testgs11 :: GameState
testgs11 = State (Turn Player1) None (8,8) testBoard11 (History 0 [])

testgsgameover :: GameState
testgsgameover = State (Turn Player1) None (8,8) testBoardGameOver (History 0 [])

testgsgameover2 :: GameState
testgsgameover2 = State (Turn Player1) None (8,8) testBoardGameOver2 (History 0 [])

testgstrap1 :: GameState
testgstrap1 = State (Turn Player2) None (8,8) trap1 (History 0 [])

testgstrap2 :: GameState
testgstrap2 = State (Turn Player1) None (8,8) trap3 (History 0 [])

testgstrap3 :: GameState
testgstrap3 = State (Turn Player2) None (8,8) trap3 (History 0 [])


