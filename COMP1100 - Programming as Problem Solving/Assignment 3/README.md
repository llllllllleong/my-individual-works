# COMP1100 Assignment 3

| Course                                           | Coursework | Mark |
|--------------------------------------------------|:----------:|:----:|
| COMP 1100 - Programming as Problem Solving       |    3/3     |  77  |






> Dear Leong,
>
> Below is your feedback for assignment 3.
>
> If you have any concerns about the mark or the feedback,  
> please reply to this email ASAP.
>
> Hope you have a good break, and see you in 2021!
>
> Thanks,
>
> =============
>
> ## [55 marks] Checkers AI
>
> ========================
>
> ## Checkers AI
> ### Basic Code <27> of 27
> ### Handling the whole turn <2> of 2
> ### Lookahead working <3> of 3
> ### Minimax <7> of 7
> ### Alpha-Beta <8> of 8
> ### Advanced Optimisations <5> of 8
>
> Some effort put into constructing a heuristic,  
> but there's no explanation as to how it was chosen, or if it even performs well.  
> Exploring capturing moves first implemented.
>
> ========================
>
> Unit tests {0} of 10
> Unit tests can still be useful to test various parts  
> of your code, you should still use them.  
> No unit tests present.
>
> ========================
>
> ## Style {6} of 10
> -1 don't leave dead code in  
> -1 avoid magic numbers in the heuristic. Put a comment explaining where they came from.  
> -1 avoid lines longer than 80 chars  
> -1 [move] ++ nextmove  
> this is inefficient, use move : nextmove
>
> ========================
>
> ## Reports [25 marks]
> For a high mark, we expect the report to:
> * demonstrate conceptual understanding of all relevant functions and depict a holistic view of program structure;
> * contain a thorough explanation of the design process, including relevant reasoning and assumptions;
> * show evidence of testing of all relevant parts of the program and a discussion on why these results imply correctness; and
> * be easily readable, with good formatting.
>
> Documentation (what)  {4} of 7
> Some functions are not adequately explained.  
> How does treeABPRev work?  
> How does your pruning work?
>
> Reflection (why)      {4} of 7
> Is there much justification for the heuristic chosen?  
> How well does it perform?
>
> Testing (how)         {7} of 7
> Good summary of testing.
>
> Style                 {4} of 4
>
> =========================
>
> Total: 77.0 of 100