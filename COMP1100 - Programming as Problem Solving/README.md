# COMP1100

| Course                                           | Coursework | Mark |
|--------------------------------------------------|:----------:|:----:|
| COMP 1100 - Programming as Problem Solving       |    1/3     |  92  |
| COMP 1100 - Programming as Problem Solving       |    2/3     |  92  |
| COMP 1100 - Programming as Problem Solving       |    3/3     |  77  |
| COMP 1100 - Programming as Problem Solving       | Final Exam |  87  |


My first COMP course, and my all-time favourite course (at the time). I enjoyed it so much, 
that it led to the decision to choose COMP electives.

The course was taught in Haskell, and at the time of writing, I completed this 
course 4 years ago. 

