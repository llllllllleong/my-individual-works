# COMP1100 Assignment 2

| Course                                           | Coursework | Mark |
|--------------------------------------------------|:----------:|:----:|
| COMP 1100 - Programming as Problem Solving       |    2/3     |  92  |


![](../../Images/Wireworld.GIF)

<br>

![](../../Images/XOR.GIF)

XOR gate modelled in Wireworld

<br>
<br>

![](../../Images/Diode.GIF)

Diode modelled in Wireworld

<br>
<br>

![](../../Images/GameOfLife.GIF)

<br>
<br>

![](../../Images/GosperGliderGun.GIF)

Gosper's glider gun

<br>
<br>

![](../../Images/Oscillators.GIF)

Oscillators

<br>
<br>


> Dear Leong,
> 
> Attached is your feedback for Assignment 2 for COMP1100.
> If you have any questions or concerns about the mark or
> the feedback, please reply via this email.
> Thanks,
> 
> =============
>
> ## [20 marks] Task 1: Types and Helper Functions
> ### Datatype {2} of 2
> ### Cycling through Wireworld type {2} of 2
> ### Reading Test Patterns {2} of 2
> ### Rendering to CodeWorld Pictures {1} of 1
> ### `get` function {7} of 7
> ### `allCoords` function {6} of 6
>
> ========================
>
> ## [30 marks] Task 2: Implementing Wireworld
> #### The rule itself {5} of 5
> #### Counting Alive Neighbours {3} of 3
> #### Finding a Cell's Neighbourhood {8} of 8
> ### `nextGenWireworld` Itself {8} of 8
> ### Evolving the Grid n Steps {6} of 6
>
> ========================
>
> ## [10 marks] Task 3: Another Automaton
> ### Automata explanation {2} of 2
> ### Data type {1} of 1
> ### The rule {1} of 1
> ### `nextGen` {2} of 2
> ### `evolve` {1} of 1
> ### `src/App.hs` modifications {2} of 2
> ### appropriate test patterns {1} of 1
>
> ========================
>
> ## Unit Tests {7} of 10
> Not all non-trivial functions tested.  
> Why did you choose these tests?
>
> ========================
>
> ## Style {10} of 10
> Code is clean and well documented.
>
> ========================
>
> ## Reports [20 marks]
> For a high mark, we expect the report to:
> * demonstrate conceptual understanding of all relevant functions and depict a holistic view of program structure;
> * contain a thorough explanation of the design process, including relevant reasoning and assumptions;
> * show evidence of testing of all relevant parts of the program and a discussion on why these results imply correctness; and
> * be easily readable, with good formatting.
>
> Documentation (what)  {2} of 5
> No need to document features I already know how to use.  
> Not all non-trivial functions have been described in full detail.  
> It's not clear how many of your functions work by reading the report.
>
> Reflection (why)      {4} of 6
> Good discussion of assumptions.  
> Summary of future designs is good.  
> Would have liked to see more about the design process.
>
> Testing (how)         {6} of 6
> Excellent summary of tests that were performed, what you learned from
> those tests, and how you subsequently fixed your code.
>
> Style                 {3} of 3
> Clean, no issues.
>
> =========================
>
> Total: 92.0 of 100