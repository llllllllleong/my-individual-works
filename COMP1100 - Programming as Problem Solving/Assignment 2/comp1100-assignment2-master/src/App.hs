{-|
Module      : COMP1100 Assignment 2 - App
Description : Contains functions to run the applications
Maintainer  : u6858120@anu.edu.au

-}
{-# LANGUAGE OverloadedStrings #-}

module App where

import           Automata
import           CodeWorld
import           Data.List    (splitAt)
import           Data.Text    (pack)
import           GridRenderer
import           TestPatterns

appMain :: IO ()
appMain = activityOf (Model 0 1 (Wireworld wireworldInitial)) handleEvent render


-- | The first 'Int' is the number of generations evolved. The second
-- is how far to jump when pressing space.
data Model = Model Int Int CellGrid


-- | The model at the start of a simulation.
initial :: CellGrid -> Model
initial = Model 0 1


-- | CellGrid - Used to identify the type of model currently implemented
data CellGrid
  = Wireworld (Grid Wireworld)
  | Life (Grid Life)

-- | TestPattern - For identifying and selecting predefined patterns
data TestPattern = One 
    | Two
    | Three
    | Four

-- | AppEvent - Contains all possible events
data AppEvent
  = CycleCell GridCoord
    -- ^ Change a cell in the grid, in a way that makes sense for the
    -- current automaton
  | LoadTestPattern TestPattern
    -- ^ Replace the grid with one of the test patterns.
  | ToWireworld
    -- ^ Switch to Wireworld.
  | ToLife
    -- ^ Switch to Game of Life
  | Canvas
    -- ^ Gives the user a clear canvas
  | Step
    -- Run the automaton one step.
  | Jump
    -- Jump forward a few steps.
  | IncreaseJumpSize
  | DecreaseJumpSize


handleEvent :: Event -> Model -> Model
handleEvent ev m = case parseEvent m ev of
  Nothing    -> m
  Just appEv -> applyEvent appEv m

-- | CodeWorld has many events and we are not interested in most of
-- them. Parse them to an app-specific event type.
--
-- Further reading, if interested: "Parse, don't validate"
-- https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/
parseEvent :: Model -> Event -> Maybe AppEvent
parseEvent (Model _ _ grid) ev = case ev of
  KeyPress k
    | k == "1" -> Just (LoadTestPattern One)
    | k == "2" -> Just (LoadTestPattern Two)
    | k == "3" -> Just (LoadTestPattern Three)
    | k == "4" -> Just (LoadTestPattern Four)
    | k == "W" -> Just ToWireworld
    | k == "T" -> Just ToLife
    | k == "Esc" -> Just Canvas
    | k == "." -> Just Step
    | k == " " -> Just Jump
    | k == "=" -> Just IncreaseJumpSize
    | k == "-" -> Just DecreaseJumpSize
    | otherwise -> Nothing
  PointerPress p -> case getGridCoord p of
    Nothing    -> Nothing
    Just coord -> Just (CycleCell coord)
  _ -> Nothing

  where
    getGridCoord p = case grid of
      Wireworld g -> fromPoint g p
      Life g -> fromPoint g p

applyEvent :: AppEvent -> Model -> Model
applyEvent ev (Model n steps grid) = case ev of
  ToWireworld -> initial (Wireworld wireworldInitial)
  ToLife      -> initial (Life lifeInitial)
  Canvas      -> initial (grid')
    where
      grid' = case grid of
          Wireworld _ -> (Wireworld wireworldCanvas)
          Life _      -> (Life lifeCanvas)
  CycleCell p -> Model n steps grid'
    where
      grid' = case grid of
        Wireworld cells -> Wireworld (at p cycleWireworld cells)
        Life cells -> Life (at p cycleLife cells)
  LoadTestPattern pat -> initial grid'
    where
      grid' = case pat of
        One -> Wireworld diodes
        Two -> Wireworld xor
        Three -> Life oscillators
        Four -> Life gosperGliderGun
  Step -> Model (n + 1) steps grid'
    where
      grid' = case grid of
        Wireworld g -> Wireworld (nextGenWireworld g)
        Life g -> Life (nextLife g)
  Jump -> Model (n + steps) steps grid'
    where
      grid' = case grid of
        Wireworld g -> Wireworld (evolveWireworld steps g)
        Life g -> Life (evolveLife steps g)

  IncreaseJumpSize -> Model n (steps + 1) grid
  DecreaseJumpSize -> Model n (max 1 (steps - 1)) grid


render :: Model -> Picture
render (Model n steps grid)
  = translated (-15) 9 (lettering (pack ("Generation: " ++ show n)))
  & translated (-10) 8 (lettering (pack ("Step size: " ++ show steps)))
  & case grid of
       Wireworld g -> renderGrid renderWireworld g
       Life g -> renderGrid renderLife g

-- | Apply a function to a certain cell inside a grid, and return a
-- new grid where that cell has been replaced with the result of the
-- function call.
at :: GridCoord -> (c -> c) -> Grid c -> Grid c
at p@(row, col) f g@(Grid w h cells) = case get g p of
  Nothing -> g
  Just c -> Grid w h cells' where
    cells' = beforeCells ++ f c:afterCells
    (beforeCells, _:afterCells) = splitAt (w * row + col) cells

-- | Replace a cell within a grid.
setAt :: GridCoord -> c -> Grid c -> Grid c
setAt p c = at p (const c)
