{-|
Module      : COMP1100 Assignment 2 - Automata
Description : Contains functions used to create the next generation model
Maintainer  : u6858120@anu.edu.au

-}
module Automata where

import           CodeWorld

-- | A 'Grid' of some cell type c consists of a pair of positive Ints
-- describing its width (columns) and height (rows), respectively,
-- along with a list of cs that is exactly (width * height) long.
data Grid c = Grid Int Int [c]
    deriving (Eq, Show)

-- | Type alias for grid coordinates. This makes it clear when we are
-- talking about grid coordinates specifically, as opposed to some
-- other pair of integers.
type GridCoord = (Int, Int)

-- | Type of cells used in the Wireworld automaton
data Wireworld = Empty
    | Wire
    | EHead
    | ETail
    deriving (Eq, Show)

-- | cycleWireworld - Changes a cell's Wireworld in a specific order
-- Used when clicking on cells, to change the cell's type
cycleWireworld :: Wireworld -> Wireworld
cycleWireworld a = case a of
    Empty    -> Wire
    Wire     -> EHead
    EHead    -> ETail
    ETail    -> Empty

-- | renderWireworld - Input a Wireworld, and outputs a respective picture for the cell
-- Used for deciding what shape and colour should be drawn for every Wireworld
renderWireworld :: Wireworld -> Picture
renderWireworld a = case a of
    Empty    -> coloured black (rectangle 1.0 1.0)
    Wire     -> coloured orange (solidRectangle 1.0 1.0)
    EHead    -> coloured green (solidRectangle 1.0 1.0)
    ETail    -> coloured grey (solidRectangle 1.0 1.0)




-- WIREWORLD GENERATION
-- | evolveWireworld - Recursive function for multiple generations to be performed
-- Input a Int and GridWireworld, and outputs the (Int)th next generation
evolveWireworld :: Int -> Grid Wireworld -> Grid Wireworld
evolveWireworld a b 
    | a <= 0    = b
    | otherwise = evolveWireworld (a-1) (nextGenWireworld b)

-- | nextGenWireworld - Creates the next Wireworld generation
-- Gathers more data and passes it along to wireCheck
nextGenWireworld :: Grid Wireworld -> Grid Wireworld
nextGenWireworld a@(Grid width height _) 
    = Grid width height (map (wireCheck a) b)
    where
        b = allCoords width height

-- | wireCheck - Decides what the cell's next gen should be
wireCheck :: Grid Wireworld -> GridCoord -> Wireworld
wireCheck a b@(row,col) = case (get a b) of
    Just Empty  -> Empty
    Just EHead  -> ETail
    Just ETail  -> Wire
    Just Wire   -> eHeadRule ((sum . map eHeadCount) neighbours)
        where
            neighbours = map (get a) [
                (row-1,col-1), (row-1, col), (row-1, col+1),
                (row,col-1), (row, col+1),
                (row+1,col-1), (row+1, col), (row+1, col+1)]
    Nothing     -> error "wireCheck has recieved Nothing as an input"




-- RULES FOR WIREWORLD
-- | eHeadRule - Conditional outcome of Wire, based on neighbouring EHeads
eHeadRule :: Integer -> Wireworld
eHeadRule a
    | a > 0  &&  a < 3      = EHead
    | otherwise             = Wire

-- | eHeadCount - Helper function used in wireCheck
-- Used to count the number of EHead in a list of neighbours
eHeadCount :: Maybe Wireworld -> Integer
eHeadCount a = case a of
    Just EHead  -> 1
    _           -> 0
    



-- HELPER FUNCTIONS FOR GENERATION
-- | get - Input Grid C and GridCoord, and Maybe returns the cell's Wireworld
-- Maybe is used to avoid problems with requests for cells outside the Grid
get :: Grid c -> GridCoord -> Maybe c
get (Grid width height a) (row,col) 
    | row > (height-1)   = Nothing
    | col > (width-1)    = Nothing
    | row < 0            = Nothing
    | col < 0            = Nothing
    | otherwise = Just (a !! b)
        where
            b = (row * width) + col

-- | allCoords - Return row-major coordinates for a grid of width w and height h
-- Example:
-- >>> allCoords 3 2
--
allCoords :: Int -> Int -> [GridCoord]
allCoords width height = [(row,col) | row <- [0..(height-1)], col <- [0..(width-1)]]









-- EXTENSION: GAME OF LIFE
-- | Types of cells used in Game of Life
data Life = Alive
    | Dead
    deriving (Eq, Show)

-- | cycleLife - Changes a cell's type. Used when clicking on cells
cycleLife :: Life -> Life
cycleLife a = case a of
    Alive -> Dead
    Dead -> Alive

-- | renderWireworld - Input a Wireworld, and outputs a respective picture for the cell
-- Used for deciding what shape and colour should be drawn for every Wireworld
renderLife :: Life -> Picture
renderLife a = case a of
    Dead    -> coloured grey (solidRectangle 1.0 1.0)
    Alive   -> coloured white (solidRectangle 1.0 1.0)




-- GAME OF LIFE GENERATION
-- | evolveLife - Recursive function to jump generations
evolveLife :: Int -> Grid Life -> Grid Life
evolveLife a b 
    | a < 0     = error "Cannot compute negative generations"
    | a == 0    = b
    | otherwise = evolveLife (a-1) (nextLife b)

-- | nextLife - Creates the next generation of Life
nextLife :: Grid Life -> Grid Life
nextLife a@(Grid width height _) 
    = Grid width height (map (lifeCheck a) b)
    where
        b = allCoords width height

-- | lifeCheck - Decides a cell's next generation type
lifeCheck :: Grid Life -> GridCoord -> Life
lifeCheck a b = case (get a b) of
    Just Alive
        | 1 < x    &&    x < 4  -> Alive
        | otherwise             -> Dead
            where
                x = aliveCount a b
    Just Dead
        | x == 3    -> Alive
        | otherwise -> Dead
            where
                x = aliveCount a b
    _   -> Dead




-- RULES FOR GAME OF LIFE
-- | aliveCount - Counts the number of neighbouring cells which are alive
aliveCount :: Grid Life -> GridCoord -> Integer
aliveCount a (row,col) = ((sum . map aliveCounter) lifeNeighbours)
    where
        lifeNeighbours = map (get a) [
                (row-1,col-1), (row-1, col), (row-1, col+1),
                (row,col-1), (row, col+1),
                (row+1,col-1), (row+1, col), (row+1, col+1)]

-- | aliveCounter - Gives a weighting to Alive or Dead cells, current rule
-- weights Alive cells as "1" and Dead cells as "0"
aliveCounter :: Maybe Life -> Integer
aliveCounter a = case a of
    Just Alive -> 1
    _          -> 0