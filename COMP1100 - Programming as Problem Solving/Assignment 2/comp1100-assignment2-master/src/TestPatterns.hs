{-|
Module      : COMP1100 Assignment 2 - TestPatterns
Description : Contains predefined patterns for Wireworld and Game of Life
Maintainer  : u6858120@anu.edu.au

-}
module TestPatterns where

import           Automata

-- | wireworldInitial - The inital pattern loaded when the program is started,
-- and when the model is switched to Wireworld
wireworldInitial :: Grid Wireworld
wireworldInitial = makeGrid toWireworld 80 17 cells where
  cells = concat
    [ "#~*#################~*#################~*#################~*#################~*#"
    , "#                                                                              #"
    , "#  *#      ## ##                                                ##         ##  #"
    , "#  ##      ## ##                                                ##         ##  #"
    , "#  ##      ##                                                   ##         ##  #"
    , "#  ##      ## ## ## ###    ######                ####   ## ###  ##   ########  #"
    , "#  ##      ## ## #######   ###### ##        ##   ####   ####### ##   ########  #"
    , "#  ##      ## ## ###   # ##    ## ##        ## ##    ## ###   # ## ##      ##  #"
    , "#  ##  ##  ## ## ##      ######## ##        ## ##    ## ##      ## ##      ##  #"
    , "#  ##  ##  ## ## ##      ######   ##        ## ##    ## ##      ## ##      ##  #"
    , "#  ####  #### ## ##      ##       ##   ##   ## ##    ## ##      ## ##      ##  #"
    , "#  ####  #### ## ##      ##    ## ##   ##   ## ##    ## ##      ## ##      ##  #"
    , "#  ##      ## ## ##        ######  ###########   ####   ##      ## ##########  #"
    , "#  ##      ##################################################################  #"
    , "#                                                                              #"
    , "#                                                                              #"
    , "#*~#################*~#################*~#################*~#################*~#"
    ]

-- | wireworldCanvas - A blank canvas for users to draw patterns
wireworldCanvas :: Grid Wireworld
wireworldCanvas = makeGrid toWireworld 30 30 cells where
  cells = (take 900 (repeat ' ')) 

-- | diodes - A pattern which simulates a diode
diodes :: Grid Wireworld
diodes = makeGrid toWireworld 30 20 cells where
  cells = concat
    [ "                              "
    , "                              "
    , "                              "
    , "                              "
    , "                              "
    , "                ##            "
    , "        ######### ########### "
    , "  *#    #       ##            "
    , " ~  #####                     "
    , "  ##    #       ##            "
    , "        ######## ############ "
    , "                ##            "
    , "                              "
    , "                              "
    , "                              "
    , "                              "
    , "                              "
    , "                              "
    , "                              "
    , "                              "
    ]

-- | xor - A pattern which simulates an XOR gate
xor :: Grid Wireworld
xor = makeGrid toWireworld 20 20 cells where
  cells = concat
    [ "                    "
    , "                    "
    , "                    "
    , "                    "
    , "                    "
    , "  ########          "
    , " #        ##        "
    , "  *~####*~  #       "
    , "           ####     "
    , "           #  ##### "
    , "           ####     "
    , "  ###~*###  #       "
    , " #        ##        "
    , "  ######*~          "
    , "                    "
    , "                    "
    , "                    "
    , "                    "
    , "                    "
    , "                    "
    ]


-- | makeGrid - Used to convert predefined patterns into a 'Grid c' format, for later
-- functions to process
makeGrid :: (Char -> c) -> Int -> Int -> [Char] -> Grid c
makeGrid f w h cells
  | length cells == w * h = Grid w h (map f cells)
  | otherwise = error "makeGrid: dimensions don't match"

-- | toWireworld - Converts Char into Wireworld
toWireworld :: Char -> Wireworld
toWireworld a = case a of
    '#'     -> Wire
    '*'     -> EHead
    '~'     -> ETail
    _       -> Empty








-- EXTENSION: GAME OF LIFE
-- | toLife - Converts Char into Life
toLife :: Char -> Life
toLife a = case a of 
    '*'     -> Alive
    _       -> Dead

-- | lifeInitial - The inital pattern displayed when the model is switched
-- to GoL
lifeInitial :: Grid Life
lifeInitial = makeGrid toLife 20 20 cells where
  cells = concat
    [ "                    "
    , "  *   *  * * ***    "
    , " *   * * *** *      "
    , " * * *** * * ***    "
    , " * * * * * * *      "
    , "  *  * * * * ***    "
    , "                    "
    , "  *  ***            "
    , " * * *              "
    , " * * ***            "
    , " * * *              "
    , "  *  *              "
    , "                    "
    , " *   * *** ***      "
    , " *   * *   *        "
    , " *   * *** ***      "
    , " *   * *   *        "
    , " *** * *   ***      "
    , "                    "
    , "                    "
    ]

-- | lifeCanvas - A blank canvas for users to draw their own patterns
lifeCanvas :: Grid Life
lifeCanvas = makeGrid toLife 30 30 cells where
  cells = (take 900 (repeat ' ')) 

-- | oscillators - A test pattern featuring oscillators, which are patterns 
-- which remain consistant through generations
oscillators :: Grid Life
oscillators = makeGrid toLife 20 20 cells where
  cells = concat
    [ "                    "
    , "                    "
    , " ***            *** "
    , "               ***  "
    , "                    "
    , "    ***   ***       "
    , "                    "
    , "  *    * *    *     "
    , "  *    * *    *     "
    , "  *    * *    *     "
    , "    ***   ***       "
    , "                    "
    , "    ***   ***       "
    , "  *    * *    *     "
    , "  *    * *    *     "
    , "  *    * *    *     "
    , "                    "
    , "    ***   ***       "
    , "                    "
    , "                    "
    ]

-- | gosperGliderGun - A test pattern which produces gliders
gosperGliderGun :: Grid Life
gosperGliderGun = makeGrid toLife 38 20 cells where
  cells = concat
    [ "                                      "
    , "                         *            "
    , "                       * *            "
    , "             **      **            ** "
    , "            *   *    **            ** "
    , " **        *     *   **               "
    , " **        *   * **    * *            "
    , "           *     *       *            "
    , "            *   *                     "
    , "             **                       "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "
    , "                                      "                                             
    ]