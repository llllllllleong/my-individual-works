{-|
Module      : COMP1100 Assignment 2 - AutomataTest
Description : 
Maintainer  : u6858120@anu.edu.au

-}
module AutomataTest where

import           Automata
import           Testing
import           TestPatterns

-- | tests - A list of 'Test', to run
tests :: [Test]
tests = [toWireworldTest,
    toLifeTest,
    evolveWireworldTest,
    cycleWireworldTest,
    cycleLifeTest,
    nextGenWireworldTest,
    getTest,
    allCoordsTest]


-- | toWireworldTest and toLifeTest - Tests the conversion of Char to 
-- Wireworld/Life, so that predefined patterns can be used
toWireworldTest :: Test
toWireworldTest = Test "Converting Char to Wireworld" (testList (
    zipWith (assertEqual)
    (map toWireworld " *~#") 
    [Empty, EHead, ETail, Wire])
    )

toLifeTest :: Test
toLifeTest = Test "Converting Char to Life" (testList (
    zipWith (assertEqual)
    (map toLife " *z") 
    [Dead, Alive, Dead])
    )


-- | cycleWireworldTest and cycleLifeTest - Tests the next Wireworld/Life
-- when a cell is clicked, so that the ability to edit cells can be used
cycleWireworldTest :: Test
cycleWireworldTest = Test "Cycling through all Wireworlds" (testList (
    zipWith (assertEqual) 
    (map cycleWireworld [Empty, Wire, EHead, ETail]) 
    [Wire, EHead, ETail, Empty])
    )
    
cycleLifeTest :: Test
cycleLifeTest = Test "Cycling through all Life" (testList (
    zipWith (assertEqual) 
    (map cycleLife [Dead, Alive, Alive, Dead]) 
    [Alive, Dead, Dead, Alive])
    )


-- | evolvedWireworldTest
{- Testing the recursive function as well as generation function. The test 
pattern is a an electron head and tail moving from left to right. This also
test the generating function's ability to deal with edge solutions, as the test
pattern has a height of 1, thus all cells are edge solutions and the first/last
cells are corner solutions.
-}
evolveWireworldTest :: Test
evolveWireworldTest = Test "Multiple generations, testing 4 generations" (
    assertEqual 
    (evolveWireworld 4 (Grid 5 1 [EHead, Wire, Wire, Wire, Wire])) 
    (Grid 5 1 [Wire, Wire, Wire, ETail, EHead])
    )


-- Testing the rules of Wireworld. EHead and ETail should never remain the same in
-- the next generation, and Empty should always remain Empty
nextGenWireworldTest :: Test
nextGenWireworldTest = Test "Testing rules of Wireworld" (testList (
    zipWith (assertNotEqual)
    (map nextGenWireworld [
        Grid 1 1 [EHead],
        Grid 1 1 [ETail],
        Grid 1 1 [Empty],
        Grid 1 1 [Empty],
        Grid 1 1 [Empty]]) 
    [
    Grid 1 1 [EHead],
    Grid 1 1 [ETail],
    Grid 1 1 [EHead],
    Grid 1 1 [ETail],
    Grid 1 1 [Wire]])
    )

-- | getTest - Testing to ensure that 'Nothing' is returned when an invalid cell
-- is inputted. Critical to ensuring neighbouring cells can be found
getTest :: Test
getTest = Test "Testing get on cells which are in and out of bound" (testList (
    zipWith (assertEqual)
    (zipWith (get) 
        (take 6 (repeat (Grid 5 5 (take 25 (repeat Empty))))) 
        [(2,2), (4,3), (-10,10), (-5,10), (10,-10), (10,10)])
    [Just Empty, Just Empty, Nothing, Nothing, Nothing, Nothing])
    )

-- Ensuring allCoordsTest outputs in (row,column) format, and row major order
-- The first example is in (column,row), row major
-- Second example is (row,column), major row
allCoordsTest :: Test
allCoordsTest = Test "Ensures (row,column) and row major order" (testList (
    zipWith (assertNotEqual)
    [allCoords 2 2, allCoords 2 2] 
    [[(0,0),(1,0),(0,1),(1,1)], [(0,1),(1,1),(0,0),(1,0)]])
    )
