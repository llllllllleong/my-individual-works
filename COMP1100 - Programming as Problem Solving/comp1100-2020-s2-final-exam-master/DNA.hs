-- You must enter your student ID number here: u6858120
module DNA where

data Base = A | G | C | T deriving (Eq, Show) 
data DNA = Nil | Next Base (DNA) deriving (Eq, Show) 


-- | Define a function lenSeq that takes a DNA sequence and returns its length
-- Examples:
-- >>> lenSeq (Next T (Next C (Next A Nil))) 
-- 3
-- >>> lenSeq Nil
-- 0
lenSeq :: DNA -> Int
lenSeq a = case a of
    Nil         -> 0
    Next _ b    -> 1 + lenSeq b

-- | Define a function numEqLetters that counts the number of matching letters
-- in the headers of two sequences i.e. until either one of the sequences reaches
-- the end (Nil) or the letters do not match.
-- Empty sequence is not equal to anything including itself
-- Examples:
-- >>> numEqLetters (Next T (Next C (Next A Nil)))   (Next T (Next C (Next A Nil)))
-- 3
-- >>> numEqLetters (Next T (Next C (Next A Nil)))   (Next T (Next C  Nil))
-- 2
-- >>> numEqLetters (Next A (Next A Nil))   (Next T (Next C (Next A Nil)))
-- 0
-- >>> numEqLetters Nil  Nil
-- 0
numEqLetters :: DNA -> DNA -> Int
numEqLetters (Next a b) (Next c d)
    | a == c    = 1 + numEqLetters b d
    | otherwise = 0
numEqLetters _ _ = 0

-- | Define a function numSubSeqs that countes the number occurences of a contiguous 
-- subsequence (2nd argument) in an input sequence (1st argument)
-- 
-- Examples:
-- >>> numSubSeqs (Next A (Next A (Next T (Next C (Next C (Next G (Next C (Next T (Next A (Next A (Next G Nil)))))))))))   (Next A (Next A Nil))
-- 2
-- >>> numSubSeqs (Next T (Next C (Next A Nil))) (Next T (Next C  Nil))
-- 1
-- >>> numSubSeqs (Next C (Next A Nil)) (Next A (Next C  Nil))
-- 0
numSubSeqs :: DNA -> DNA -> Int
numSubSeqs Nil _ = 0
numSubSeqs a@(Next _ y) b 
    | (numSubSeqs2 a b)     = 1 + (numSubSeqs y b)
    | otherwise             = (numSubSeqs y b)

numSubSeqs2 :: DNA -> DNA -> Bool
numSubSeqs2 _ Nil = True
numSubSeqs2 Nil _ = False
numSubSeqs2 (Next a b) (Next c d)
    | a == c  &&  (numSubSeqs2 b d) = True
    | otherwise                     = False    