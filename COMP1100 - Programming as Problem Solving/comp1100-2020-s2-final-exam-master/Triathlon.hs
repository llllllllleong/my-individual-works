-- You must enter your student ID number here: u6858120
module Triathlon where

data Activity = None | Swim | Bicyle |  Run   


-- | Define a function calories that computes the number of calories 
-- burned during various activities  e.g. running, cycling or swimming 
-- depending on the distance. Given that
-- running burns 95 kilocalories  per a kilometre, and cycling is one third
-- of it, while swimming burns 4 times more calories than running
-- Examples:
-- >>> calories None 10
-- 0.0
-- >>> calories Run 10
-- 950.0
-- >>> calories Swim 13
-- 4940.0
calories :: Activity -> Double -> Double
calories a x = case a of
    None    -> 0
    Run     -> r * x
    Bicyle  -> b * x
    Swim    -> s * x
    where 
        r = 95.0
        b = r/3
        s = 4 * r

-- | Define a sum type Triathlon that has three values 
-- Novice, Olympic, and Ironman and a corresponding function
-- totalCalories that computes the total number of calories 
-- that are burned during one of the three triathlons
--  Type    | Swimming | Bicyle | Run
--  -------------------------------------
--  Novice  |  0.3 km  |   8 km |  2   km
--  Olympic |  1.5 km  |  40 km | 15   km
--  Ironman |  3.9 km  | 181 km | 42.2 km 
-- 
-- Examples:
-- >>> totalCalories Olympic
-- 3261.666666666665
-- >>> totalCalories Ironman
-- 11222.666666666666
data Triathlon = Novice
    | Olympic
    | Ironman

totalCalories :: Triathlon -> Double
totalCalories a = case a of
    Novice  -> calories Swim 0.3 + calories Bicyle 8.0 + calories Run 2.0
    Olympic -> calories Swim 1.5 + calories Bicyle 40.0 + calories Run 15.0
    Ironman -> calories Swim 3.9 + calories Bicyle 181.0 + calories Run 42.2