module Test where


data Temperature =  Low | Normal | High
    deriving (Ord, Eq, Show)


lengthh :: [Double]
lengthh = [1.0, 2.0, 3.0, 4]

newtype Point = Point (Double, Double) deriving Eq


fib :: [Integer]
fib = 0:1:[a + b | (a, b) <- zip fib (tail fib)]


inf :: Int
inf = 1 + inf


map2 :: (t -> a) -> [t] -> [a]
map2 _ []     = []
map2 f (x:xs) = f x : map f xs