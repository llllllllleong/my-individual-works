-- You must enter your student ID number here: u6858120
module Election where

-- DO NOT EDIT THESE TYPE DECLARATIONS.

data Ballot = Red | Blue | Green
    deriving (Show, Eq)

-- An example list of Ballot, for testing purposes.
-- There is no need to edit this.

exampleBallots :: [Ballot]
exampleBallots = [Red, Red, Green, Blue, Green, Red]

-- | countBallot
-- Given a list of ballots and Ballot as input,
-- return the count of the given Ballot in the list.
-- Do not reorder the list.
--
-- Examples:
--
-- >>> countBallot Red []
-- 0
-- >>> countBallot Blue [Red, Blue, Green]
-- 1

countBallot :: Ballot -> [Ballot] -> Int
countBallot a b = case b of
    []      -> 0
    x:xs
        | a == x    -> 1 + countBallot a xs
        | otherwise -> countBallot a xs

-- | rankBallots:
-- Given a list of ballots,
-- return the ballots in the order of the number of counts.
-- If there is a tie, the order does not matter between ties.
--
-- Note that it is recommended to complete countBallot
-- before attempting this question
--
-- >>> rankBallots exampleBallots
-- [Red,Green,Blue]
-- >>> rankBallots []
-- []
-- >>> rankBallots [Red, Red, Red, Blue]
-- [Red,Blue,Green]
rankBallots :: [Ballot] -> [Ballot]
rankBallots [] = []
rankBallots a 
    | r >= b && b >= g   = [Red, Blue, Green]
    | r >= g && g >= b   = [Red, Green, Blue]
    | b >= r && r >= g   = [Blue, Red, Green]
    | b >= g && g >= r   = [Blue, Green, Red]
    | g >= r && r >= b   = [Green, Red, Blue]
    | g >= b && b >= r   = [Green, Blue, Red]
    where
        r = countBallot Red a
        b = countBallot Blue a 
        g = countBallot Green a
rankBallots _ = []


-- | winnerBallot
-- Given a list of ballots, return Just the Ballot which has the most
-- counts. If there is no clear winner, return Nothing.
--
-- Note that it is NOT necessary to complete the previous functions
-- before attempting this question, but it is allowed to use previous functions.
--
-- Examples:
--
-- >>> winnerBallot exampleBallots
-- Just Red
-- >>> winnerBallot []
-- Nothing
-- >>> winnerBallot [Red, Green]
-- Nothing

winnerBallot :: [Ballot] -> Maybe Ballot
winnerBallot [] = Nothing
winnerBallot a 
    | r > b && b >= g   = Just Red
    | r > g && g >= b   = Just Red
    | b > r && r >= g   = Just Blue
    | b > g && g >= r   = Just Blue
    | g > r && r >= b   = Just Green
    | g > b && b >= r   = Just Green
    | otherwise         = Nothing
    where
        r = countBallot Red a
        b = countBallot Blue a 
        g = countBallot Green a

