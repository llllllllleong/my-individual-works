-- You must enter your student ID number here: u6858120
module Persons where

-- DO NOT EDIT THESE TYPE DECLARATIONS.

data Gender = Male | Female 
    deriving (Show, Eq)
    
data Person = Person String Gender Int 

-- An example list of Person, for testing purposes.
-- There is no need to edit this.

examplePersons :: [Person]
examplePersons = [(Person "Alex" Male 31), 
                (Person "Ellie" Female 29), 
                (Person "Victor" Male 33)]

-- | show:
-- We would like to print a person's details in a prettier format.
--
-- Make Person an instance of Show
-- by following exactly the following example specifications:
--
-- >>> Person "Alex" Male 31
-- Alex (Male, 31)
-- >>> Person "Ellie" Female 29
-- Ellie (Female, 29)

instance Show Person where
    show (Person a b c) = a ++ " (" ++ show b ++ ", " ++ show c ++ ")"

-- | isMale
-- Given a list of persons as input,
-- return the *names only* of all male persons
-- Do not reorder the list.
--
-- Note that it is NOT necessary to complete show
-- before attempting this question
--
-- Examples:
--
-- >>> isMale []
-- []
-- >>> isMale examplePersons
-- ["Alex","Victor"]

isMale :: [Person] -> [String]
isMale a = case a of
    []      -> []
    x:xs    -> case x of
        Person b c _
            | c == Male     -> b:isMale xs
            | otherwise     -> isMale xs

-- | over20
-- Given a list of persons, 
-- return True if all persons are over age 20 (>20),
-- and False otherwise.
--
-- Note that it is NOT necessary to complete the previous functions
-- before attempting this question
--
-- Examples:
--
-- >>> over20 examplePersons
-- True
-- >>> over20 []
-- True
-- >>> over20 [(Person "Ella" Female 21), (Person "Rachel" Female 32), (Person "Monica" Female 20)]
-- False

over20 :: [Person] -> Bool
over20 a = case a of
    []      -> True
    x:xs    -> case x of
        Person _ _ c
            | 20 < c  && over20 xs  -> True
            | otherwise             -> False
