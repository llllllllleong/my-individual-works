-- You must enter your student ID number here: u6858120
module Expressions where

data Expr = Val Double | Mult Expr Expr | Div Expr Expr 

-- | Create an instance of class Show and implement a function show 
-- that prints an expression of Expr in a human readable format
-- Examples:
-- >>> (Mult (Mult (Val 2) (Val 3)) (Val 4))
-- ((2.0 * 3.0) * 4.0)
-- >>>  Mult (Div (Val 2) (Val 4)) (Div (Val 1) (Val 5))
-- ((2.0 / 4.0) * (1.0 / 5.0))
-- >>> Mult (Div (Val 6) (Val 3)) (Val 4)
-- ((6.0 / 3.0) * 4.0)
-- >>>  Div (Div (Val 24) (Val 3)) (Mult (Val 2) (Val 5))
-- ((24.0 / 3.0) / (2.0 * 5.0))

instance Show Expr where
    show (Val a) = show a
    show (Mult a b) = "(" ++ show a ++ " * " ++ show b ++ ")"
    show (Div a b ) = "(" ++ show a ++ " / " ++ show b ++ ")"

-- | Define a function eval that evaluates an expression of type Expr
-- Examples:
-- >>> eval (Mult (Mult (Val 2) (Val 3)) (Val 4))
-- 24.0
-- >>> eval (Mult (Div (Val 2) (Val 4)) (Div (Val 1) (Val 5)))
-- 0.1
-- >>> eval (Mult (Div (Val 6) (Val 3)) (Val 4))
-- 8.0
-- >>> eval (Div (Div (Val 24) (Val 3)) (Mult (Val 2) (Val 5)))
-- 0.8

eval :: Expr -> Double
eval a = case a of
    Val x       -> x
    Mult x y    -> (eval x) * (eval y)
    Div x y     -> (eval x) / (eval y)
