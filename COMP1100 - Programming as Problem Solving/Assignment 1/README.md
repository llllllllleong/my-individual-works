# COMP1100 Assignment 1

| Course                                           | Coursework | Mark |
|--------------------------------------------------|:----------:|:----:|
| COMP 1100 - Programming as Problem Solving       |    1/3     |  92  |


> Hi Leong,
> I've followed up with Artin and there was a clerical error about extensions.
> I've marked the assignment you submitted.
> Here is the feedback.
> # Assignment 1 Mark Breakdown
> ## Task 1 [20 marks]
> ### toolToLabel {6} of 6
> ### nextColourIs {6} of 6
> ### nextToolIs {8} of 8
> 
> ## Task 2 [30 marks]
> ### colourNameToColour {5} of 5
> ### shapeToPicture {15} of 15
> ### colourShapeToPicture {5} of 5
> ### colourShapesToPicture {5} of 5
>
> ## Task 3 [25 marks]
> ### Task 3.1: Mouse Input {15} of 15
> ### Task 3.2: Key Presses {10} of 10
>
> ## Task 4 [10 marks]
> ### Calculating the area of individual shapes {5} of 5:
> ### Computing area of all shapes {5} of 5
>
> ========================
>
> ## Reports [15 marks]
> Documentation (what)  {1.5} of 4
> Large parts of your report repeat information that was provided
> in the assignment spec. It's not clear just from reading the
> report how a lot of your codebase works.
>
> Reflection (why)      {2} of 4
> The remarks on what could be done better is valuable,
> but you should also cover the design process, why you chose to
> write functions the way that you did.
>
> Testing (how)         {1} of 4
> Very brief mention of testing given. No concrete tests provided.
>
> Style                 {2} of 3
> Try to break up the report into more, smaller, sections.
>
> =========================
>
> No marks are awarded for code style in this assignment,
> but style marks will be given for later assignments.
> This feedback might help you improve your code style.
>
> Excellent written and formatted code. Use of comments is clear,
> and makes the code very easy to understand.
>
> Redundant case in colourShapesToPicture. You don't need to match
> against the case when you have a list of one shape.
>
> Total: 91.5 of 100