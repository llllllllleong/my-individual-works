--- Copyright 2020 The Australian National University, All rights reserved

module Controller where

import CodeWorld
import Model

import Data.Text (pack, unpack)

-- | Compute the new Model in response to an Event.
handleEvent :: Event -> Model -> Model
handleEvent event m@(Model ss t c) =
  case event of
    KeyPress key
      -- revert to an empty canvas
      | k == "Esc" -> emptyModel

      -- write the current model to the console
      | k == "D" -> trace (pack (show m)) m

      -- display the mystery image
      | k == "M" -> Model mystery t c
 
      -- Drop the last added shape, given there is a shape in the model
      | (k == "Backspace" || k == "Delete") && not (null ss)   -> Model (drop 1 ss) t c

      -- Finish polygon vertices, changes tools if less than three points are stored in PolygonTool
      | k == " " ->  case t of
          PolygonTool a   
              | (length a) >= 3   -> Model ((c,Polygon a):ss) (PolygonTool []) c
              | otherwise         -> Model ss (RectangleTool (Nothing)) c
          _     -> m

      -- Switch to the next tool, given there are no points stored in the current tool
      | k == "T" -> Model ss (nextTool t) c

      -- Switch to the next colour
      | k == "C" -> Model ss t (nextColour c)

      -- Rotate the last drawn shape anti-clockwise by 1 degrees, given that it was a rectangle or ellipse
      | k == "Left" -> case ss of 
          []    -> m
          x:_   -> case x of
              (aa,Rectangle bb cc dd)   -> Model ((aa,Rectangle bb cc (dd+(pi/180))):(drop 1 ss)) t c
              (aa,Ellipse bb cc dd)     -> Model ((aa,Ellipse bb cc (dd+(pi/180))):(drop 1 ss)) t c
              _                         -> m
    
      -- Rotate the last drawn shape clockwise by 1 degrees, given that it was a rectangle or ellipse
      | k == "Right" -> case ss of 
          []    -> m
          x:_   -> case x of
              (aa,Rectangle bb cc dd)   -> Model ((aa,Rectangle bb cc (dd-(pi/180))):(drop 1 ss)) t c
              (aa,Ellipse bb cc dd)     -> Model ((aa,Ellipse bb cc (dd-(pi/180))):(drop 1 ss)) t c
              _                         -> m

      -- Ignore other events
      | otherwise -> m
      where
        k = unpack key

    PointerPress p ->  case t of
        LineTool _                                      -> Model ss (LineTool (Just p)) c
        RectangleTool _                                 -> Model ss (RectangleTool (Just p)) c                                
        CircleTool _                                    -> Model ss (CircleTool (Just p)) c
        EllipseTool _                                   -> Model ss (EllipseTool (Just p)) c
        PolygonTool a                                   -> Model ss (PolygonTool (p:a)) c
        ParallelogramTool (Nothing) (Nothing)           -> Model ss (ParallelogramTool (Just p) (Nothing)) c   
        ParallelogramTool (Just a) (Nothing)            -> Model ss (ParallelogramTool (Just a) (Just p)) c
        ParallelogramTool (Nothing) (Just a)            -> Model ss (ParallelogramTool (Just a) (Just p)) c
        ParallelogramTool (Just a) (Just b)             -> Model (newshape:ss) (ParallelogramTool (Nothing) (Nothing)) c
            where
              newshape = (c,Parallelogram a b p)

    PointerRelease p ->  case t of
        LineTool (Just a)                               -> Model ((c,Line a p):ss) (LineTool (Nothing)) c
        RectangleTool (Just a)                          -> Model ((c,Rectangle a p 0):ss) (RectangleTool (Nothing)) c                                
        CircleTool (Just a)                             -> Model ((c,Circle a p):ss) (CircleTool (Nothing)) c
        EllipseTool (Just a)                            -> Model ((c,Ellipse a p 0):ss) (EllipseTool (Nothing)) c
        _                                               -> m
    _   -> m


-- Returns the next colour 
nextColour :: ColourName -> ColourName
nextColour a = case a of
    Black -> Red
    Red -> Orange
    Orange -> Yellow
    Yellow -> Green
    Green -> Blue
    Blue -> Purple
    Purple -> Black


-- Changes to the next tool, only if the current tool is not holding a point
nextTool :: Tool -> Tool
nextTool a = case a of
    LineTool (Nothing)                      -> PolygonTool []
    PolygonTool []                          -> RectangleTool (Nothing)
    RectangleTool (Nothing)                 -> CircleTool (Nothing)
    CircleTool (Nothing)                    -> EllipseTool (Nothing)
    EllipseTool (Nothing)                   -> ParallelogramTool (Nothing) (Nothing)
    ParallelogramTool (Nothing) (Nothing)   -> LineTool (Nothing)
    _                                       -> a
