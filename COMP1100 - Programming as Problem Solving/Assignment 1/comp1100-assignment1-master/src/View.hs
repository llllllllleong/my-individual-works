--- Copyright 2020 The Australian National University, All rights reserved

module View where

import CodeWorld
import Data.Text (pack)
import Model

-- | Render all the parts of a Model to a CodeWorld picture.
modelToPicture :: Model -> Picture
modelToPicture (Model ss t c)
  = translated 0 8 toolText
  & translated 0 7 colourText
  & translated 0 (-8) areaText
  & colourShapesToPicture ss
  & coordinatePlane
  where
    colourText = stringToText (show c)
    toolText = stringToText (toolToLabel t)
    areaText = stringToText (areaToLabel ss t)
    stringToText = lettering . pack


{- | Using the areaShapes function, return a string that describes the area
   of all the shapes matching the current tool. -}
areaToLabel :: [ColourShape] -> Tool -> String
areaToLabel css t = case t of
  RectangleTool _       -> "The total area of the rectangles is "
                           ++ (show $ areaShapes ss t) ++ " units."
  CircleTool _          -> "The total area of the circles is "
                           ++ (show $ areaShapes ss t) ++ " units."
  EllipseTool _         -> "The total area of the ellipses is "
                           ++ (show $ areaShapes ss t) ++ " units."
  ParallelogramTool _ _ -> "The total area of the parallelograms is "
                           ++ (show $ areaShapes ss t) ++ " units."
  _                     -> []
  where ss = map snd css


-- toolToLabel: Returns tool specific instructions
toolToLabel :: Tool -> String
toolToLabel a = case a of
    LineTool _ -> "Line... click-drag-release"
    PolygonTool _ ->  "Polygon... click 3 or more times then spacebar"
    RectangleTool _ -> "Rectangle... click-drag-release"
    CircleTool _ -> "Circle... click-drag-release"
    EllipseTool _ -> "Ellipse... click-drag-release"
    ParallelogramTool _ _ -> "Parallelogram... click two opposite vertices, then a third"


-- colourShapesToPicture: Takes a list of ColourShapes, and inputs each element individually into colourShapeToPicture
-- Recursion allows for multiple shapes to be displayed at the same time
-- If an empty list is inputted, a blank picture is returned
colourShapesToPicture :: [ColourShape] -> Picture
colourShapesToPicture a = case a of
    []    -> blank
    x:[]  -> colourShapeToPicture x
    x:xs  -> colourShapeToPicture x & colourShapesToPicture xs


-- colourShapeToPicture: Takes a ColourShape, and renders a picture as specified by the ColourShape
colourShapeToPicture :: ColourShape -> Picture
colourShapeToPicture (a,b) = coloured (colourNameToColour a) (shapeToPicture b)


-- colourNameToColour: Takes a ColourName (which is used for string output) and changes it to a CodeWorld colour (for picture output)
colourNameToColour :: ColourName -> Colour
colourNameToColour a = case a of
    Black   -> black
    Red     -> red 
    Orange  -> orange
    Yellow  -> yellow
    Green   -> green
    Blue    -> blue
    Purple  -> purple
   

-- shapeToPicture: Input a shape, and output a codeworld picture
shapeToPicture :: Shape -> Picture
shapeToPicture shape = case shape of
    Line a b -> polyline [a,b]
    Polygon a -> solidPolygon a
    Rectangle (ax,ay) (bx,by) e -> translated tx ty (rotated e (solidRectangle wd ht)) 
        where
          tx = (ax+bx)*0.5
          ty = (ay+by)*0.5
          wd = ax-bx
          ht = ay-by
    Circle (a,b) (c,d) -> translated a b (solidCircle (sqrt ((dx*dx)+(dy*dy))))
        where
          dx = c-a
          dy = d-b 
    Ellipse (a,b) (c,d) e -> translated tx ty (rotated e (scaled sx sy (solidCircle r)))
        where
          tx = (a+c)/2.0
          ty = (b+d)/2.0
          sx = 1.0
          sy = (d-b)/(c-a)
          r = (c-a)*0.5
    Parallelogram (ax,ay) (cx,cy) (bx,by) -> solidPolygon [(ax,ay),(bx,by),(cx,cy),(dx,dy)]
        where
          dx = ax+cx-bx
          dy = ay+cy-by


-- areaShapes & helpers: Calculates the total area of a specific shape, on the current model. Applicable only to Rectangle, Circle, Ellipse and Parallelograms.
-- areaShapes: Identifies the shape of interest, and passes on the list of Shapes to the respective area calculation function 
areaShapes :: [Shape] -> Tool -> Double
areaShapes a t = case t of
    RectangleTool _       -> areaRec a
    CircleTool _          -> areaCirc a
    EllipseTool _         -> areaEllipse a
    ParallelogramTool _ _ -> areaPara a
    _                     -> 0

-- areaRec: Filters out shapes which are not rectangles, and outputs the sum of the area of shapes which are rectangles
areaRec :: [Shape] -> Double
areaRec [] = 0
areaRec (x:xs) = case x of
    Rectangle (x1,y1) (x2,y2) _     -> abs (x2-x1) * abs (y2-y1) + areaRec xs
    _   -> areaRec xs

-- areaCirc: Filters out shapes which are not Circles, and outputs the sum of the area of shapes which are Circles
areaCirc :: [Shape] -> Double
areaCirc [] = 0
areaCirc (x:xs) = case x of
    Circle (x1,y1) (x2,y2)          -> pi*r*r + areaCirc xs
        where
          r = sqrt (dx*dx + dy*dy)
            where
              dx = x2-x1
              dy = y2-y1             
    _   -> areaCirc xs

-- areaEllipse: Filters out shapes which are not Ellipses, and outputs the sum of the area of shapes which are Ellipses
areaEllipse :: [Shape] -> Double
areaEllipse [] = 0
areaEllipse (x:xs) = case x of
    Ellipse (x1,y1) (x2,y2) _       -> pi*l1*l2 + areaEllipse xs
        where
          l1 = 0.5*(abs (x2-x1))
          l2 = 0.5*(abs (y2-y1))
    _   -> areaEllipse xs

-- areaPara: Filters out shapes which are not Parallelogram, and outputs the sum of the area of shapes which are Parallelogram
areaPara :: [Shape] -> Double
areaPara [] = 0
areaPara (x:xs) = case x of
    Parallelogram (x1,y1) (x2,y2) (x3,y3)    -> abs (p1-p2) + areaPara xs
        where
          p1 = (x3-x1) * (y2-y1)
          p2 = (y3-y1) * (x2-x1)
    _   -> areaPara xs




