# FINM3007

| Course                                                   | Coursework | Mark | ANU Grade |
|----------------------------------------------------------|:----------:|:----:|:---------:|
| FINM3007 - Advanced Derivatives Pricing and Applications |    1/2     |  84  |    HD     |
| FINM3007 - Advanced Derivatives Pricing and Applications |    2/2     | 100  |    HD     |

Most difficult course of my entire degree.
