# My Individual Works

Hello, and welcome to my university portfolio!


![](Images/GameOfLife.GIF)
<br>
From: COMP1100 Assignment 2 (2020)
<br>
<br>


## What is this?
At the time of creation, I am in the final year of my Actuarial and Quant 
Finance degree. I realised that I don't have a way to showcase the work I am 
proud of, or to share what I have learnt at university. I created this repo 
(and a personal GitLab account) to function as a record of my learning, and a 
portfolio of my individual achievements.

Additionally, I often found myself furthering my leaning with code, whether it 
was to deepen my understanding of Monte Carlo simulation by creating my own method 
rather than modifying the lecture sample, or to optimise study efficiency 
with my Life Contingencies calculator. I want to use this repo to record such 
instances where my curiosity peaked.

<br>
<img src="Images/ADPA1.jpg" width="400" height="565">
<img src="Images/ADPA2.jpg" width="400" height="565">
<br>
From: FINM3007 Assignment 1 (2024)
<br>
<br>
<img src="Images/StudyMePreview.jpg" width="800" height="450">
<br>
From: COMP2100 Group Project (2024)
<br>
<br>




## Summary of works
Reverse chronological order, by course.

| Course                                                   |                 Coursework                  | Mark | ANU Grade |
|----------------------------------------------------------|:-------------------------------------------:|:----:|:---------:|
| COMP3600 - Algorithms                                    |                     1/2                     |  83  |    HD     |
| COMP3600 - Algorithms                                    |                     2/2                     | 100  |    HD     |
| COMP2400 - Relational Databases                          |                     1/2                     | 100  |    HD     |
| COMP2400 - Relational Databases                          |                     2/2                     |  95  |    HD     |
| FINM3007 - Advanced Derivatives Pricing and Applications |                     1/2                     |  84  |    HD     |
| FINM3007 - Advanced Derivatives Pricing and Applications |                     2/2                     | 100  |    HD     |
| COMP2620 - Logic                                         |                     1/2                     |  76  |     D     |
| COMP2620 - Logic                                         |                     2/2                     |  86  |    HD     |
| COMP2100 - Software Design Methodologies                 | Group Project <br/> (Individually Assessed) |  97  |    HD     |
| ECON2102 - Macroeconomics II                             |                     NA                      |  -   |     -     |
| STAT3058 - Risk Modelling II                             |                     1/3                     |  96  |    HD     |
| STAT3058 - Risk Modelling II                             |                     2/3                     |  97  |    HD     |
| STAT3058 - Risk Modelling II                             |                     3/3                     |  89  |    HD     |
| STAT3037 - Life Contingencies                            |                     1/2                     |  80  |    HD     |
| STAT3037 - Life Contingencies                            |                     2/2                     | 100  |    HD     |
| STAT3038 - Actuarial Techniques                          |                     NA                      |  -   |     -     |
| STAT3057 - Risk Modelling I                              |                     1/2                     |  78  |     D     |
| STAT3057 - Risk Modelling I                              |                     2/2                     |  84  |    HD     |
| STAT3032 - Survival Models                               |                     1/2                     |  94  |    HD     |
| STAT3032 - Survival Models                               |                     2/2                     |  98  |    HD     |
| FINM3003 - Continuous Time Finance                       |                     NA                      |  -   |     -     |
| STAT2032 - Financial Mathematics                         |                     1/1                     | 100  |    HD     |
| STAT2005 - Introduction to Stochastic Processes          |                     1/3                     |  71  |     D     |
| STAT2005 - Introduction to Stochastic Processes          |                     2/3                     |  95  |    HD     |
| STAT2005 - Introduction to Stochastic Processes          |                     3/3                     |  86  |    HD     |
| COMP1110 - Structured Programming                        |                Group Project                |  91  |    HD     |
| COMP1600 - Foundations of Computation                    |                     1/3                     |  67  |    CR     |
| COMP1600 - Foundations of Computation                    |                     2/3                     |  92  |    HD     |
| COMP1600 - Foundations of Computation                    |                     3/3                     |  92  |    HD     |
| STAT2001 - Introductory Mathematical Statistics          |                     1/2                     |  92  |    HD     |
| STAT2001 - Introductory Mathematical Statistics          |                     2/2                     | 100  |    HD     |
| COMP1100 - Programming as Problem Solving                |                     1/3                     |  92  |    HD     |
| COMP1100 - Programming as Problem Solving                |                     2/3                     |  92  |    HD     |
| COMP1100 - Programming as Problem Solving                |                     3/3                     |  77  |     D     |


## What will I share here?
All assignments I have completed since starting/transferring to BACT/FIN. 
* My submissions
* Questions ~~and Solutions~~ (I'll try my best to include)

Bits and pieces of programming:
* Java
* R
* Haskell

My handwritten notes:
* I often put effort into my notes to improve legibility and effectiveness
* Helps to decrease study burden


More Samples:
<br>
<img src="Images/RM1A.jpg" width="400" height="565">
<img src="Images/RM1B.jpg" width="400" height="565">
<br>
From: STAT3057 Notes (2023)
<br>
<br>
<img src="Images/CTF1.jpg" width="400" height="565">
<img src="Images/CTF2.jpg" width="400" height="565">
<br>
From: FINM3003 Notes (2023)
<br>
<br>
<img src="Images/FC1.jpg" width="400" height="565">
<img src="Images/FC2.jpg" width="400" height="565">
<br>
<img src="Images/FC3.jpg" width="400" height="565">
<img src="Images/FC4.jpg" width="400" height="565">
<br>
From: COMP1600 Assignment 2 (2022)
<br>
<br>
<img src="Images/INV1.jpg" width="400" height="437">
<img src="Images/INV2.jpg" width="400" height="437">
<br>
From: FINM2003 Notes (2020, Not uploaded)
<br>
<br>




Note: I do not assure any accuracy of the content in this repo. The material I 
upload here is raw, in the sense that they are the same as they were at 
their respective time e.g. at time of submission, or at time of course completion.

<br>
<br>


## Timeline
#### 2019 Semester 1: Enrolled in Bachelor of Economics and Finance
- ECON1101 - Microeconomics I
- ECON1102 - Macroeconomics I
- FINM1001 - Foundations of Finance
- STAT1008 - Quantitative Research Methods
#### 2019 Semester 2
- BUSN1001 - Business Reporting and Analysis
- FINM2001 - Corporate Finance
- MATH1013 - Mathematics and Applications I
- MGMT2100 - Communication for Business
#### 2020 Semester 1
- ECON2101 - Microeconommics II
- FINM2002 - Derivatives
- MATH1014 - Mathematics and Applications II
- STAT2008 - Regression Modelling
#### 2020- Semester 2 (Remote study in Singapore)
- COMP1100 - Programming as Problem Solving
- FINM2003 - Investments
- FINM3005 - Corporate Valuation
- STAT2001 - Introductory Mathematical Statistics
#### 2020 - 2022: National Service Fulfillment
#### 2022: Transfer to Bachelor of Actuarial Studies and Finance
Upon completion of National Service in Singapore, I returned to my studies
with a new mindset. I felt that I had been restricted to doing "nothing" during the
term of service, and my prior mindset of being content with passing, would not
suffice. My decision to transfer to a more rigorous degree, was essentially the
first time I had ever proposed a challenge to myself. In hindsight, I failed to
consider my passion for CompSci as a personal strength, and I tunnel-visioned
myself into my current degree. I don't regret the decision to transfer, but I often ponder if
I'd be happier off learning CompSci as opposed to grinding advanced
statistics into my memory.

To fulfill my passion/interest for CompSci, I exhausted all my remaining electives on
COMP courses.

#### 2022 Semester 2
- COMP1110 - Structured Programming
- COMP1600 - Foundations of Computing
- STAT2005 - Introduction to Stochastic Processes
- STAT2032 - Financial Mathematics
#### 2023: Awarded Senior Resident scholarship, reduced study load
I was fortunate to receive a Senior Resident scholarship at Wright Hall for the 
2023 academic year. Self reflection allowed me to recognise that my personality of that of a quiet, and perhaps  
awkward one. Furthermore, there may have been occasions where I struggled to convey 
my thoughts and feelings effectively and to understand the emotions of others.

I saw this scholarship as a valuable opportunity to develop my intrapersonal 
skills. My army training instilled the principles of efficient teamwork, 
leading me to realise that if I wanted to excel in my career, cultivating these 
collaborative skills was more important than focusing solely on my individual 
abilities. I came to understand the critical role of teamwork in the 
professional world, something I had previously overlooked.

Given these insights, I decided to reduce my course load to three courses per 
semester, allowing me to dedicate more time and effort to my Senior Resident 
role. As a result, my studies will be extended by one semester, with my 
graduation now deferred to the end of 2024.
#### 2023 Semester 1
- STAT3057 - Risk Modelling I
- STAT3032 - Survival Models
- FINM3003 - Continuous Time Finance
#### 2023 Semester 2
- STAT3058 - Risk Modelling II
- STAT3037 - Life Contingencies
- STAT3038 - Actuarial Techniques
#### 2024 Semester 1
- COMP2620 - Logic
- COMP2100 - Software Design Methodologies
- ECON2102 - Macroeconomics II
#### 2024 Semester 2
- FINM3007 - Advanced Derivatives Pricing and Applications
- COMP3600 - Algorithms
- COMP2400 - Relational Databases


<br>
<br>

### Unrelated rant about online exams and assessments
For better or worse, I have always conducted my work individually(apart from 
group projects). Although it is inefficient, I value raw learning derived from 
doing things myself. From what I observed in the first two years of 
uni, colluding on assessments is simply an optimisation strategy to 
achieve the highest grade with minimal effort (Note: I make a 
distinction between colluding and collaboration. I acknowledge that 
collaboration with like-minded goal orientated individuals is likely the 
optimal learning experience).

The advent of COVID saw the necessary evil of remote leaning being instated at 
almost every university, including mine. However, I'm sure that my college 
will not acknowledge the levels of rampant cheating and collusion I observed. 
I felt betrayed, because it was entirely possible for anyone to obtain 
a bachelor degree without ever setting foot on campus, or reading a single 
lecture slide. I even met a final year actuarial student with a 6.5+ GPA, who 
could not solve problems from first year STAT courses. They explained to me, 
how every other student is doing the same thing, and when an assessment comes along, they simply group up. Online exams were intentionally unreasonably 
lengthy, to deter students from searching for answers. However, they showed me 
about how they pre-allocate questions and use the time that they would 
otherwise spend on other questions, to search for answers online (Did you 
know, actuarial exam exemptions were still granted during this period?). 

Feeling betrayed, I consulted with lecturers, one of them replying:

> "Hi Leong,
    Sorry for not replying sooner.
    Yes, the possibility of collusion is one of the very frustrating aspects 
    of remote exams. Many of us are looking forward to a return to on-campus 
    exams next semester. We try to detect collusion during the marking process 
    by looking for similarities between student answers. This is imperfect, 
    but if we encounter evidence of probable collusion we refer it on to our 
    head of education to investigate."

Now I'm not one to snitch, and never-mind 
what other people do. You could argue that in this world, you do what you have 
to do to get ahead of others. The only reason why this bothers me is that 
final grades are often scaled according to the cohort's performance.

ANU reinstated in-person teaching and exams in 2023, Semester 2.













