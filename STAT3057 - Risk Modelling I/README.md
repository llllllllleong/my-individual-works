# STAT3057

| Course                                           | Coursework |  Mark  |
|--------------------------------------------------|:----------:|:------:|
| STAT 3057 - Risk Modelling I                     |    1/2     |   78   |
| STAT 3057 - Risk Modelling I                     |    2/2     |   84   |



All R-Code in this directory was written by myself.
I have left the code in raw form because it is unlikely I will use it again.

---
## Assignment 1
* Inverse-Burr distribution derivation
* Quantiles function derivation
* Method of Maximum Likelihood 
* Method of Moments 
* Method of Percentiles
* Portfolio-wide distribution of X
* Chi-square goodness-of-fit 
* Chi-square test statistic

R-Code
* Unremarkable

---
## Assignment 2
* Extreme Value Theory
* Gaussian Dependency Copula
* Gumbel Dependency Copula
* Value at Rick (VaR)
* Time-Series
* ARIMA
* PACF
* ACF
* Time-Series diagnostics

This assignment was situated in the last teaching week, which is unusual. The 
time series content was split into 6 parts, all crammed into week 11 and 12. 
We were supposed to study and finish the assignment in week 12, because the 
final exam was scheduled for the week after.

Overall, I feel like I learnt nothing useful about time series modelling, and 
that I was just mindlessly copying and adjusting R-Code to reproduce results.

R-Code
* Extreme Value Theory calculations
* Time Series packages
