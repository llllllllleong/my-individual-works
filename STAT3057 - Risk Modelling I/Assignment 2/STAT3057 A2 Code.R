rm(list = ls())
set.seed(6858120)

setwd("/Users/leong/Desktop/STAT3057/Assignment 2")

library(MASS)
# install.packages("copula")
library(copula)
# install.packages("scatterplot3d")
library(scatterplot3d)
# install.packages("ggplot2")
library(ggplot2)
# install.packages("grid")
library(grid)
# install.packages("fExtremes")
library(fExtremes)
# install.packages("forecast")
require(forecast)
library(tseries)
# install.packages("rugarch")
library(rugarch)

# Question 2
# Ai)
# Generate a Gaussian copula with 2 dimensions (bivariate) and rho=0.3
lnormSD <- sqrt(1.3)
gaussianCopula <- normalCopula(param = 0.3,
                      dim = 2)
gamma_LN_MVD_Gaussian <- mvdc(copula = gaussianCopula,
                     margins = c("gamma", "lnorm"),
                     paramMargins = list(list(shape = 0.63, scale = 4780),
                                         list(meanlog = 6.2, sdlog = lnormSD)
                                         ))
simulatedData <- rMvdc(10000,gamma_LN_MVD_Gaussian)

par(mfrow=c(2,2))
plot(simulatedData[,1],
     simulatedData[,2],
     xlab="Payment A (Gamma distributed)",
     ylab="Payment B (Lognormal distributed)",
     main = "Gaussian Dependency",
     pch=46)
# scatterplot3d(simulatedData[,1], 
#               simulatedData[,2], 
#               xlab="y1 (gamma distributed)",
#               ylab="y2 (lognormal distributed)",
#               color = "blue",
#               pch = ".")
# plot(simulatedData[,1])


# Aii)
# Generate Gumbel copula with 2 dimensions and alpha = 10
gumbelCopula <- archmCopula(family = "gumbel",
                            dim = 2,
                            param = 10)

gamma_LN_MVD_Gumbel <- mvdc(copula = gumbelCopula,
                     margins = c("gamma", "lnorm"),
                     paramMargins = list(list(shape = 0.63, scale = 4780),
                                         list(meanlog = 6.2, sdlog = lnormSD)
                                         ))
simulatedData2 <- rMvdc(10000,gamma_LN_MVD_Gumbel)
plot(simulatedData2[,1],
     simulatedData2[,2],
     xlab="Payment A (Gamma distributed)",
     ylab="Payment B (Lognormal distributed)",
     main = "Gumbel Dependency",
     pch=46)
scatterplot3d(simulatedData2[,1], 
              simulatedData2[,2], 
              xlab="Payment A (Gamma distributed)",
              ylab="Payment B (Lognormal distributed)",
              color = "blue",
              pch = ".")


# Bi)
# Number of simulations
n <- 10000
# Define Retention level
m <- 15000
# Gaussian copula
mean(simulatedData[,1])
mean(simulatedData[,2])
# Define X as the sum of A and B
xGaussian <- simulatedData[,1] + simulatedData[,2]
xGaussianLargerThan15000Count <- length(xGaussian[xGaussian > m])
# P(X > 15,000)
(xGaussianLargerThan15000Count / n)

# Gumbel copula
mean(simulatedData2[,1])
mean(simulatedData2[,2])
# Define X as the sum of A and B
xGumbel <- simulatedData2[,1] + simulatedData2[,2]
xGumbelLargerThan15000Count <- length(xGumbel[xGumbel > m])
# P(X > 15,000)
(xGumbelLargerThan15000Count / n)


# Bii)
# Gaussian copula
xGaussianLargerThan15000 <- xGaussian[xGaussian > m]
xGaussianConditionalExcess <- xGaussianLargerThan15000 - 15000
# Conditional Excess
mean(xGaussianConditionalExcess)
# Unconditional Excess
xGaussianExcess <- ifelse(xGaussian > 15000, xGaussian - m, 0)
mean(xGaussianExcess)

# Gumbel copula
xGumbelLargerThan15000 <- xGumbel[xGumbel > m]
xGumbelConditionalExcess <- xGumbelLargerThan15000 - 15000
# Conditional Excess
mean(xGumbelConditionalExcess)
# Unconditional Excess
xGumbelExcess <- ifelse(xGumbel > 15000, xGumbel - m, 0)
mean(xGumbelExcess)










# Question 3
Q3data <- read.csv("Assign2_Q3data.csv", header = TRUE)
# C)
# q3Losses <- Q3data * 5000000 * -1
q3Losses <- Q3data * -1
GEV <- gevFit(q3Losses, block = 21, type = c("mle"))
# xi is gamma
# mu is alpha
# beta is beta
g <- -0.104945702
a <- 0.014968065
b <- 0.006180474
a + (b/g) * ((-21 * log(0.95))^(-g)-1)
5000000*(a + (b/g) * ((-21 * log(0.95))^(-g)-1))
# E)
VaR <- quantile(q3Losses, probs= c(0.95), na.rm= TRUE)
VaR
VaR * 5000000
# 95% 
# 78275 
extremeLossCount <- length(q3Losses[q3Losses > VaR])
extremeLossSum <- sum(q3Losses[q3Losses > VaR])
TVaR <- extremeLossSum / extremeLossCount
TVaR * 5000000.0000
# > TVaR
# [1] 104910.3



# Question 4
# C)
# Parameters
# Phi = 0.4
# Theta = 0.3
# Var(Wt) = 1
# n = 100
set.seed(123)
phi <- 0.4
theta <- 0.3
variance <- 1
simulatedARIMA <- arima.sim(n = 1000,
                            model = list(ar = c(0, 0, phi),
                                         ma = c(0, theta)),
                            sd = sqrt(1)
)
par(mfrow=c(1,2))
plot(simulatedARIMA,
     type="l",
     main="Simulated ARIMA(3,0,2)",
     ylab = "x(t)")
simulatedARIMACumSum <- cumsum(simulatedARIMA)
plot(simulatedARIMACumSum,
     type="l",
     main="Cumulative Sum) ARIMA(3,0,2)",
     xlab = "Time",
     ylab = "x(t)")
# Fitting a model to simulated data
arimaFit <- arima(simulatedARIMA,
                  order = c(3,0,2),
                  include.mean=FALSE)
arimaFit
par(mfrow=c(1,1))
plot(forecast(arimaFit,h=50),
     ylab = "x(t)",
     xlab = "Time")
summary(forecast(arimaFit,h=50))
forecast(arimaFit,h=50)




# Question 5
rm(list = ls())
Q5data <- read.csv("Assign2_Q5data.csv", header = TRUE)

Q5data2  <- Q5data
Q5data2$day = rep(1:200)
Q5data2$price = rep(1:200)
Q5data2[1,3] = (1 + 1*Q5data2$Return[1])
for (i in 2:200) {
  Q5data2[i,3] <- Q5data2[i-1,3] + Q5data2[i-1,3] * Q5data2[i,1]
}
Q5data2$day = as.Date(Q5data2$day)

# Undiferenced Underlying movement, PCF and PACF Plots
par(mfrow=c(1,3))
plot(Q5data2$day,Q5data2$price,
     type="l",
     col=4,
     main = "Underlying Movement")
acf(Q5data2$price,
    main = "Sample ACF")
pacf(Q5data2$price,
     main = "Sample PACF")
adf.test(Q5data2$price)
# Dicky fuller tests concludes there is a unit root
# Suggesting that differencing is required

# A)
par(mfrow=c(1,3))
plot(Q5data$Return,
     type="l",
     col=4,
     main = "Differenced Underlying Movement")
acf(Q5data,
    main = "Sample ACF")
pacf(Q5data,
     main = "Sample PACF")
# Blue lines are 95% Confidence intervals
# Significant autocorrelation is above the lines
# We would expect 1 out of 20 to exceed
# x value is the lags
# ACF cuts off after 7

# Residual ACF values are approximately normally distributed, where (+- (-2)/SQRT(N) )
acf(Q5data,
    main = "Sample ACF",
    plot = FALSE)
pacf(Q5data,
     main = "Sample PACF",
     plot = FALSE)
# Part B)
# Fitting models to the differenced data
AR1 <- arima(Q5data,
             order = c(1,0,0))
AR2 <- arima(Q5data,
             order = c(2,0,0))
AR3 <- arima(Q5data,
             order = c(3,0,0))
AR4 <- arima(Q5data,
             order = c(4,0,0))
AR5 <- arima(Q5data,
             order = c(5,0,0))
AR6 <- arima(Q5data,
             order = c(6,0,0))

AR1AIC <- -2*AR1$loglik+2*(1+2)
AR2AIC <- -2*AR2$loglik+2*(2+2)
AR3AIC <- -2*AR3$loglik+2*(3+2)
AR4AIC <- -2*AR4$loglik+2*(4+2)
AR5AIC <- -2*AR5$loglik+2*(5+2)
AR6AIC <- -2*AR6$loglik+2*(6+2)

# intercept term is the mean
# Use "Arima" to find phi0

# AR3 has lowest AIC value
AR3AIC
AR3
# residuals(AR3)
Arima(Q5data,
      order = c(3,0,0))
Arima(Q5data,
      order = c(3,0,0))[1]
phi0 <- 0.002249151 * (1 - 0.265958316 - 0.033427541 - 0.284847233)
phi0
phi1 <- 0.265958316
phi2 <- 0.033427541
phi3 <- 0.284847233
# DIAGNOSTICS
# ljung-box test
# Also check: residuals have mean of zero and constant variance
# Residuals are normally distributed
# Use qqplot and shapiro-wil
Box.test(AR3$residuals,
         lag = 10,
         type = "Ljung",
         fitdf = 4)
# Null = residuals are white noise (independent)
# Reject null (bad)

qqnorm(AR3$residuals)
qqline(AR3$residuals, col = 2)


# Test of normality
shapiro.test(AR3$residuals)
# Fail to reject?
# It is normal?


checkresiduals(AR3)
plot(AR3$residuals,
     main = "AR3 Residual Plot")



# D)
squaredData <- Q5data^2
par(mfrow=c(1,2))
acf(squaredData,
    main = "Squared sample ACF")
pacf(squaredData,
     main = "Squared sample PACF")
AR3Squared <- arima(squaredData,
             order = c(3,0,0))

acf(AR3Squared$residuals,
    main = "Squared sample ACF")
pacf(AR3Squared$residuals,
     main = "Squared sample PACF")
checkresiduals(AR3Squared)


# E)
arch1 = ugarchspec(variance.model=list(garchOrder=c(1,0)),
                   mean.model = list(armaOrder=c(3,0),include.mean=TRUE),
                   fixed.pars=list(mu = phi0, 
                                   ar1 = phi1,
                                   ar2 = phi2,
                                   ar3 = phi3))
arch1Fit <- ugarchfit(spec=arch1, data=Q5data)
arch1Fit

garch1 <- ugarchspec(variance.model=list(garchOrder=c(1,1)),
                     mean.model = list(armaOrder=c(3,0),include.mean=TRUE),
                     fixed.pars=list(mu = phi0,
                                     ar1 = phi1,
                                     ar2 = phi2,
                                     ar3 = phi3))
garch1Fit = ugarchfit(spec=garch1, data=Q5data)  
garch1Fit
