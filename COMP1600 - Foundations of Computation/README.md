# COMP1600

| Course                                           | Coursework |  Mark  |
|--------------------------------------------------|:----------:|:------:|
| COMP 1600 - Foundations of Computation           |    1/3     |   67   |
| COMP 1600 - Foundations of Computation           |    2/3     |   92   |
| COMP 1600 - Foundations of Computation           |    3/3     |   92   |

One of my all time favorite courses, a field of study which was new to me.

---
## Assignment 1
* Boolean Functions
* Boolean Equations
* Propositional Natural Deduction
* First Order Natural Deduction
* Binary Relations 

---
## Assignment 2
* Structural Induction over Lists
* Structural Induction on Trees
* Hoare Logic (partial correctness)


---
## Assignment 3
* DFA Languages
* FSA Conversions
* e-NFA to NFA
* Regex
* CFGs and PDAs
* Determinism in PDAs