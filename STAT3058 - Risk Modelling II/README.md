# STAT3058

| Course                                           | Coursework | Mark |
|--------------------------------------------------|:----------:|:----:|
| STAT 3058 - Risk Modelling II                    |    1/3     |  96  |
| STAT 3058 - Risk Modelling II                    |    2/3     |  97  |
| STAT 3058 - Risk Modelling II                    |    3/3     |  89  |

All R-Code in this directory was written by myself.
I have left the code in raw form because it is unlikely I will use it again. 

Note: In assignment 3's report, turnitin incorrectly states the final mark 
as (75.5/90). As in the solutions, the mark should be calculated as
(75.5/85) for undergraduates.

---
## Assignment 1
* Utility Function: Individual and Insurer
* Risk Tolerance
* Policyholder Behavior
* Aggregate Claims Modelling
* Collective Risk Model
* Individual Risk Model
* Aggregate via Convolution
* EOL Reinsurance
* Claims simulations and comparisons

R-Code:
* Discretization approximation
* n-fold convolution calculation via matrix
* Simulation of individual claims

---
## Assignment 2
* Cumulant generating functions
* Lognormal and translated Gamma approximations
* MGF of random claim amount
* Adjustment coefficient: r
* Monte Carlo simulation
* Probability of ultimate ruin
* Lundberg’s Upper Bound


R-Code:
* Claims simulation
* Ultimate Ruin probability simulation
* Monte Carlo Simulation via Interarrival Times
* Monte Carlo Simulation via Time Steps
* Surplus process simulation


---

#### From: Gaurav Khemka <gaurav.khemka@anu.edu.au>
#### Sent: Wednesday, 20 September 2023 11:57 AM
#### To: Leong Chan <u6858120@anu.edu.au>
#### Subject: STAT3058 A2 - Simulation question

Hi Leong,

I am happy for you to simulate inter-arrival times. The results are then expected to be slightly different, but that is okay.

Regards,  
Garry

---

#### From: Leong Chan <u6858120@anu.edu.au>
#### Sent: Wednesday, 20 September 2023 11:55 AM
#### To: Gaurav Khemka <gaurav.khemka@anu.edu.au>
#### Subject: Re: STAT3058 A2 - Simulation question

Hi Professor,

Thank you for your brisk reply.

Upon reviewing my work, I realised that I had been simulating interarrival times rather than claim frequencies per time step.

However, after making changes to the Monte Carlo Sim code, only the value for U0 = 2 remains the same. Am I right in saying that you want us to simulate via claim frequencies per time step? Is there any differences/advantages to simulating claim frequencies rather than interarrival times?

Thank you.

Regards,  
Leong Yin Chan  
U6858120

---

#### From: Gaurav Khemka <gaurav.khemka@anu.edu.au>
#### Sent: Wednesday, 20 September 2023 10:47 AM
#### To: Leong Chan <u6858120@anu.edu.au>
#### Subject: RE: STAT3058 A2 - Simulation question

Hi Leong,

There are no issues with implementing your own function. If you implement the set.seed properly, it should give exactly the same values as the method in lectures. It's important that you implement the set.seed properly.

Regards,  
Garry

---

#### From: Leong Chan <u6858120@anu.edu.au>
#### Sent: Wednesday, 20 September 2023 10:44 AM
#### To: Gaurav Khemka <gaurav.khemka@anu.edu.au>
#### Subject: STAT3058 A2 - Simulation question

Dear Professor,

This is a query in regards to question 2c of assignment 2, the Monte Carlo simulation question. Will I be penalised if I use my own Monte Carlo simulation function? My self-written function still utilises the inverse CDF, a random number generator from 0-1, and the uniroot function. However, I'm not sure how set.seed() is implemented, and I'm concerned that it will lead to different final values.

Thank you.

Leong Yin Chan  
U6858120


---
## Assignment 3
* Data sense checks
* Run-Off triangles
* Active claim number triangle
* Claim payment triangles
* Payments Per Claim Incurred (PPCI) / Payments Per Active Claim (PPAC)
* Bühlmann-Straub credibility


R-Code:
* Run-Off triangles

For this assignment, I performed calculations by representing the triangle as 
a matrix, and manually performing array manipulations. Although this allowed 
me to learn exactly how the model works, with hindsight it was very inefficient 
time-wise. It was intended for students to perform the calculations in excel, 
but I decided to repeatedly write for-loops instead.
