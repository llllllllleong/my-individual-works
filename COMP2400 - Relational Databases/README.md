# COMP2400

| Course                                                   | Coursework | Mark | ANU Grade |
|----------------------------------------------------------|:----------:|:----:|:---------:|
| COMP2400 - Relational Databases                          |    1/2     | 100  |    HD     |
| COMP2400 - Relational Databases                          |    2/2     |  95  |    HD     |

Easy course.

### A Final Sendoff

Assignment 2 was the last piece of coursework I would submit for my degree.
Wanting to make it special, I decided to go the extra mile—not just in solving
the problems, but in how I presented them. I took it upon myself to learn LaTeX
and use it to format my submission, despite knowing it would be a massive and
inefficient time commitment. It was a challenge, but a satisfying one, and in
the end, I was glad I did it.

### Recursive Bitmask Backtracking Algorithm

One of the most interesting parts of the assignment was Question 2, which
required determining the number of unique BCNF decompositions for a given
relation. Rather than manually evaluating all possible decompositions, I saw an
opportunity to take a more algorithmic approach. I wrote a recursive bitmask
backtracking algorithm to systematically generate and count the valid
decompositions. It was a fun problem to solve, and I was confident in my answer.

Despite my confidence, my final grade didn’t reflect the effort I had put in. I
wasn’t penalized for an incorrect answer but rather for not following the
lecture notes exactly—notes that, ironically, contained contradictions. Instead
of rewarding correctness, the grading favored those who blindly followed the
provided algorithm, even when it was flawed. It was a frustrating conclusion,
but I still take pride in the work I did.


Assignment 2 was going to be the last piece of coursework I would submit for
my degree. I was in the mood to put special effort as a final sendoff sort of
thing, so I decided to teach myself Latex and use it to write up my submission.
It was a massive and inefficient time commitment, but i'm glad I did it.
