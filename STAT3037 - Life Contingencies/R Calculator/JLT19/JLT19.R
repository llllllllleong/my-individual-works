rm(list = ls())


setwd("/Users/leong/Desktop/STAT3037 - Life Contingencies/R Calculator/JLT19/")

MaleLifeTables <- read.table(file = "JLTM19.txt",
                             header = FALSE,
                             sep=" ")
FemaleLifeTables <- read.table(file = "JLTF19.txt",
                               header = FALSE,
                               sep=" ")
AnnuityAssurance <- read.table(file = "JLA19.txt",
                               header = FALSE,
                               sep=" ")
JointAnnuity <- read.table(file = "JLA.txt",
                           header = FALSE,
                           sep=" ")
MaleLifeTables$V2 <- as.numeric(as.character(gsub(",", "", MaleLifeTables$V2)))
FemaleLifeTables$V2 <- as.numeric(as.character(gsub(",", "", FemaleLifeTables$V2)))
FemaleLifeTables$V3 <- as.numeric(as.character(gsub(",", "", FemaleLifeTables$V3)))
v <- 1/1.04
{
  lm <- function(x) {
    return (MaleLifeTables$V2[x-19])
  }
  dm <- function(x) {
    return (MaleLifeTables$V3[x-19])
  }
  
  pm <- function(x) {
    return (MaleLifeTables$V4[x-19])
  }
  
  qm <- function(x) {
    return (MaleLifeTables$V5[x-19])
  }
  
  mum <- function(x) {
    return (MaleLifeTables$V6[x-19])
  }
  
  em <- function(x) {
    return (MaleLifeTables$V7[x-19])
  }
  
  lf <- function(x) {
    return (FemaleLifeTables$V2[x-19])
  }
  df <- function(x) {
    return (FemaleLifeTables$V3[x-19])
  }
  
  pf <- function(x) {
    return (FemaleLifeTables$V4[x-19])
  }
  
  qf <- function(x) {
    return (FemaleLifeTables$V5[x-19])
  }
  
  muf <- function(x) {
    return (FemaleLifeTables$V6[x-19])
  }
  
  ef <- function(x) {
    return (FemaleLifeTables$V7[x-19])
  }
  
  addm <- function(x) {
    return (AnnuityAssurance$V2[x-19])
  }
  
  twoAm <- function(x) {
    return (AnnuityAssurance$V3[x-19])
  }
  
  addf <- function(x) {
    return (AnnuityAssurance$V5[x-19])
  }
  
  twoAf <- function(x) {
    return (AnnuityAssurance$V6[x-19])
  }
  
  add <- function(x, d) {
    if (d == -20) return (JointAnnuity$V2[x-44])
    else if (d == -15) return (JointAnnuity$V3[x-44])
    else if (d == -10) return (JointAnnuity$V4[x-44])
    else if (d == -5) return (JointAnnuity$V5[x-44])
    else if (d == -4) return (JointAnnuity$V6[x-44])
    else if (d == -3) return (JointAnnuity$V7[x-44])
    else if (d == -2) return (JointAnnuity$V8[x-44])
    else if (d == -1) return (JointAnnuity$V9[x-44])
    else if (d == 0) return (JointAnnuity$V10[x-44])
    else if (d == 1) return (JointAnnuity$V11[x-44])
    else if (d == 2) return (JointAnnuity$V12[x-44])
    else if (d == 3) return (JointAnnuity$V13[x-44])
    else if (d == 4) return (JointAnnuity$V14[x-44])
    else if (d == 5) return (JointAnnuity$V15[x-44])
    else if (d == 10) return (JointAnnuity$V16[x-44])
    else if (d == 15) return (JointAnnuity$V17[x-44])
    else if (d == 20) return (JointAnnuity$V18[x-44])
    else return (NA)
  }
  
  pm <- function (x, n) {
    return (lm(x+n) / lm(x))
  }
  pf <- function (y, n) {
    return (lf(y+n) / lf(y))
  }
  
}



a1 <- addm(50) - (v^30 * pm(50,30) * addm(80))
a2 <- addf(50) - (v^30 * pf(50,30) * addf(80))
a3 <- add(50,0) - (v^30 * pm(50,30) * pf(50,30) * add(80,0))
a <- a1 + a2 - a3

