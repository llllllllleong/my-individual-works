# STAT3037

| Course                                           | Coursework |  Mark  |
|--------------------------------------------------|:----------:|:------:|
| STAT 3037 - Life Contingencies                   |    1/2     |   80   |
| STAT 3037 - Life Contingencies                   |    2/2     |  100   |

All R-Code in this directory was written by myself.
I have left the code in raw form because it is unlikely I will use it again. 

---
## R-Code: Life Contingencies Calculator
This was quite a straight-forward course, similar to Financial Maths(STAT2032). 
After understanding the conditional characteristics of a policy, it was just 
a matter of looking up the right numbers on Actuarial Tables, and plugging in 
the values into a calculator.

I got annoyed that for all the questions in this course, I could easily form 
the solution equation of value(EOV) in less than a minute, but the actual 
calculation of the EOV would take like 10 minutes. Worst would be when my 
formulation of the EOV was correct but the answer was wrong because of a 
mis-reference of a value in the Actuarial Table.

To solve this problem, I made a personal Life Contingencies calculator. 

In hindsight, it's really just a very use-case specific, glorified getter and 
setter method, that retries values from an Actuarial Table. The retried 
values are then manipulated using R. 

This is one of my favourite projects, because it was very effective at 
increasing my study efficiency. Calculation time was shortened to just a few 
minutes, but more importantly I could be confident that I could blame mistakes 
on my application of theory (which leads to better learning).

I have left the code for the calculator as is, because I don't see myself 
using it again. Furthermore, it's unlikely to be relevant again because it 
depends on the given Actuarial Tables.


---
## Assignment 1
* Endowment policy valuation

Easy assignment, only made 1 mistake. However, because the assignment is short, 
that mistake was worth 20%.


---
## Assignment 2
* Standard Deviation of Expected Present Value of Death Benefit
* Advanced Term-Annuity
* Gross Premium Prospective Policy Value
* Gross Premium Retrospective Policy Value



---

#### From: Jacie Liu <Jia.Liu3@anu.edu.au>
#### Date: Thursday, 28 September 2023 at 6:55 PM
#### To: Leong Chan <u6858120@anu.edu.au>
#### Subject: Re: STAT3037 Assignment 2

Your working is sufficient.

Cheers,
Jacie

---

#### From: Leong Chan <u6858120@anu.edu.au>
#### Sent: Thursday, 28 September 2023 5:43 PM
#### To: Jacie Liu <Jia.Liu3@anu.edu.au>
#### Subject: Re: STAT3037 Assignment 2

Dear Professor,

Thank you for your clarification.
I have uploaded my draft submission to the Wattle portal. However, I used my self-made calculator in R to calculate answers.

I created the calculator in R because of the tedious and error-prone nature of repeatedly referring to life tables. I did this by:
- Saving the life table values in a text file
- Reading the text file into R, and storing the values in a data frame
- Creating functions that access the respective values in the data frame

It’s not really a calculator because the functions I wrote only access values in the life table. For example, I have the function A(x), which returns the respective EPV from the life table of a life insurance plan for an individual aged x.

I’ve included my R code in my submission, but it will not run on any computer other than mine, because the text files are stored locally on my computer.

Would this be sufficient workings? Or should I adjust my answer script and write down every single EPV?

Thank you.

Regards,  
Leong Yin Chan  
U6858120

---

#### From: Jacie Liu <Jia.Liu3@anu.edu.au>
#### Date: Thursday, 28 September 2023 at 3:35 PM
#### To: Leong Chan <u6858120@anu.edu.au>
#### Subject: Re: STAT3037 Assignment 2

Hi Leong Yin,

It should be 1x initial and 20x renewal expenses. The word "renewal" here is more like "recurring." See slide 16 (Ex5.4) of Lecture 5 for reference.

Cheers,
Jacie

---

#### From: Leong Chan <u6858120@anu.edu.au>
#### Sent: Wednesday, 27 September 2023 6:14 PM
#### To: Jacie Liu <Jia.Liu3@anu.edu.au>
#### Subject: Re: STAT3037 Assignment 2

Dear Professor,

Thank you for your reply. Your response has answered my queries.

I’d like to ask one more question regarding how "renewal expenses" is defined. In question 2, the expenses are specified as:
- Initial expense of 10% of the first premium, incurred at the beginning of the insurance contract
- Renewal expenses of 5% of the annual survival benefit amount, incurred at the same time the benefits are paid

Is renewal expense defined as the annual cost of continuing the annuity/insurance plan, within the term? If so, I would interpret that as the expense for the first term, 0<t<=1, with 1x initial and 19x renewal expenses.

Or does the renewal expense include the first year, making it 1x initial and 20x renewal expenses?

Thank you.

Regards,
Leong Yin Chan  
U6858120

---

#### From: Jacie Liu <Jia.Liu3@anu.edu.au>
#### Date: Wednesday, 27 September 2023 at 5:15 PM
#### To: Leong Chan <u6858120@anu.edu.au>
#### Subject: Re: STAT3037 Assignment 2

Hi Leong Yin,

Quoting different quantities from the life table might lead to slightly different answers. Please do not use the commutation functions for calculation since they are out of syllabus. For the other quantities in the life table, feel free to use any of them as long as you show clearly your workings. For instance, if you are calculating px using l_x+1 and l_x, make sure you have the l_x values in your spreadsheet, and your px value is entered as a formula rather than a hardcoded value.

For the final answers, it is okay not to do rounding or you may round to the nearest two decimal places.

Cheers,
Jacie

---

#### From: Leong Chan <u6858120@anu.edu.au>
#### Sent: Wednesday, 27 September 2023 11:43 AM
#### To: Jacie Liu <Jia.Liu3@anu.edu.au>
#### Subject: STAT3037 Assignment 2

Dear Professor,

I would like to ask about your standards regarding rounding and using commutation functions. I’ve coded functions to evaluate the actuarial present values, but I've found that I receive different answers due to rounding errors. For example, using p(x) versus using (l(x+1) / l(x)). Commutation functions also give rounding errors, and since they are not examined, I’m not sure which answer I should use, or if you want us to round the final answer.

Thank you.

Leong Yin Chan  
U6858120
