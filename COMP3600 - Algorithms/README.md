# COMP3600

| Course                |  Coursework  | Mark | ANU Grade |
|-----------------------|:------------:|:----:|:---------:|
| COMP3600 - Algorithms |     1/2      |  83  |    HD     |
| COMP3600 - Algorithms |     2/2      | 100  |    HD     |

This course ended up replacing COMP1100 as my all-time favorite. In terms of
difficulty, I found the content quite manageable—not just because of my
mathematical background, but also thanks to the months I had spent practicing
Leetcode problems before the semester started. That
preparation paid off, making the concepts feel intuitive and the problem-solving
aspect especially enjoyable.

### Coursework

The coursework consisted of two three-hour online quizzes, each made up of a mix
of short and long answer questions. While they were time-intensive, they were
also straightforward. Given the format, I decided not to document every question
here, as taking screenshots of all of them would be tedious.

### MSE

One of the more unusual aspects of the course was that the conveners not only
provided feedback on the mid-semester exam but also returned scanned copies of
our exam submissions.

One of the key questions, Q4, was a significantly weighted problem that involved
solving a Leetcode style problem. The task required not only solving the problem
but also analyzing the time complexity and correctness of our proposed
algorithm. Given my prior experience with competitive programming, I felt
confident going in.
However, I made a critical oversight—assuming that a HashSet has an O(n) time
complexity operations, when in reality, while it only amortized complexity, and
its worst-case scenario can degrade to O(n²) due to hash collisions.

Unfortunately, this mistake resulted in a 50% mark penalty for the entire
question.

I was deeply disappointed in myself—this was an easy question, and I knew I
should have done better. However, upon reviewing the feedback, I realized that
the markers had been extremely generous. Given that my proposed time complexity
was off by a factor of n, I technically should have lost 75% of the marks.
Instead, I only lost half.

I can only assume that the marker recognized the situation and, rather than
penalizing me for a lack of understanding, they deducted marks for carelessness.
For that, I am incredibly grateful.

![](../../../Screenshots/Screenshot 2025-01-25 at 10.14.54 PM.png)