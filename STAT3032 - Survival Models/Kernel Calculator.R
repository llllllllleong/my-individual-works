
# Kernel Smoothing
# Input data
x <- c(20,21,22,23,24,25)
y <- c(0.028884,0.016260,0.040123,0.018386,0.043750,0.022293)
bandwidth <- 5
smoothedAtPoint <- 22



kernelInput <- (abs(smoothedAtPoint - x) 
                / bandwidth)
# Normal Kernel VERIFIED
# kernel <- function(t) {
#   numerator = exp(-t^2 / 2)
#   denominator = sqrt(2*pi)
#   return (numerator/denominator)
# }

# Triangle Kernel VERIFIED
# kernel <- function(t) {
#   if (abs(t) > 0.5) {
#     return (0)
#   } else {
#     return (2 - 4 * abs(t))
#   }
# }

# Custom Kernel
# kernel <- function(t) {
#   return (exp(-2 * abs(t)))
# }

kernelOutput <- sapply(kernelInput,
                       FUN = kernel)
kernelSum <- sum(kernelOutput)
w <- (kernelOutput) / kernelSum
wy = w * y
(sumWY = sum(wy))






# Visualise different bandwidth
set.seed(123)
x<-seq(0,4,by=0.05)
y<-sin(4*x)+rnorm(length(x),0,1/3)
# par(mfrow=c(2,1))
plot(x,y)
#Kernel Smoothing
fit<-ksmooth(x,y,kernel="normal",bandwidth=0.25)
lines(fit$x,fit$y,col="red")
fit<-ksmooth(x,y,kernel="normal",bandwidth=0.1)
lines(fit$x,fit$y,col="blue")
fit<-ksmooth(x,y,kernel="normal",bandwidth=0.5)
lines(fit$x,fit$y,col="green")
fit<-ksmooth(x,y,kernel="normal",bandwidth=1)
lines(fit$x,fit$y,col="purple")


#using different values of bandwidth suggets that a value around 0.25 is OK.
plot(x,y)
#Spline Smoothing
library(splines)
fit<-lm(y~ns(x,df=12))
values<-seq(0,4,0.01)
temp<-data.frame("x"=values)
fit<-predict(fit,temp)
lines(values,fit,col="red")
#Using different values of df suggets that a value around 12 is OK.


data <- read.table(file = "~/Desktop/STAT3032/A2/temp.txt",
                   header = FALSE,
                   sep=" ")
standardisedVariance <- sum(data$V8^2)

