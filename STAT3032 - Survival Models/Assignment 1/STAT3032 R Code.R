# Generate country, year and gender using student ID
myID <- 6858120 # put your student ID here (as a number, without the "u")
# Aus 1921-2016, Austria 1947-2017, Belarus 1959-2016, Chile 1992-2008
d1 <- data.frame(country="Australia",year=c(1921:2016))
d2 <- data.frame(country="Austria",year=c(1947:2017))
d3 <- data.frame(country="Belarus",year=c(1959:2016))
d4 <- data.frame(country="Belgium",year=c(1841:2018))
d5 <- data.frame(country="Bulgaria",year=c(1947:2017))
d6 <- data.frame(country="Canada",year=c(1921:2016))
d7 <- data.frame(country="Chile",year=c(1992:2008))
d8 <- data.frame(country="Croatia",year=c(2002:2017))
d9 <- data.frame(country="Czechia",year=c(1970:2017))
d10 <- data.frame(country="Denmark",year=c(1835:2016))
d11 <- data.frame(country="Estonia",year=c(1959:2017))
d12 <- data.frame(country="Finland",year=c(1878:2018))
d13 <- data.frame(country="France",year=c(1816:2017))
d14 <- data.frame(country="Germany",year=c(1990:2017))
d15 <- data.frame(country="Greece",year=c(1981:2017))
d16 <- data.frame(country="Hong Kong",year=c(1986:2017))
d17 <- data.frame(country="Japan",year=c(1947:2017))
d18 <- data.frame(country="New Zealand",year=c(1948:2013))
d.A <- d.B <- rbind(d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15)
d.A$sex <- "male"
d.B$sex <- "female"
d <- rbind(d.A,d.B)
set.seed(myID)
my.example <- sample(nrow(d),1)
d[my.example,]




#Initialisation
mylifetab <- read.table("~/Desktop/STAT3032/A1/fltper_1x1.txt",
                        header=TRUE,
                        stringsAsFactors=FALSE)
mylifetab <- mylifetab[mylifetab$Year==1834,]
mylifetab$Age2 <- mylifetab$Age
mylifetab$Age2[mylifetab$Age=='110+'] <- 110
mylifetab$age.numeric <- as.numeric(mylifetab$Age2)
attach(mylifetab)


#Question 1
#Plot Deaths vs Age
par(mar = c(5.1, 5.1, 4.1, 2.1))
plot(age.numeric, dx,
     main = "Deaths ~ Age",
     xlab = "Age (Years)",
     ylab = "Deaths"
)

#Ignore deaths from age 0 to 5
plot(age.numeric[age.numeric>5], dx[age.numeric>5],
     main = "Deaths ~ Age > 5",
     xlab = "Age (Years)",
     ylab = "Deaths"
)


#Question 2
#Plot Death Probability vs Age
plot(age.numeric, qx,
     main = "Death Probability ~ Age",
     xlab = "Age (Years)",
     ylab = "Death Probability"
)

#Censor age 110
plot(age.numeric[0:110], qx[0:110],
     main = "Death Probability ~ Age < 110",
     xlab = "Age (Years)",
     ylab = "Death Probability"
)
lines(lowess(age.numeric, qx), 
      col="blue")

#Try log transformation
plot(age.numeric[0:110], log(qx[0:110]),
     main = "Log(Death Probability) ~ Age",
     xlab = "Age (Years)",
     ylab = "Log(Death Probability)"
)
lines(lowess(age.numeric, log(qx)), 
      col="blue")


#Question 3
#Curated expectation of life
mylifetab$px <- (1 -mylifetab$qx)
#For a person aged 0
mylifetab$kp0 <- cumprod(mylifetab$px)
sum(mylifetab$kp0)

#For a person aged 10
mylifetab$kp10 <- mylifetab$px
mylifetab$kp10[0:10] <- 1
mylifetab$kp10 <- cumprod(mylifetab$kp10)
mylifetab$kp10[0:10] <- 0
sum(mylifetab$kp10)
      
#For a person aged 30
mylifetab$kp30 <- mylifetab$px
mylifetab$kp30[0:30] <- 1
mylifetab$kp30 <- cumprod(mylifetab$kp30)
mylifetab$kp30[0:30] <- 0
sum(mylifetab$kp30)


#Question 4
#Gompertz model
model <- lm(log(qx) ~ age.numeric)
summary(model)
model$coef
exp(model$coef)


#Question 5
plot(age.numeric, log(qx),
     main = "Log(qx) ~ Age",
     xlab = "Age (Years)",
     ylab = "Log(qx)")
abline(model,
       col = "red", 
       lty = 1)
lines(lowess(age.numeric, log(qx)), 
      col="blue")

#Residuals plot
plot(fitted(model), residuals(model),
     main = "Residuals vs Fitted",
     xlab = "Fitted Values",
     ylab = "Residuals")
abline(0,0)
lines(lowess(fitted(model), residuals(model)), col="blue")

#QQ Normal Plot
res=residuals(model)
qqnorm(res, main = "Normal QQ plot")
qqline(res,
       col = "red")
identify(res)


#Comparing transformations
modlog <- lm(log(qx) ~ age.numeric)
modsqrt <- lm(sqrt(qx) ~ age.numeric)
modsqrtquarter <- lm(((qx)^(0.25)) ~ age.numeric)
modnotransform <- lm(qx ~ age.numeric)

par(mar = c(5.1, 5.1, 4.1, 2.1))
par(mfrow=c(2,2))

plot(fitted(modlog), 
     residuals(modlog), 
     xlab="Fitted", 
     ylab="Residual", 
     cex.main=1.5, 
     cex.lab=1.25, 
     pch = 16, 
     main="Log Transformation")
abline(h=0)
lines(lowess(fitted(modlog), residuals(modlog)), col="red")

plot(fitted(modsqrt), 
     residuals(modsqrt), 
     xlab="Fitted", 
     ylab="Residual", 
     cex.main=1.5, 
     cex.lab=1.25, 
     pch = 16, 
     main="Square Root Transformation")
abline(h=0)
lines(lowess(fitted(modsqrt), residuals(modsqrt)), col="red")

plot(fitted(modsqrtquarter), 
     residuals(modsqrtquarter), 
     xlab="Fitted", 
     ylab="Residual", 
     cex.main=1.5, 
     cex.lab=1.25, 
     pch = 16, 
     main="0.25 Power Transformation")
abline(h=0)
lines(lowess(fitted(modsqrtquarter), residuals(modsqrtquarter)), col="red")

plot(fitted(modnotransform), 
     residuals(modnotransform), 
     xlab="Fitted", 
     ylab="Residual", 
     cex.main=1.5, 
     cex.lab=1.25, 
     pch = 16, 
     main="No Transformation")
abline(h=0)
lines(lowess(fitted(modnotransform), residuals(modnotransform)), col="red")

