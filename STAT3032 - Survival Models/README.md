# STAT3032

| Course                                           | Coursework |  Mark  |
|--------------------------------------------------|:----------:|:------:|
| STAT 3032 - Survival Models                      |    1/2     |   94   |
| STAT 3032 - Survival Models                      |    2/2     |   98   |

All R-Code in this directory was written by myself.
I have left the code in raw form because it is unlikely I will use it again. 

---
## R-Code: Kernel Smoothing Calculator
I forgot why I made this, or what I used this for, but I'm a bit too lazy to 
look into it.

---
## Assignment 1
* Synthetic Data Analysis Report
* Hazard rate
* Curtate expectation of life
* Gompertz model and diagnostics

R-Code:
* Unremarkable code, generating graphs and models


---
## Assignment 2
* Synthetic Data Analysis Report
* Kaplan-Meier Estimate of Survival Function
* Cox Proportional Hazards model

R-Code:
* Unremarkable code, generating graphs and models